use std::process::{Command, Stdio};

use camino::{Utf8Path, Utf8PathBuf};
use path_ext::Utf8PathExt as _;

use crate::DownloadError;

pub async fn podcast(
    audio_url: &str,
    image_url: &str,
    base_filename: &Utf8Path,
    directory: &Utf8Path,
) -> Result<Vec<Utf8PathBuf>, DownloadError> {
    // make sure download directory actually exists
    directory.create_dir_all().await?;

    let (audio_file, image_file) =
        get_files(audio_url, image_url, base_filename, directory).await?;

    let video_file = create_video(&audio_file, &image_file, base_filename, directory).await?;

    audio_file.delete().await?;

    Ok(vec![video_file, image_file])
}

async fn get_files(
    audio_url: &str,
    image_url: &str,
    base_filename: &Utf8Path,
    directory: &Utf8Path,
) -> Result<(Utf8PathBuf, Utf8PathBuf), DownloadError> {
    let client = network::new_client()?;

    let audio_path = directory.join(base_filename.append_extension("mp3"));
    let image_path = directory.join(base_filename.append_extension("jpg"));

    let audio_file = network::download_file(&client, audio_url, &audio_path).await?;
    let image_file = network::download_file(&client, image_url, &image_path).await?;

    Ok((audio_file, image_file))
}

async fn create_video(
    audio_file: &Utf8Path,
    image_file: &Utf8Path,
    base_filename: &Utf8Path,
    directory: &Utf8Path,
) -> Result<Utf8PathBuf, DownloadError> {
    let video_file = directory.join(base_filename.append_extension("mkv"));
    let temp_video_file = directory.join(base_filename.append_extension("t.mkv"));

    // create a 2s long 30fps video from thumbnail
    #[rustfmt::skip]
    process::Command::new("ffmpeg")
        .args([ "-y", "-hide_banner", "-loglevel", "error", "-t", "2", "-r", "30", "-loop", "1", "-framerate", "1", "-i",
              image_file.as_str(), "-c:v", "libx264", "-preset", "veryslow", "-profile:v", "main", "-movflags", "+faststart", "-vf",
              "scale=w=1280:h=720:force_original_aspect_ratio=decrease,format=yuv420p", temp_video_file.as_str()])
        .output()
        .await
        .map_err(|source| DownloadError::FfmpegError { source })?;

    // create continuous loop from that 2s long video
    #[rustfmt::skip]
    let cmd1 = Command::new("ffmpeg")
        .stdout(Stdio::piped())
        .args([ "-y", "-hide_banner", "-loglevel", "error", "-stream_loop", "-1", "-i", temp_video_file.as_str(), 
              "-c", "copy", "-v", "0", "-f", "nut", "-"])
        .spawn()
        .map_err(|source| DownloadError::FfmpegChildError { source })?;

    // merge video from that loop with the audio
    #[rustfmt::skip]
    process::Command::new("ffmpeg")
        .stdin(cmd1.stdout.ok_or(DownloadError::FfmpegPipeError)?)
        .args([ "-y", "-hide_banner", "-loglevel", "error", "-thread_queue_size", "10K", "-i", "-", "-i", audio_file.as_str(), 
              "-c", "copy", "-map", "0:v", "-map", "1:a", "-shortest", video_file.as_str()])
        .output()
        .await
        .map_err(|source| DownloadError::FfmpegError { source })?;

    temp_video_file.delete().await?;

    if !video_file.is_file() {
        return Err(DownloadError::FileNotFoundError(video_file));
    }

    Ok(video_file)
}
