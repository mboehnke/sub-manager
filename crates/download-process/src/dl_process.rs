use std::{
    ops::{Deref, DerefMut},
    time::Duration,
};

use camino::{Utf8Path, Utf8PathBuf};
use path_ext::Utf8PathExt as _;
use yt_dlp::YtDlpProcess;

use crate::DownloadError;

#[derive(Debug, Clone)]
pub struct DownloadProcess {
    filename: Utf8PathBuf,
    process: YtDlpProcess,
}

impl DownloadProcess {
    pub(crate) fn new(base_filename: &Utf8Path, directory: &Utf8Path) -> Self {
        let filename = directory.join(base_filename);

        let mut process = YtDlpProcess::default();
        process
            .output(&format!("{filename}.%(ext)s"))
            .merge_output_format("mkv")
            .write_thumbnail()
            .convert_thumbnails("jpg")
            .prefer_free_formats()
            .quiet()
            .no_warnings();

        Self { filename, process }
    }

    pub fn mediathek_id(mut self, video_id: &str) -> Self {
        self.process.mediathek_id(video_id);
        self
    }

    pub fn twitch_id(mut self, video_id: &str) -> Self {
        self.process.twitch_id(video_id);
        self
    }

    pub fn youtube_id(mut self, video_id: &str) -> Self {
        self.process.youtube_id(video_id);
        self
    }

    /// Number of fragments of a dash/hlsnative video that should be downloaded concurrently (default is 1)
    pub fn concurrent_fragments(mut self, n: u8) -> Self {
        if n > 1 {
            self.process.concurrent_fragments(&n.to_string());
        }
        self
    }

    /// Video format code
    pub fn format(mut self, f: &str) -> Self {
        if !f.is_empty() {
            self.process.format(f);
        }
        self
    }

    /// Sort the formats by the fields given
    pub fn format_sort(mut self, s: &str) -> Self {
        if !s.is_empty() {
            self.process.format_sort(s);
        }
        self
    }

    /// Requires the download to complete before the specified duration has elapsed.
    ///
    /// If the duration elapses before the download completes an error is returned and the download is canceled.
    pub fn timeout(mut self, t: Duration) -> Self {
        self.process.timeout(t);
        self
    }

    /// Minimum download rate in bytes per second below which throttling is assumed and the video data is re-extracted (e.g. 100K)
    pub fn throttled_rate(mut self, t: &str) -> Self {
        if !t.is_empty() {
            self.process.throttled_rate(t);
        }
        self
    }

    /// SponsorBlock categories to create chapters for, separated by commas.
    /// Available categories are all, default(=all), sponsor, intro, outro, selfpromo, preview,
    /// filler, interaction, music_offtopic, poi_highlight. You can prefix the category with
    /// a "-" to exempt it. See 1 for description of the categories. Eg: --sponsorblock-mark all,-preview
    ///
    /// 1 https://wiki.sponsor.ajay.app/w/Segment_Categories
    pub fn sponsorblock_mark(mut self, s: &str) -> Self {
        if !s.is_empty() {
            self.process.sponsorblock_mark(s);
        }
        self
    }

    /// SponsorBlock categories to be removed from the video file, separated by commas.
    /// If a category is present in both mark and remove, remove takes precedence.
    /// The syntax and available categories are the same as for --sponsorblock-mark except
    /// that "default" refers to "all,-filler" and poi_highlight is not available
    pub fn sponsorblock_remove(mut self, s: &str) -> Self {
        if !s.is_empty() {
            self.process.sponsorblock_remove(s);
        }
        self
    }

    pub fn sponsorblock_api(mut self, s: &str) -> Self {
        if !s.is_empty() {
            self.process.sponsorblock_api(s);
        }
        self
    }

    pub fn sponsorblock_disable(mut self) -> Self {
        self.process.no_sponsorblock();
        self
    }

    /// downloads both video and a thumbnail image
    pub async fn get(mut self) -> Result<Vec<Utf8PathBuf>, DownloadError> {
        self.process.run().await?;

        let image_file = self.filename.append_extension("jpg");
        if !image_file.is_file() {
            return Err(DownloadError::FileNotFoundError(image_file));
        }
        let video_file = ["mp4", "mkv", "webm"]
            .into_iter()
            .map(|ext| self.filename.append_extension(ext))
            .find(|f| f.is_file())
            .ok_or_else(|| {
                DownloadError::FileNotFoundError(self.filename.append_extension("[mkv/mp4/webm]"))
            })?;

        Ok(vec![video_file, image_file])
    }
}

impl Deref for DownloadProcess {
    type Target = YtDlpProcess;

    fn deref(&self) -> &Self::Target {
        &self.process
    }
}

impl DerefMut for DownloadProcess {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.process
    }
}
