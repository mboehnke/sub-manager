//! # Dependencies:
//! - python3
//! - yt-dlp
//! - ffmpeg
//! - ffprobe
//! - aria2

mod dl_process;
mod podcast;

use camino::{Utf8Path, Utf8PathBuf};
use network::NetworkError;
use path_ext::Utf8PathError;
use thiserror::Error;
use yt_dlp::CommandError;

use dl_process::DownloadProcess;
pub use podcast::podcast;

pub use yt_dlp::YtDlpOption;

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum DownloadError {
    #[error(transparent)]
    FileError(#[from] Utf8PathError),

    #[error(transparent)]
    YtDlpError(#[from] CommandError),

    #[error(transparent)]
    NetworkError(#[from] NetworkError),

    #[error("could not locate file after download: {0:?}")]
    FileNotFoundError(Utf8PathBuf),

    #[error("error running ffmpeg")]
    FfmpegError { source: process::CommandError },

    #[error("error spawning ffmpeg child process")]
    FfmpegChildError { source: std::io::Error },

    #[error("error piping ffmpeg subprocesses")]
    FfmpegPipeError,
}

pub fn ard(video_id: &str, base_filename: &Utf8Path, directory: &Utf8Path) -> DownloadProcess {
    DownloadProcess::new(base_filename, directory).mediathek_id(video_id)
}

pub fn twitch(video_id: &str, base_filename: &Utf8Path, directory: &Utf8Path) -> DownloadProcess {
    DownloadProcess::new(base_filename, directory).twitch_id(video_id)
}

pub fn youtube(video_id: &str, base_filename: &Utf8Path, directory: &Utf8Path) -> DownloadProcess {
    DownloadProcess::new(base_filename, directory).youtube_id(video_id)
}
