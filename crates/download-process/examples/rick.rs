const VIDEO_ID: &str = "dQw4w9WgXcQ";

#[tokio::main]
async fn main() {
    let files = download_process::youtube(VIDEO_ID, "rick".into(), "/tmp".into())
        .get()
        .await
        .unwrap();
    println!("downloaded files:");
    for file in files {
        let (size, unit) = match file.metadata().unwrap().len() {
            s if s < 1024 => (s, "B"),
            s if s < 1024 * 1024 => (s / 1024, "KB"),
            s => (s / 1024 / 1024, "MB"),
        };
        println!("{file} ({size}{unit})");
    }
}
