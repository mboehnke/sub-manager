const AUDIO_URL: &str =
    "https://sphinx.acast.com/p/acast/s/die-sprechstunde/e/6224fc6bafd150001386e4d7/media.mp3";
const IMAGE_URL: &str = "https://assets.pippa.io/shows/60ddbc617c3018e3a69a190a/1646591050268-16c9a8cdfa0990c22d964d9f15b7ad63.jpeg";

#[tokio::main]
async fn main() {
    let files = download_process::podcast(AUDIO_URL, IMAGE_URL, "sprechstunde".into(), "/tmp".into())
        .await
        .unwrap();
    println!("downloaded files:");
    for file in files {
        let (size, unit) = match file.metadata().unwrap().len() {
            s if s < 1024 => (s, "B"),
            s if s < 1024 * 1024 => (s / 1024, "K"),
            s => (s / 1024 / 1024, "M"),
        };
        println!("{file} ({size}{unit})");
    }
}
