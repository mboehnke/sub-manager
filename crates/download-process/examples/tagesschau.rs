const VIDEO_ID: &str =
    "Y3JpZDovL2Rhc2Vyc3RlLmRlL3RhZ2Vzc2NoYXUvMzkxY2VmYmItMTNhMS00Y2UzLThkZTUtOWE0ODU2M2Q1YjQx";

#[tokio::main]
async fn main() {
    let files = download_process::ard(VIDEO_ID, "tagesschau".into(), "/tmp".into())
        .get()
        .await
        .unwrap();
    println!("downloaded files:");
    for file in files {
        let (size, unit) = match file.metadata().unwrap().len() {
            s if s < 1024 => (s, "B"),
            s if s < 1024 * 1024 => (s / 1024, "K"),
            s => (s / 1024 / 1024, "M"),
        };
        println!("{file} ({size}{unit})");
    }
}
