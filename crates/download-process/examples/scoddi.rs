const VIDEO_ID: &str = "1269147811";

#[tokio::main]
async fn main() {
    let files = download_process::twitch(VIDEO_ID, "scoddi".into(), "/tmp".into())
        .get()
        .await
        .unwrap();
    println!("downloaded files:");
    for file in files {
        let (size, unit) = match file.metadata().unwrap().len() {
            s if s < 1024 => (s, "B"),
            s if s < 1024 * 1024 => (s / 1024, "K"),
            s => (s / 1024 / 1024, "M"),
        };
        println!("{file} ({size}{unit})");
    }
}
