use crate::{YouTubeError, YouTubeVideo};
use apply::Apply;
use atom_syndication::{Entry, Feed};
use chrono::{Duration, Utc};
use network::Client;

impl YouTubeVideo {
    pub async fn from_channel_feed(
        client: &Client,
        channel_id: &str,
        max_age: Duration,
    ) -> Result<Vec<YouTubeVideo>, YouTubeError> {
        let feed_url = format!(
            "https://www.youtube.com/feeds/videos.xml?channel_id={}",
            channel_id
        );

        let feed = (&feed_url)
            .apply(|feed_url| network::download_text(client, feed_url))
            .await?
            .parse::<Feed>()
            .map_err(|source| YouTubeError::FeedParseError {
                source,
                url: feed_url.to_string(),
            })?;

        let channel = feed.title().to_string();

        let now = Utc::now();

        let videos = feed
            .entries()
            .iter()
            .filter_map(|entry| Self::maybe_from(entry, &channel))
            // younger than max_age
            .filter(|v| v.upload_date + max_age > now)
            .collect();

        Ok(videos)
    }

    fn maybe_from(entry: &Entry, channel: &str) -> Option<YouTubeVideo> {
        // get published date / ignore older videos
        let upload_date = (*entry.published()?).into();
        let id = entry
            .extensions()
            .get("yt")?
            .get("videoId")?
            .first()?
            .value()?
            .to_string();
        let description = entry
            .extensions()
            .get("media")?
            .get("group")?
            .first()?
            .children()
            .get("description")?
            .first()?
            .value()?
            .to_string();
        Some(YouTubeVideo {
            id,
            channel: channel.to_string(),
            title: entry.title().to_string(),
            description,
            upload_date,
        })
    }
}
