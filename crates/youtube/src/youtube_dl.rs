use std::convert::TryFrom;
use std::convert::TryInto;

use crate::{YouTubeError, YouTubeVideo};

use chrono::DateTime;
use lazy_static::lazy_static;
use regex::Regex;
use serde::Deserialize;

#[derive(Deserialize)]
struct Info {
    display_id: String,
    uploader: String,
    title: String,
    description: String,
    upload_date: String,
}

impl YouTubeVideo {
    /// gets info for a youtube video
    pub async fn from_id(id: &str) -> Result<YouTubeVideo, YouTubeError> {
        use yt_dlp::YtDlpOption::*;
        let json = yt_dlp::run([Simulate, DumpJson, Url(id.into())]).await?;
        let info = serde_json::from_str::<Info>(&json)
            .map_err(|source| YouTubeError::JsonParseError { source })?;
        info.try_into()
    }

    /// gets info for a youtube video
    pub async fn from_url(url: &str) -> Result<YouTubeVideo, YouTubeError> {
        lazy_static! {
            static ref RE:Regex=Regex::new(r#"(?:youtube(?:-nocookie)?\.com/(?:[^/\n\s]+/\S+/|(?:v|e(?:mbed)?)/|\S*?[?&]v=)|youtu\.be/)([a-zA-Z0-9_-]{11})"#)
            .unwrap();
        }
        let id = RE
            .captures(url)
            .and_then(|c| c.get(1))
            .map(|id| id.as_str())
            .ok_or_else(|| YouTubeError::NoIdError(url.to_string()))?;
        Self::from_id(id).await
    }
}

impl TryFrom<Info> for YouTubeVideo {
    type Error = YouTubeError;

    fn try_from(info: Info) -> Result<Self, Self::Error> {
        let date = format!("{}000000+0000", info.upload_date);
        let upload_date = DateTime::parse_from_str(&date, "%Y%m%d%H%M%S%z")
            .map_err(|source| YouTubeError::DatetimeParseError { source, date })?
            .into();
        Ok(YouTubeVideo {
            id: info.display_id,
            channel: info.uploader,
            title: info.title,
            description: info.description,
            upload_date,
        })
    }
}
