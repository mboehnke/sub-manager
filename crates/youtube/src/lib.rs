//! # Dependencies:
//! - python3
//! - pip3
//! - yt-dlp

mod rss;

use std::time::Duration;

use chrono::{DateTime, Utc};
use google_api::VideoInfo;
use network::NetworkError;
use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub struct YouTubeVideo {
    pub channel: String,
    pub title: String,
    pub description: String,
    pub upload_date: DateTime<Utc>,
    pub id: String,
}

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum YouTubeError {
    #[error(transparent)]
    NetworkError(#[from] NetworkError),

    #[error("could not download rss feed: {url:?}")]
    FeedDownloadError {
        source: network::NetworkError,
        url: String,
    },

    #[error("could not parse rss feed: {url:?}")]
    FeedParseError {
        source: atom_syndication::Error,
        url: String,
    },

    #[error("error running youtube-dl")]
    YoutubeDlIoError { source: std::io::Error },

    #[error("error running youtube-dl, process timed out after {timeout:?}")]
    YoutubeDlTimeoutError {
        source: tokio::time::error::Elapsed,
        timeout: Duration,
    },

    #[error("failed to extract youtube id from url: {0:?}")]
    NoIdError(String),
}

impl From<VideoInfo> for YouTubeVideo {
    fn from(item: VideoInfo) -> Self {
        YouTubeVideo {
            channel: item.channel,
            title: item.title,
            description: item.description,
            id: item.id,
            upload_date: item.upload_date,
        }
    }
}

impl From<&VideoInfo> for YouTubeVideo {
    fn from(item: &VideoInfo) -> Self {
        item.clone().into()
    }
}
