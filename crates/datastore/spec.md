# Datastore
- **open**(path: *impl AsRef\<Utf8Path>*) -> *Result\<Datastore, DatastoreError>*
- **history**()
    - **insert**(entry: *impl Into\<HistoryEntry*>) -> *Result\<bool, DatastoreError>*
    - **get_all**() -> *Result\<Vec\<HistoryEntry>, DatastoreError>*
- **rules**()
    - **get_all**() -> *Result\<HashMap\<u64, Rule>, DatastoreError>*
    - **insert**(value: *Rule*) -> *Result\<bool, DatastoreError>*
    - **swap**(key1: *u64*, key2: *u64*) -> *Result\<(), DatastoreError>*
    - **remove**(key: *u64*) -> *Result\<bool, DatastoreError>*
- **settings**()
    - **get_all**() -> *Result\<HashMap\<String, Setting>, DatastoreError>*
    - **get**(key: *&str*) -> *Result\<Setting, DatastoreError>*
    - **insert**(key: *&str*, value: *impl Into\<Setting>*) -> *Result\<bool, DatastoreError>*
    - **get_or_insert**(key: *&str*, default_value: *impl Into\<Setting>*) -> *Result\<Setting, DatastoreError>*
    - **remove**(key: *&str*) -> *Result\<bool, DatastoreError>*
- **subscriptions**()
    - **get_all**() -> *Result\<Vec\<Subscription>, DatastoreError>*
    - **insert**(sub: *Subscription*) -> *Result\<bool, DatastoreError>*
    - **remove**(sub: *&Subscription*) -> *Result\<bool, DatastoreError>*
- **queue**()
    - **iter**() -> *impl Iterator\<Item = QueueEntryReference>*
    - **entry**(id: *u64*) -> *QueueEntryReference*
    - **get_all**() -> *Result\<HashMap\<u64, QueueEntry>, DatastoreError>*
    - **get_all_as_historyentry**() -> *Result\<Vec\<HistoryEntry>, DatastoreError>*
    - **insert**(item: *impl Into\<QueueItem>*) -> *Result\<bool, DatastoreError>*
    - **insert_if_new**(item: *impl Into\<QueueItem>*) -> *Result\<bool, DatastoreError>*

# HistoryEntry
- **from**(entry: *&QueueEntry*) -> *Self*
- **from**(item: *&QueueItem*) -> *Self*
- **from**(podcast: *&Podcast*) -> *Self*
- **from**(video: *&TwitchVideo*) -> *Self*
- **from**(video: *&YouTubeVideo*) -> *Self*- **podcast**() -> *Option\<String>*
- **twitch**() -> *Option\<String>*
- **youtube**() -> *Option\<String>*

# Setting
- **from**(inner: *i64*) -> *Self*
- **from**(inner: *DateTime<Utc>*) -> *Self*
- **from**(inner: *&str*) -> *Self*
- **from**(inner: *&String*) -> *Self*
- **from**(inner: *String*) -> *Self*
- **text**() -> *Option\<String>*
- **number**() -> *Option\<i64>*
- **date**() -> *Option\<DateTime\<Utc>>*

# Subscription
- **podcast**() -> *Option\<String>*
- **twitch**() -> *Option\<String>*


# QueueEntry
- **from**(item: *QueueItem*) -> *Self*

# QueueItem
- **from**(inner: *Podcast*) -> *Self*
- **from**(inner: *TwitchVideo*) -> *Self*
- **from**(inner: *YouTubeVideo*) -> *Self*
- **podcast**() -> *Option\<Podcast>*
- **twitch**() -> *Option\<TwitchVideo>*
- **youtube**() -> *Option\<YouTubeVideo>*

# QueueEntryReference
- **get**() -> *Result\<QueueEntry, DatastoreError>*
- **remove**() -> *Result\<bool, DatastoreError>*
- **record_error**(error: *impl std::fmt::Display*) -> *Result\<(), DatastoreError>*
- **mark_active**() -> *Result\<(), DatastoreError>*
- **mark_paused**(resume_at: *DateTime\<Utc>*) -> *Result\<(), DatastoreError>*
- **mark_stopped**() -> *Result\<(), DatastoreError>*
- **mark_pending**() -> *Result\<(), DatastoreError>*
