use crate::{DatastoreError, Subscription, Subscriptions};

impl Subscriptions {
    pub fn get_all(&self) -> Result<Vec<Subscription>, DatastoreError> {
        self.0.keys()
    }

    pub fn insert(&self, sub: Subscription) -> Result<bool, DatastoreError> {
        self.0.insert(sub, ())
    }

    pub fn remove(&self, sub: &Subscription) -> Result<bool, DatastoreError> {
        self.0.remove(sub)
    }
}

impl Subscription {
    pub fn podcast(self) -> Option<String> {
        match self {
            Self::Podcast(sub) => Some(sub),
            _ => None,
        }
    }

    pub fn twitch(self) -> Option<String> {
        match self {
            Self::Twitch(sub) => Some(sub),
            _ => None,
        }
    }
}
