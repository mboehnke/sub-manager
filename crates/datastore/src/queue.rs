use crate::{
    table::serialize,
    DatastoreError::{self, *},
    DownloadError, DownloadStatus, HistoryEntry, Queue, QueueEntry, QueueEntryReference, QueueItem,
    QueueIterator,
};

use chrono::{DateTime, Utc};
use hashbrown::HashMap;
use podcast_dl::Podcast;
use sled::transaction::{ConflictableTransactionError, TransactionalTree};
use sled::Transactional as _;
use twitch_dl::TwitchVideo;
use youtube::YouTubeVideo;

impl Queue {
    pub fn iter(&self) -> impl Iterator<Item = QueueEntryReference> {
        QueueIterator {
            queue: self.queue.clone(),
            visited_ids: Vec::new(),
        }
    }

    pub fn entry(&self, id: u64) -> QueueEntryReference {
        QueueEntryReference {
            queue: self.queue.clone(),
            id,
        }
    }

    pub fn get_all(&self) -> Result<HashMap<u64, QueueEntry>, DatastoreError> {
        self.queue.get_all()
    }

    pub fn get_all_as_historyentry(&self) -> Result<Vec<HistoryEntry>, DatastoreError> {
        let entries = self.queue.values()?;
        let identifiers = entries
            .into_iter()
            .map(|entry| HistoryEntry::from(&entry.item))
            .collect();
        Ok(identifiers)
    }

    /// returns `true` if the item was added to the Queue, `false` if it wasn't
    pub fn insert(&self, item: impl Into<QueueItem>) -> Result<bool, DatastoreError> {
        self.insert_(item, false)
    }

    /// returns `true` if the item was added to the Queue, `false` if it wasn't
    pub fn insert_if_new(&self, item: impl Into<QueueItem>) -> Result<bool, DatastoreError> {
        self.insert_(item, true)
    }

    /// returns `true` if the item was added to the Queue, `false` if it wasn't
    fn insert_(
        &self,
        item: impl Into<QueueItem>,
        must_be_new: bool,
    ) -> Result<bool, DatastoreError> {
        let item: QueueItem = item.into();
        let unique_identifier = HistoryEntry::from(&item);
        let entry = QueueEntry::from(item);
        let unique_identifier_bytes = serialize(&unique_identifier)?;
        let entry_bytes = serialize(&entry)?;

        // since false negatives here shouldn't be THAT bad, we just throw away all errors and hope for the best
        let in_queue = self
            .get_all_as_historyentry()
            .unwrap_or_default()
            .contains(&unique_identifier);

        let try_insert = |(history, queue): &(
            TransactionalTree,
            TransactionalTree,
        )|
         -> Result<bool, ConflictableTransactionError> {
            let in_history = history
                .insert(&unique_identifier_bytes, &[])?
                .is_some();
            if must_be_new && (in_history || in_queue) {
                Ok(false)
            } else {
                let id = queue.generate_id()?;
                // serializing a u64 shouldn't be able to fail
                let id_bytes = serialize(&id).unwrap();
                queue.insert(&id_bytes, &entry_bytes)?;
                Ok(true)
            }
        };

        let inserted = (&self.history.tree, &self.queue.tree)
            .transaction(try_insert)
            .map_err(|source| {
                let tables = vec![self.history.name.clone(), self.queue.name.clone()];
                DatabaseTransactionError { source, tables }
            })?;
        Ok(inserted)
    }
}

impl From<Podcast> for QueueItem {
    fn from(inner: Podcast) -> Self {
        Self::Podcast(inner)
    }
}

impl From<TwitchVideo> for QueueItem {
    fn from(inner: TwitchVideo) -> Self {
        Self::Twitch(inner)
    }
}

impl From<YouTubeVideo> for QueueItem {
    fn from(inner: YouTubeVideo) -> Self {
        Self::YouTube(inner)
    }
}

impl<T> From<&T> for QueueItem
where
    T: Clone + Into<QueueItem>,
{
    fn from(inner: &T) -> Self {
        inner.clone().into()
    }
}

impl From<QueueItem> for QueueEntry {
    fn from(item: QueueItem) -> Self {
        QueueEntry {
            item,
            status: DownloadStatus::Pending,
            errors: Vec::new(),
            added: chrono::Utc::now(),
        }
    }
}

impl Iterator for QueueIterator {
    type Item = QueueEntryReference;

    fn next(&mut self) -> Option<Self::Item> {
        let id = self
            .queue
            .keys()
            .ok()?
            .into_iter()
            .find(|id| !self.visited_ids.contains(id))?;
        self.visited_ids.push(id);
        let reference = QueueEntryReference {
            queue: self.queue.clone(),
            id,
        };
        Some(reference)
    }
}

impl QueueEntryReference {
    pub fn get(&self) -> Result<QueueEntry, DatastoreError> {
        self.queue
            .get(&self.id)?
            .ok_or_else(|| DatastoreError::EntryNotFoundError {
                entry: self.id.to_string(),
                table: self.queue.name.to_string(),
            })
    }

    pub fn remove(&self) -> Result<bool, DatastoreError> {
        self.queue.remove(&self.id)
    }

    pub fn record_error<T: std::fmt::Display>(&self, error: T) -> Result<(), DatastoreError> {
        let update_function = |mut entry: QueueEntry| {
            let error = DownloadError(Utc::now(), error.to_string());
            entry.errors.push(error);
            entry
        };
        self.update_entry(update_function)
    }

    pub fn mark_paused(&self, resume_at: DateTime<Utc>) -> Result<(), DatastoreError> {
        let update_function = |entry: QueueEntry| QueueEntry {
            status: DownloadStatus::Paused(resume_at),
            ..entry
        };
        self.update_entry(update_function)
    }

    pub fn mark_active(&self) -> Result<(), DatastoreError> {
        let update_function = |entry: QueueEntry| QueueEntry {
            status: DownloadStatus::Active,
            ..entry
        };
        self.update_entry(update_function)
    }

    pub fn mark_stopped(&self) -> Result<(), DatastoreError> {
        let update_function = |entry: QueueEntry| QueueEntry {
            status: DownloadStatus::Stopped,
            ..entry
        };
        self.update_entry(update_function)
    }

    pub fn mark_pending(&self) -> Result<(), DatastoreError> {
        let update_function = |entry: QueueEntry| QueueEntry {
            status: DownloadStatus::Pending,
            ..entry
        };
        self.update_entry(update_function)
    }

    fn update_entry(
        &self,
        update_function: impl Fn(QueueEntry) -> QueueEntry,
    ) -> Result<(), DatastoreError> {
        self.queue.update(&self.id, update_function)
    }
}

impl QueueItem {
    pub fn podcast(self) -> Option<Podcast> {
        match self {
            Self::Podcast(x) => Some(x),
            _ => None,
        }
    }

    pub fn twitch(self) -> Option<TwitchVideo> {
        match self {
            Self::Twitch(x) => Some(x),
            _ => None,
        }
    }

    pub fn youtube(self) -> Option<YouTubeVideo> {
        match self {
            Self::YouTube(x) => Some(x),
            _ => None,
        }
    }
}
