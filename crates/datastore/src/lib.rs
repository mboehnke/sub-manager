#[cfg(test)]
mod tests;

mod history;
mod queue;
mod rules;
mod settings;
mod subscriptions;
mod table;

use std::marker::PhantomData;

use camino::{Utf8Path, Utf8PathBuf};
use chrono::{DateTime, Utc};
pub use podcast_dl::Podcast;
use serde::{Deserialize, Serialize};
use thiserror::Error;
pub use twitch_dl::TwitchVideo;
pub use youtube::YouTubeVideo;
use DatastoreError::*;

const FLUSH_EVERY_MS: Option<u64> = Some(60_000);
const CACHE_CAPACITY: u64 = 500_000_000;
const USE_COMPRESSION: bool = true;
const COMPRESSION_FACTOR: i32 = 10;
const PRINT_PROFILE_ON_DROP: bool = true;

#[derive(Debug, Clone)]
pub struct Datastore {
    database: sled::Db,
    subscriptions: Table<Subscription, ()>,
    settings: Table<String, Setting>,
    history: Table<HistoryEntry, ()>,
    queue: Table<u64, QueueEntry>,
    rules: Table<u64, Rule>,
}

#[derive(Debug, Clone)]
struct Table<K, V> {
    tree: sled::Tree,
    key: PhantomData<K>,
    value: PhantomData<V>,
    name: String,
}

#[derive(Debug, Clone)]
pub struct Subscriptions(Table<Subscription, ()>);

#[derive(Debug, Clone)]
pub struct Settings(Table<String, Setting>);

#[derive(Debug, Clone)]
pub struct History(Table<HistoryEntry, ()>);

#[derive(Debug, Clone)]
pub struct Queue {
    history: Table<HistoryEntry, ()>,
    queue: Table<u64, QueueEntry>,
}

#[derive(Debug, Clone)]
pub struct Rules(Table<u64, Rule>, sled::Db);

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, Hash, PartialOrd, Ord)]
pub enum Subscription {
    Podcast(String),
    Twitch(String),
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub enum Setting {
    Text(String),
    Number(i64),
    Date(DateTime<Utc>),
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub enum Rule {
    Podcast(PodcastRule),
    Twitch(TwitchRule),
    YouTube(YouTubeRule),
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub enum PodcastRule {
    Channel(String, Utf8PathBuf),
    Title(String, Utf8PathBuf),
    Description(String, Utf8PathBuf),
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub enum TwitchRule {
    Channel(String, Utf8PathBuf),
    Title(String, Utf8PathBuf),
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub enum YouTubeRule {
    Channel(String, Utf8PathBuf),
    Title(String, Utf8PathBuf),
    Description(String, Utf8PathBuf),
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub struct QueueEntry {
    pub added: DateTime<Utc>,
    pub status: DownloadStatus,
    pub item: QueueItem,
    pub errors: Vec<DownloadError>,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub enum DownloadStatus {
    Active,
    Pending,
    Paused(DateTime<Utc>),
    Stopped,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub struct DownloadError(pub DateTime<Utc>, pub String);

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub enum QueueItem {
    YouTube(YouTubeVideo),
    Twitch(TwitchVideo),
    Podcast(Podcast),
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, Hash, PartialOrd, Ord)]
pub enum HistoryEntry {
    YouTube(String),
    Twitch(String),
    Podcast(String),
}

#[derive(Debug, Clone)]
pub struct QueueIterator {
    queue: Table<u64, QueueEntry>,
    visited_ids: Vec<u64>,
}

#[derive(Debug, Clone)]
pub struct QueueEntryReference {
    queue: Table<u64, QueueEntry>,
    pub id: u64,
}

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum DatastoreError {
    #[error("could not open database file: {path:?}")]
    DatabaseOpenError {
        source: sled::Error,
        path: Utf8PathBuf,
    },

    #[error("could not open database table : {table:?}")]
    TableOpenError { source: sled::Error, table: String },

    #[error("could not deserialize data into {datatype:?}")]
    DeserializationError {
        source: bincode::Error,
        datatype: &'static str,
    },

    #[error("could not serialize data from {datatype:?}")]
    SerializationError {
        source: bincode::Error,
        datatype: &'static str,
    },

    #[error("could not read entry from database table: {table:?}")]
    DatabaseReadError { source: sled::Error, table: String },

    #[error("could not find entry {key:?} in database table: {table:?}")]
    DatabaseKeyNotFoundError { key: String, table: String },

    #[error("could not write entry to database table: {table:?}")]
    DatabaseWriteError { source: sled::Error, table: String },

    #[error("could not remove entry from database table: {table:?}")]
    DatabaseRemoveError { source: sled::Error, table: String },

    #[error("could not apply transaction to database tables: {tables:?}")]
    DatabaseTransactionError {
        source: sled::transaction::TransactionError,
        tables: Vec<String>,
    },

    #[error("could not generate id")]
    IdGenerationError { source: sled::Error },

    #[error("could not update queue entry: invalid data")]
    QueueEntryUpdateError,

    #[error("could not find entry in {table:?} table: {entry:?}")]
    EntryNotFoundError { entry: String, table: String },
}

impl Datastore {
    pub fn open(path: impl AsRef<Utf8Path>) -> Result<Datastore, DatastoreError> {
        fn table_from<K, V>(db: &sled::Db, name: &str) -> Result<Table<K, V>, DatastoreError> {
            let tree = db
                .open_tree(name.as_bytes())
                .map_err(|source| TableOpenError {
                    source,
                    table: name.to_string(),
                })?;
            let table = Table {
                tree,
                key: PhantomData,
                value: PhantomData,
                name: name.to_string(),
            };
            Ok(table)
        }

        let database = sled::Config::new()
            .path(path.as_ref())
            .cache_capacity(CACHE_CAPACITY)
            .use_compression(USE_COMPRESSION)
            .compression_factor(COMPRESSION_FACTOR)
            .print_profile_on_drop(PRINT_PROFILE_ON_DROP)
            .flush_every_ms(FLUSH_EVERY_MS)
            .open()
            .map_err(|source| DatabaseOpenError {
                source,
                path: path.as_ref().to_path_buf(),
            })?;

        Ok(Datastore {
            subscriptions: table_from(&database, "subscriptions")?,
            settings: table_from(&database, "settings")?,
            history: table_from(&database, "history")?,
            queue: table_from(&database, "queue")?,
            rules: table_from(&database, "rules")?,
            database,
        })
    }

    pub fn subscriptions(&self) -> Subscriptions {
        Subscriptions(self.subscriptions.clone())
    }

    pub fn settings(&self) -> Settings {
        Settings(self.settings.clone())
    }

    pub fn history(&self) -> History {
        History(self.history.clone())
    }

    pub fn queue(&self) -> Queue {
        Queue {
            history: self.history.clone(),
            queue: self.queue.clone(),
        }
    }

    pub fn rules(&self) -> Rules {
        Rules(self.rules.clone(), self.database.clone())
    }
}
