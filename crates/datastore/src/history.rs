use apply::Apply as _;
use podcast_dl::Podcast;
use twitch_dl::TwitchVideo;
use youtube::YouTubeVideo;

use crate::{DatastoreError, History, HistoryEntry, QueueEntry, QueueItem};

impl History {
    pub fn get_all(&self) -> Result<Vec<HistoryEntry>, DatastoreError> {
        self.0.keys()
    }

    /// returns `true` if the entry was new, `false` if it was already in the history
    pub fn insert(&self, entry: impl Into<HistoryEntry>) -> Result<bool, DatastoreError> {
        self.0.insert(entry.into(), ())
    }

    pub fn remove(&self, entry: &HistoryEntry) -> Result<bool, DatastoreError> {
        self.0.remove(entry)
    }
}

impl HistoryEntry {
    pub fn podcast(self) -> Option<String> {
        match self {
            Self::Podcast(x) => Some(x),
            _ => None,
        }
    }

    pub fn twitch(self) -> Option<String> {
        match self {
            Self::Twitch(x) => Some(x),
            _ => None,
        }
    }

    pub fn youtube(self) -> Option<String> {
        match self {
            Self::YouTube(x) => Some(x),
            _ => None,
        }
    }
}

impl From<&QueueEntry> for HistoryEntry {
    fn from(entry: &QueueEntry) -> Self {
        HistoryEntry::from(&entry.item)
    }
}

impl From<&QueueItem> for HistoryEntry {
    fn from(item: &QueueItem) -> Self {
        match item {
            QueueItem::YouTube(inner) => HistoryEntry::from(inner),
            QueueItem::Twitch(inner) => HistoryEntry::from(inner),
            QueueItem::Podcast(inner) => HistoryEntry::from(inner),
        }
    }
}

impl From<&Podcast> for HistoryEntry {
    fn from(podcast: &Podcast) -> Self {
        podcast.audio.to_string().apply(HistoryEntry::Podcast)
    }
}

impl From<&TwitchVideo> for HistoryEntry {
    fn from(video: &TwitchVideo) -> Self {
        video.id.to_string().apply(HistoryEntry::Twitch)
    }
}

impl From<&YouTubeVideo> for HistoryEntry {
    fn from(video: &YouTubeVideo) -> Self {
        video.id.to_string().apply(HistoryEntry::YouTube)
    }
}
