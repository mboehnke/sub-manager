use std::any::type_name;

use hashbrown::HashMap;
use serde::{de::DeserializeOwned, Serialize};
use sled::{transaction::TransactionalTree, IVec};

use crate::{
    DatastoreError::{self, *},
    Table,
};

impl<K, V> Table<K, V>
where
    K: Serialize + DeserializeOwned + std::fmt::Debug + Eq + std::hash::Hash,
    V: Serialize + DeserializeOwned + std::fmt::Debug,
{
    pub fn get(&self, key: &K) -> Result<Option<V>, DatastoreError> {
        let key_bytes = serialize(&key)?;
        let value_bytes = self.tree.get(&key_bytes).map_err(|source| {
            let table = self.name.clone();
            DatabaseReadError { source, table }
        })?;
        let value = match value_bytes {
            Some(bytes) => Some(deserialize(bytes)?),
            None => None,
        };
        Ok(value)
    }

    pub fn keys(&self) -> Result<Vec<K>, DatastoreError> {
        self.iter_raw()
            .map(|bytes| bytes.map(|(key, _)| key).and_then(deserialize))
            .collect()
    }

    pub fn values(&self) -> Result<Vec<V>, DatastoreError> {
        self.iter_raw()
            .map(|bytes| bytes.map(|(_, value)| value).and_then(deserialize))
            .collect()
    }

    pub fn get_all(&self) -> Result<HashMap<K, V>, DatastoreError> {
        self.iter_raw()
            .map(|bytes| bytes.and_then(deserialize_pair))
            .collect()
    }

    /// returns `true` if key was new
    pub fn insert(&self, key: K, value: V) -> Result<bool, DatastoreError> {
        let key_bytes = serialize(&key)?;
        let value_bytes = serialize(&value)?;
        let is_new = self
            .tree
            .insert(key_bytes, value_bytes)
            .map(|old_value| old_value.is_none())
            .map_err(|source| {
                let table = self.name.clone();
                DatabaseWriteError { source, table }
            })?;
        Ok(is_new)
    }

    pub fn update<F: Fn(V) -> V>(&self, key: &K, update_function: F) -> Result<(), DatastoreError> {
        let key_bytes = serialize(&key)?;
        let try_update = |tree: &TransactionalTree| {
            let value_bytes = tree.get(&key_bytes)?;
            let value = value_bytes.and_then(|bytes| deserialize(bytes).ok());
            #[allow(clippy::redundant_closure)]
            let updated_value = value.map(|v| update_function(v));
            let updated_value_bytes = updated_value.and_then(|v| serialize(&v).ok());
            if let Some(updated_value_bytes) = updated_value_bytes {
                tree.insert(&key_bytes, updated_value_bytes)?;
                Ok(true)
            } else {
                Ok(false)
            }
        };
        let updated = self.tree.transaction(try_update).map_err(|source| {
            let tables = vec![self.name.clone()];
            DatabaseTransactionError { source, tables }
        })?;
        updated.then_some(()).ok_or(QueueEntryUpdateError)
    }

    pub fn swap(&self, key1: K, key2: K) -> Result<(), DatastoreError> {
        let key1_bytes = serialize(&key1)?;
        let key2_bytes = serialize(&key2)?;
        let try_swap = |tree: &TransactionalTree| {
            if let Some(value1) = tree.get(&key1_bytes)? {
                if let Some(value2) = tree.get(&key2_bytes)? {
                    tree.insert(&key1_bytes, value2)?;
                    tree.insert(&key2_bytes, value1)?;
                }
            }
            Ok(())
        };
        self.tree
            .transaction::<_, _, sled::Error>(try_swap)
            .map_err(|source| {
                let tables = vec![self.name.clone()];
                DatabaseTransactionError { source, tables }
            })?;
        Ok(())
    }

    pub fn remove(&self, key: &K) -> Result<bool, DatastoreError> {
        let key_bytes = serialize(key)?;
        self.tree
            .remove(&key_bytes)
            .map(|old_value| old_value.is_some())
            .map_err(|source| {
                let table = self.name.clone();
                DatabaseRemoveError { source, table }
            })
    }

    fn iter_raw(&self) -> impl Iterator<Item = Result<(IVec, IVec), DatastoreError>> + '_ {
        self.tree.iter().map(move |result| {
            result.map_err(|source| {
                let table = self.name.clone();
                DatabaseReadError { source, table }
            })
        })
    }
}

// for now we just serialize data in spite of the added overhead,
// it places the least restrictions on datatypes and seems the hardest to actually mess up
// (performance is not that critical for now)
pub(crate) fn serialize<T: Serialize>(value: &T) -> Result<IVec, DatastoreError> {
    bincode::serialize(value)
        .map(IVec::from)
        .map_err(|source| SerializationError {
            source,
            datatype: type_name::<T>(),
        })
}

pub(crate) fn deserialize<T: DeserializeOwned>(bytes: IVec) -> Result<T, DatastoreError> {
    bincode::deserialize(&bytes).map_err(|source| DeserializationError {
        source,
        datatype: type_name::<T>(),
    })
}

pub(crate) fn deserialize_pair<A, B>(
    (bytes_a, bytes_b): (IVec, IVec),
) -> Result<(A, B), DatastoreError>
where
    A: DeserializeOwned,
    B: DeserializeOwned,
{
    let value_a = deserialize(bytes_a)?;
    let value_b = deserialize(bytes_b)?;
    Ok((value_a, value_b))
}
