use crate::{DatastoreError, Setting, Settings};

use chrono::{DateTime, Utc};
use hashbrown::HashMap;

impl Settings {
    pub fn get_all(&self) -> Result<HashMap<String, Setting>, DatastoreError> {
        self.0.get_all()
    }

    pub fn get(&self, key: &str) -> Result<Setting, DatastoreError> {
        self.0
            .get(&key.to_string())?
            .ok_or_else(|| DatastoreError::EntryNotFoundError {
                entry: key.to_string(),
                table: self.0.name.to_string(),
            })
    }

    pub fn insert(&self, key: &str, value: impl Into<Setting>) -> Result<bool, DatastoreError> {
        self.0.insert(key.to_string(), value.into())
    }

    pub fn get_or_insert(
        &self,
        key: &str,
        default_value: impl Into<Setting>,
    ) -> Result<Setting, DatastoreError> {
        if let Some(value) = self.0.get(&key.to_string())? {
            Ok(value)
        } else {
            let value = default_value.into();
            self.0.insert(key.to_string(), value.clone())?;
            Ok(value)
        }
    }

    pub fn get_insert_or_default_number(&self, key: &str, default_value: i64) -> i64 {
        self.get_or_insert(key, default_value)
            .ok()
            .and_then(Setting::number)
            .unwrap_or(default_value)
    }

    pub fn get_insert_or_default_text(&self, key: &str, default_value: &str) -> String {
        self.get_or_insert(key, default_value)
            .ok()
            .and_then(Setting::text)
            .unwrap_or_else(|| default_value.into())
    }

    pub fn get_insert_or_default_date(
        &self,
        key: &str,
        default_value: DateTime<Utc>,
    ) -> DateTime<Utc> {
        self.get_or_insert(key, default_value)
            .ok()
            .and_then(Setting::date)
            .unwrap_or(default_value)
    }

    pub fn remove(&self, key: &str) -> Result<bool, DatastoreError> {
        self.0.remove(&key.to_string())
    }
}

impl Setting {
    pub fn text(self) -> Option<String> {
        match self {
            Self::Text(x) => Some(x),
            _ => None,
        }
    }

    pub fn number(self) -> Option<i64> {
        match self {
            Self::Number(x) => Some(x),
            _ => None,
        }
    }

    pub fn date(self) -> Option<DateTime<Utc>> {
        match self {
            Self::Date(x) => Some(x),
            _ => None,
        }
    }
}

impl From<i64> for Setting {
    fn from(inner: i64) -> Self {
        Setting::Number(inner)
    }
}

impl From<DateTime<Utc>> for Setting {
    fn from(inner: DateTime<Utc>) -> Self {
        Setting::Date(inner)
    }
}

impl From<&str> for Setting {
    fn from(inner: &str) -> Self {
        Setting::Text(inner.to_string())
    }
}

impl From<&String> for Setting {
    fn from(inner: &String) -> Self {
        Setting::Text(inner.to_string())
    }
}

impl From<String> for Setting {
    fn from(inner: String) -> Self {
        Setting::Text(inner)
    }
}
