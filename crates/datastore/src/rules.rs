use crate::{
    DatastoreError::{self, *},
    PodcastRule, Rule, Rules, TwitchRule, YouTubeRule,
};

use hashbrown::HashMap;

impl Rules {
    pub fn get_all(&self) -> Result<HashMap<u64, Rule>, DatastoreError> {
        self.0.get_all()
    }

    pub fn insert(&self, value: Rule) -> Result<bool, DatastoreError> {
        let key = self
            .1
            .generate_id()
            .map_err(|source| IdGenerationError { source })?;
        self.0.insert(key, value)
    }

    pub fn swap(&self, key1: u64, key2: u64) -> Result<(), DatastoreError> {
        self.0.swap(key1, key2)
    }

    pub fn remove(&self, key: u64) -> Result<bool, DatastoreError> {
        self.0.remove(&key)
    }
}

impl Rule {
    pub fn podcast(self) -> Option<PodcastRule> {
        match self {
            Self::Podcast(x) => Some(x),
            _ => None,
        }
    }

    pub fn twitch(self) -> Option<TwitchRule> {
        match self {
            Self::Twitch(x) => Some(x),
            _ => None,
        }
    }

    pub fn youtube(self) -> Option<YouTubeRule> {
        match self {
            Self::YouTube(x) => Some(x),
            _ => None,
        }
    }
}
