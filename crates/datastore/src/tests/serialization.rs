//! feeble attempt to establish and test compatibility with the binary format of the database
//!
//! far from perfect but should at least be able to catch *some* mistakes

use crate::table::{deserialize, serialize};
use crate::{QueueEntry, Rule, Setting, Subscription, HistoryEntry};

use paste::paste;

macro_rules! serialization_tests {
    ($type:ty, $file:expr) => {
        paste! {
            const [<$type:upper S>] :[u8; include_bytes!($file).len()] = *include_bytes!($file);

            #[test]
            fn [<serialize_ $type:lower>]() {
                let data: Vec<($type, Vec<u8>)> = bincode::deserialize(&[<$type:upper S>]).unwrap();
                for (value, value_s) in data {
                    let serialized = serialize(&value).unwrap();
                    assert_eq!(&serialized, &value_s)
                }
            }

            #[test]
            fn [<deserialize_ $type:lower>]() {
                let data: Vec<($type, Vec<u8>)> = bincode::deserialize(&[<$type:upper S>]).unwrap();
                for (value, value_s) in data {
                    let serialized = sled::IVec::from(value_s.to_vec());
                    let deserialized: $type = deserialize(serialized).unwrap();
                    assert_eq!(&deserialized, &value)
                }
            }
        }
    };
}

serialization_tests!(u64, "data/u64.bin");
serialization_tests!(String, "data/String.bin");
serialization_tests!(Setting, "data/Setting.bin");
serialization_tests!(Subscription, "data/Subscription.bin");
serialization_tests!(Rule, "data/Rule.bin");
serialization_tests!(HistoryEntry, "data/HistoryEntry.bin");
serialization_tests!(QueueEntry, "data/QueueEntry.bin");
