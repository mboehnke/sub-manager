use itertools::Itertools;

use crate::tests::test_data;
use crate::Setting;

#[test]
fn into_number() {
    let (numbers, text, dates) = data();
    assert!(numbers.map(Setting::number).all(|x| x.is_some()));
    assert!(text.map(Setting::number).all(|x| x.is_none()));
    assert!(dates.map(Setting::number).all(|x| x.is_none()));
}

#[test]
fn into_text() {
    let (numbers, text, dates) = data();
    assert!(numbers.map(Setting::text).all(|x| x.is_none()));
    assert!(text.map(Setting::text).all(|x| x.is_some()));
    assert!(dates.map(Setting::text).all(|x| x.is_none()));
}

#[test]
fn into_date() {
    let (numbers, text, dates) = data();
    assert!(numbers.map(Setting::date).all(|x| x.is_none()));
    assert!(text.map(Setting::date).all(|x| x.is_none()));
    assert!(dates.map(Setting::date).all(|x| x.is_some()));
}

#[test]
fn from_i64() {
    for val in test_data::i64s() {
        let a = Setting::Number(val);
        let b = Setting::from(val);
        assert_eq!(a, b)
    }
}

#[test]
fn from_datetime() {
    for val in test_data::dates() {
        let a = Setting::Date(val);
        let b = Setting::from(val);
        assert_eq!(a, b)
    }
}

#[test]
fn from_str() {
    let vals = test_data::strings().collect_vec();
    for val in vals.iter().map(|s| s.as_str()) {
        let a = Setting::Text(val.to_string());
        let b = Setting::from(val);
        assert_eq!(a, b)
    }
}

#[test]
fn from_string() {
    for val in test_data::strings() {
        let a = Setting::Text(val.clone());
        let b = Setting::from(val);
        assert_eq!(a, b)
    }
}

#[test]
fn from_string_ref() {
    for val in test_data::strings() {
        let a = Setting::Text(val.clone());
        let b = Setting::from(&val);
        assert_eq!(a, b)
    }
}

fn data() -> (
    impl Iterator<Item = Setting>,
    impl Iterator<Item = Setting>,
    impl Iterator<Item = Setting>,
) {
    let numbers = test_data::i64s().map(Setting::Number);
    let text = test_data::strings().map(Setting::Text);
    let dates = test_data::dates().map(Setting::Date);
    (numbers, text, dates)
}
