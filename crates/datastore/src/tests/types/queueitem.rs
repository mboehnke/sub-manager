use crate::tests::test_data;
use crate::QueueItem;

#[test]
fn into_podcast() {
    let (podcasts, twitch, youtube) = data();
    assert!(podcasts.map(QueueItem::podcast).all(|x| x.is_some()));
    assert!(twitch.map(QueueItem::podcast).all(|x| x.is_none()));
    assert!(youtube.map(QueueItem::podcast).all(|x| x.is_none()));
}

#[test]
fn into_twitch() {
    let (podcasts, twitch, youtube) = data();
    assert!(podcasts.map(QueueItem::twitch).all(|x| x.is_none()));
    assert!(twitch.map(QueueItem::twitch).all(|x| x.is_some()));
    assert!(youtube.map(QueueItem::twitch).all(|x| x.is_none()));
}

#[test]
fn into_youtube() {
    let (podcasts, twitch, youtube) = data();
    assert!(podcasts.map(QueueItem::youtube).all(|x| x.is_none()));
    assert!(twitch.map(QueueItem::youtube).all(|x| x.is_none()));
    assert!(youtube.map(QueueItem::youtube).all(|x| x.is_some()));
}

// # QueueEntry
// - from(item: QueueItem) -> Self

#[test]
fn from_podcast() {
    for val in test_data::podcasts() {
        let a = QueueItem::Podcast(val.clone());
        let b = QueueItem::from(val);
        assert_eq!(a, b)
    }
}

#[test]
fn from_twitchvideo() {
    for val in test_data::twitchvideos() {
        let a = QueueItem::Twitch(val.clone());
        let b = QueueItem::from(val);
        assert_eq!(a, b)
    }
}

#[test]
fn from_youtubevideo() {
    for val in test_data::youtubevideos() {
        let a = QueueItem::YouTube(val.clone());
        let b = QueueItem::from(val);
        assert_eq!(a, b)
    }
}

fn data() -> (
    impl Iterator<Item = QueueItem>,
    impl Iterator<Item = QueueItem>,
    impl Iterator<Item = QueueItem>,
) {
    let podcasts = test_data::podcasts().map(QueueItem::Podcast);
    let twitch = test_data::twitchvideos().map(QueueItem::Twitch);
    let youtube = test_data::youtubevideos().map(QueueItem::YouTube);
    (podcasts, twitch, youtube)
}
