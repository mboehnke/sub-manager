use crate::tests::test_data;
use crate::Rule;

#[test]
fn into_podcast() {
    let (podcasts, twitch, youtube) = data();
    assert!(podcasts.map(Rule::podcast).all(|x| x.is_some()));
    assert!(twitch.map(Rule::podcast).all(|x| x.is_none()));
    assert!(youtube.map(Rule::podcast).all(|x| x.is_none()));
}

#[test]
fn into_twitch() {
    let (podcasts, twitch, youtube) = data();
    assert!(podcasts.map(Rule::twitch).all(|x| x.is_none()));
    assert!(twitch.map(Rule::twitch).all(|x| x.is_some()));
    assert!(youtube.map(Rule::twitch).all(|x| x.is_none()));
}

#[test]
fn into_youtube() {
    let (podcasts, twitch, youtube) = data();
    assert!(podcasts.map(Rule::youtube).all(|x| x.is_none()));
    assert!(twitch.map(Rule::youtube).all(|x| x.is_none()));
    assert!(youtube.map(Rule::youtube).all(|x| x.is_some()));
}

fn data() -> (
    impl Iterator<Item = Rule>,
    impl Iterator<Item = Rule>,
    impl Iterator<Item = Rule>,
) {
    let podcasts = test_data::podcastrules().map(Rule::Podcast);
    let twitch = test_data::twitchrules().map(Rule::Twitch);
    let youtube = test_data::youtuberules().map(Rule::YouTube);
    (podcasts, twitch, youtube)
}
