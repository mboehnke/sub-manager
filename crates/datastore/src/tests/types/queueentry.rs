use crate::{
    tests::test_data::{self, dates},
    QueueEntry,
};

#[test]
fn from_queueitem() {
    let date = dates().next().unwrap();
    for item in test_data::queueitems() {
        let a = QueueEntry {
            added: date,
            status: crate::DownloadStatus::Pending,
            item: item.clone(),
            errors: Vec::new(),
        };
        let mut b = QueueEntry::from(item);
        b.added = date;
        assert_eq!(a, b)
    }
}
