use crate::tests::test_data;
use crate::Subscription;

#[test]
fn into_podcast() {
    let (podcasts, twitch) = data();
    assert!(podcasts.map(Subscription::podcast).all(|x| x.is_some()));
    assert!(twitch.map(Subscription::podcast).all(|x| x.is_none()));
}

#[test]
fn into_twitch() {
    let (podcasts, twitch) = data();
    assert!(podcasts.map(Subscription::twitch).all(|x| x.is_none()));
    assert!(twitch.map(Subscription::twitch).all(|x| x.is_some()));
}

fn data() -> (
    impl Iterator<Item = Subscription>,
    impl Iterator<Item = Subscription>,
) {
    let podcasts = test_data::strings().map(Subscription::Podcast);
    let twitch = test_data::strings().map(Subscription::Twitch);
    (podcasts, twitch)
}
