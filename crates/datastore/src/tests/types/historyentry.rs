use crate::tests::test_data;
use crate::HistoryEntry;

#[test]
fn into_podcast() {
    let (podcasts, twitch, youtube) = data();
    assert!(podcasts.map(HistoryEntry::podcast).all(|x| x.is_some()));
    assert!(twitch.map(HistoryEntry::podcast).all(|x| x.is_none()));
    assert!(youtube.map(HistoryEntry::podcast).all(|x| x.is_none()));
}

#[test]
fn into_twitch() {
    let (podcasts, twitch, youtube) = data();
    assert!(podcasts.map(HistoryEntry::twitch).all(|x| x.is_none()));
    assert!(twitch.map(HistoryEntry::twitch).all(|x| x.is_some()));
    assert!(youtube.map(HistoryEntry::twitch).all(|x| x.is_none()));
}

#[test]
fn into_youtube() {
    let (podcasts, twitch, youtube) = data();
    assert!(podcasts.map(HistoryEntry::youtube).all(|x| x.is_none()));
    assert!(twitch.map(HistoryEntry::youtube).all(|x| x.is_none()));
    assert!(youtube.map(HistoryEntry::youtube).all(|x| x.is_some()));
}

fn data() -> (
    impl Iterator<Item = HistoryEntry>,
    impl Iterator<Item = HistoryEntry>,
    impl Iterator<Item = HistoryEntry>,
) {
    let podcasts = test_data::strings().map(HistoryEntry::Podcast);
    let twitch = test_data::strings().map(HistoryEntry::Twitch);
    let youtube = test_data::strings().map(HistoryEntry::YouTube);
    (podcasts, twitch, youtube)
}

#[test]
fn from_queueentry() {
    for entry in test_data::queueentrys() {
        let a = match entry.item {
            crate::QueueItem::YouTube(ref x) => HistoryEntry::YouTube(x.id.clone()),
            crate::QueueItem::Twitch(ref x) => HistoryEntry::Twitch(x.id.clone()),
            crate::QueueItem::Podcast(ref x) => HistoryEntry::Podcast(x.audio.clone()),
        };
        let b = HistoryEntry::from(&entry);
        assert_eq!(a, b)
    }
}

#[test]
fn from_queueitem() {
    for item in test_data::queueitems() {
        let a = match item {
            crate::QueueItem::YouTube(ref x) => HistoryEntry::YouTube(x.id.clone()),
            crate::QueueItem::Twitch(ref x) => HistoryEntry::Twitch(x.id.clone()),
            crate::QueueItem::Podcast(ref x) => HistoryEntry::Podcast(x.audio.clone()),
        };
        let b = HistoryEntry::from(&item);
        assert_eq!(a, b)
    }
}

#[test]
fn from_podcast() {
    for podcast in test_data::podcasts() {
        let a = HistoryEntry::Podcast(podcast.audio.clone());
        let b = HistoryEntry::from(&podcast);
        assert_eq!(a, b)
    }
}

#[test]
fn from_twitchvideo() {
    for twitchvideo in test_data::twitchvideos() {
        let a = HistoryEntry::Twitch(twitchvideo.id.clone());
        let b = HistoryEntry::from(&twitchvideo);
        assert_eq!(a, b)
    }
}

#[test]
fn from_youtubevideo() {
    for youtubevideo in test_data::youtubevideos() {
        let a = HistoryEntry::YouTube(youtubevideo.id.clone());
        let b = HistoryEntry::from(&youtubevideo);
        assert_eq!(a, b)
    }
}
