mod history;
mod queue;
mod rules;
mod settings;
mod subscriptions;

use std::env::temp_dir;

use apply::Apply as _;
use camino::Utf8PathBuf;
use hashbrown::HashMap;

use crate::tests::test_data;
use crate::{Datastore, HistoryEntry, QueueEntry, Rule, Setting, Subscription};

const TEST_DB: &str = "datastore-test";

#[derive(Default, Clone, Debug)]
pub struct Db {
    pub settings: HashMap<String, Setting>,
    pub history: Vec<HistoryEntry>,
    pub subscriptions: Vec<Subscription>,
    pub rules: HashMap<u64, Rule>,
    pub queue: HashMap<u64, QueueEntry>,
}

pub trait EqWeak {
    fn eq_weak(self, _: Self) -> bool;
}
#[test]
fn create_new() {
    let db = new_db("create_new");

    assert!(db_path("create_new").is_dir());
    assert!(db_path("create_new").join("db").is_file());
    assert!(db_path("create_new").join("conf").is_file());

    assert!(Db::from(&db).is_empty())
}

pub fn new_db(s: &str) -> Datastore {
    delete_db(s);

    Datastore::open(&db_path(s)).unwrap_or_else(|_| panic!("could not open test db: {s}"))
}

pub fn assert_eq_weak<T: EqWeak>(a: T, b: T) {
    assert!(a.eq_weak(b))
}

impl Db {
    pub fn is_empty(&self) -> bool {
        self.settings.is_empty()
            && self.history.is_empty()
            && self.subscriptions.is_empty()
            && self.rules.is_empty()
            && self.queue.is_empty()
    }
}

impl From<&Datastore> for Db {
    fn from(db: &Datastore) -> Self {
        Db {
            settings: db
                .settings()
                .get_all()
                .expect("could not read settings table from test db"),
            history: db
                .history()
                .get_all()
                .expect("could not read history table from test db"),
            subscriptions: db
                .subscriptions()
                .get_all()
                .expect("could not read subscriptions table from test db"),
            rules: db
                .rules()
                .get_all()
                .expect("could not read rules table from test db"),
            queue: db
                .queue()
                .get_all()
                .expect("could not read queue table from test db"),
        }
    }
}

impl<T: Ord> EqWeak for Vec<T> {
    fn eq_weak(mut self, mut c: Self) -> bool {
        self.sort_unstable();
        c.sort_unstable();

        self == c
    }
}

impl EqWeak for Db {
    fn eq_weak(mut self, mut c: Self) -> bool {
        let date = test_data::dates().next().unwrap();
        self.queue.values_mut().for_each(|mut e| e.added = date);
        c.queue.values_mut().for_each(|mut e| e.added = date);

        self.settings == c.settings
            && self.history.eq_weak(c.history)
            && self.subscriptions.eq_weak(c.subscriptions)
            && self.rules == c.rules
            && self.queue == c.queue
    }
}

fn db_path(s: &str) -> Utf8PathBuf {
    temp_dir()
        .join(TEST_DB)
        .apply(Utf8PathBuf::from_path_buf)
        .unwrap_or_else(|_| panic!("invalid db path: {TEST_DB}"))
        .join(s)
}

fn delete_db(s: &str) {
    if db_path(s).exists() {
        std::fs::remove_dir_all(&db_path(s))
            .unwrap_or_else(|_| panic!("could not delete test db: {s}"));
    }
}
