use super::{assert_eq_weak, new_db, Db};
use crate::tests::test_data;

#[test]
fn insert() {
    let (data, db) = init_db("subscriptions_insert");
    assert_eq_weak(data, Db::from(&db));
}

#[test]
fn remove() {
    let (mut data, db) = init_db("subscriptions_remove");

    while let Some(sub) = data.subscriptions.pop() {
        db.subscriptions()
            .remove(&sub)
            .expect("could not remove data from subscriptions table");

        assert_eq_weak(data.clone(), Db::from(&db));
    }
    assert_eq_weak(data, Db::default());
}

fn init_db(s: &str) -> (Db, crate::Datastore) {
    let db = new_db(s);
    let data = Db {
        subscriptions: test_data::subscriptions().collect(),
        ..Default::default()
    };
    for d in &data.subscriptions {
        db.subscriptions()
            .insert(d.clone())
            .expect("could not insert data into subscriptions table");
    }
    (data, db)
}
