use super::{assert_eq_weak, new_db, Db};
use crate::tests::test_data;

#[test]
fn insert() {
    let (data, db) = init_db("history_insert");
    assert_eq_weak(data, Db::from(&db));
}

#[test]
fn remove() {
    let (mut data, db) = init_db("history_remove");

    while let Some(entry) = data.history.pop() {
        db.history()
            .remove(&entry)
            .expect("could not remove data from history table");

        assert_eq_weak(data.clone(), Db::from(&db));
    }
    assert_eq_weak(data, Db::default());
}

fn init_db(s: &str) -> (Db, crate::Datastore) {
    let db = new_db(s);
    let data = Db {
        history: test_data::historyentrys().collect(),
        ..Default::default()
    };
    for d in &data.history {
        db.history()
            .insert(d.clone())
            .expect("could not insert data into history table");
    }
    (data, db)
}
