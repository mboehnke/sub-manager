use hashbrown::HashMap;

use super::{assert_eq_weak, new_db, Db};
use crate::{tests::test_data, DatastoreError};

#[test]
fn insert() {
    let (data, db) = init_db("settings_insert");
    assert_eq_weak(data, Db::from(&db));
}

#[test]
fn get() {
    let (data, db) = init_db("settings_get");

    for (key, value) in data.settings {
        let v = db.settings().get(&key).unwrap();
        assert_eq!(value, v);
    }
}

#[test]
fn get_or_insert() {
    let db = new_db("settings_get_or_insert");
    for (i, value) in test_data::settings().enumerate() {
        let key = i.to_string();
        let before = db.settings().get(&key);
        assert!(matches!(
            before,
            Err(DatastoreError::EntryNotFoundError { entry: _, table: _ })
        ));
        let inserted = db.settings().get_or_insert(&key, value.clone()).unwrap();
        assert_eq!(inserted, value);
        let after = db.settings().get(&key).unwrap();
        assert_eq!(after, value);
    }
}

#[test]
fn remove() {
    let (mut data, db) = init_db("settings_remove");

    while let Some(key) = data.settings.keys().next().cloned() {
        data.settings.remove(&key);
        db.settings()
            .remove(&key)
            .expect("could not remove data from settings table");

        assert_eq_weak(data.clone(), Db::from(&db));
    }
    assert_eq_weak(data, Db::default());
}

fn init_db(s: &str) -> (Db, crate::Datastore) {
    let db = new_db(s);
    let data = Db {
        settings: test_data::settings()
            .enumerate()
            .map(|(k, v)| (k.to_string(), v))
            .collect::<HashMap<_, _>>(),
        ..Default::default()
    };
    for (k, v) in &data.settings {
        db.settings()
            .insert(k, v.clone())
            .expect("could not insert data into settings table");
    }
    (data, db)
}
