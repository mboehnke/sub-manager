use hashbrown::HashMap;
use itertools::Itertools;

use super::{assert_eq_weak, new_db, Db};
use crate::tests::test_data;
use crate::{
    DownloadError, DownloadStatus, HistoryEntry, QueueEntry, QueueEntryReference, QueueItem,
};

#[test]
fn iter_len() {
    let (data, db) = init_db("queue_iter_len");
    let iter_len = db.queue().iter().collect_vec().len();
    assert_eq!(iter_len, data.queue.len())
}

#[test]
fn entry() {
    let date = test_data::dates().next().unwrap();
    let (mut data, db) = init_db("queue_entry");
    data.queue = db.queue().get_all().unwrap();
    for (id, entry) in data.queue.into_iter() {
        let entry_a = QueueEntry {
            added: date,
            ..entry
        };
        let entry_b = QueueEntry {
            added: date,
            ..db.queue().entry(id).get().unwrap()
        };
        assert_eq!(entry_a, entry_b);
    }
}

#[test]
fn get_historyentries() {
    let (data, db) = init_db("queue_get_historyentries");
    let uids_a = data.queue.values().map(HistoryEntry::from).collect_vec();
    let uids_b = db.queue().get_all_as_historyentry().unwrap();

    assert_eq_weak(uids_a, uids_b)
}

#[test]
fn insert() {
    let date = test_data::dates().next().unwrap();
    let entries = data()
        .queue
        .values()
        .cloned()
        .map(|mut entry| {
            entry.added = date;
            entry
        })
        .collect_vec();
    let db = new_db("queue_insert");
    let mut entries_a = Vec::new();
    let mut data_a = Db::default();
    for entry in entries {
        let history_entry = match entry.item {
            QueueItem::Podcast(ref inner) => HistoryEntry::Podcast(inner.audio.clone()),
            QueueItem::YouTube(ref inner) => HistoryEntry::YouTube(inner.id.clone()),
            QueueItem::Twitch(ref inner) => HistoryEntry::Twitch(inner.id.clone()),
        };
        data_a.history.push(history_entry);
        entries_a.push(entry.clone());
        assert!(db.queue().insert(entry.item).unwrap());
        let entries_b = db
            .queue()
            .get_all()
            .unwrap()
            .values()
            .cloned()
            .map(|mut entry| {
                entry.added = date;
                entry
            })
            .collect_vec();

        assert_eq_weak(entries_a.clone(), entries_b);
        let mut data_b = Db::from(&db);
        data_b.queue = HashMap::new();
        assert_eq_weak(data_a.clone(), data_b);
    }
}

#[test]
fn insert_if_new() {
    let date = test_data::dates().next().unwrap();
    let entries = data()
        .queue
        .values()
        .cloned()
        .map(|mut entry| {
            entry.added = date;
            entry
        })
        .collect_vec();
    let db = new_db("queue_insert_if_new");
    let mut entries_a = Vec::new();
    let mut data_a = Db::default();
    for entry in entries.clone() {
        let history_entry = match entry.item {
            QueueItem::Podcast(ref inner) => HistoryEntry::Podcast(inner.audio.clone()),
            QueueItem::YouTube(ref inner) => HistoryEntry::YouTube(inner.id.clone()),
            QueueItem::Twitch(ref inner) => HistoryEntry::Twitch(inner.id.clone()),
        };
        data_a.history.push(history_entry);
        entries_a.push(entry.clone());
        assert!(db.queue().insert_if_new(entry.item).unwrap());
        let entries_b = db
            .queue()
            .get_all()
            .unwrap()
            .values()
            .cloned()
            .map(|mut entry| {
                entry.added = date;
                entry
            })
            .collect_vec();
        assert_eq_weak(entries_a.clone(), entries_b);
        let mut data_b = Db::from(&db);
        data_b.queue = HashMap::new();
        assert_eq_weak(data_a.clone(), data_b);
    }
    for entry in entries {
        assert!(!db.queue().insert_if_new(entry.item).unwrap());
        let entries_b = db
            .queue()
            .get_all()
            .unwrap()
            .values()
            .cloned()
            .map(|mut entry| {
                entry.added = date;
                entry
            })
            .collect_vec();
        assert_eq_weak(entries_a.clone(), entries_b);
        let mut data_b = Db::from(&db);
        data_b.queue = HashMap::new();
        assert_eq_weak(data_a.clone(), data_b);
    }
}

#[test]
fn ref_get() {
    let (mut data, db) = init_db("queue_ref_get");
    let date = test_data::dates().next().unwrap();
    data.queue.values_mut().for_each(|mut e| e.added = date);

    for queue_ref in db.queue().iter() {
        let mut entry = queue_ref.get().unwrap();
        entry.added = date;
        assert!(data.queue.values().any(|e| e == &entry))
    }
}

#[test]
fn ref_remove() {
    let (mut data, db) = init_db("queue_ref_remove");
    data.queue = db.queue().get_all().unwrap();
    let date = test_data::dates().next().unwrap();
    for queue_ref in db.queue().iter() {
        let entry = queue_ref.get().unwrap();
        data.queue = data
            .queue
            .into_iter()
            .filter(|(_, e)| {
                QueueEntry {
                    added: date,
                    ..e.clone()
                } != QueueEntry {
                    added: date,
                    ..entry.clone()
                }
            })
            .collect();
        queue_ref.remove().unwrap();

        assert_eq_weak(data.clone(), Db::from(&db));
    }
}

#[test]
fn ref_record_error() {
    let (_, db) = init_db("queue_ref_record_error");
    let refs: Vec<QueueEntryReference> = db.queue().iter().collect_vec();
    let date = test_data::dates().next().unwrap();
    let msg = test_data::strings().max_by_key(|s| s.len()).unwrap();

    for queue_ref in refs {
        let entry_before = queue_ref.get().unwrap();
        let mut entry_changed = entry_before.clone();
        entry_changed.errors.push(DownloadError(date, msg.clone()));
        queue_ref.record_error(&msg).unwrap();
        let mut entry_after = queue_ref.get().unwrap();
        entry_after.errors = entry_after
            .errors
            .iter()
            .map(|DownloadError(_, m)| DownloadError(date, m.clone()))
            .collect();
        assert_eq!(entry_changed, entry_after)
    }
}

#[test]
fn ref_mark_active() {
    let (_, db) = init_db("queue_ref_mark_active");
    let refs: Vec<QueueEntryReference> = db.queue().iter().collect_vec();

    for queue_ref in refs {
        let entry_before = queue_ref.get().unwrap();
        let entry_changed = QueueEntry {
            status: DownloadStatus::Active,
            ..entry_before.clone()
        };
        queue_ref.mark_active().unwrap();
        let entry_after = queue_ref.get().unwrap();
        assert_eq!(entry_changed, entry_after)
    }
}

#[test]
fn ref_mark_paused() {
    let (_, db) = init_db("queue_ref_mark_paused");
    let refs: Vec<QueueEntryReference> = db.queue().iter().collect_vec();
    let date = test_data::dates().next().unwrap();

    for queue_ref in refs {
        let entry_before = queue_ref.get().unwrap();
        let entry_changed = QueueEntry {
            status: DownloadStatus::Paused(date),
            ..entry_before.clone()
        };
        queue_ref.mark_paused(date).unwrap();
        let entry_after = queue_ref.get().unwrap();
        assert_eq!(entry_changed, entry_after)
    }
}

#[test]
fn ref_mark_stopped() {
    let (_, db) = init_db("queue_ref_mark_stopped");
    let refs: Vec<QueueEntryReference> = db.queue().iter().collect_vec();

    for queue_ref in refs {
        let entry_before = queue_ref.get().unwrap();
        let entry_changed = QueueEntry {
            status: DownloadStatus::Stopped,
            ..entry_before.clone()
        };
        queue_ref.mark_stopped().unwrap();
        let entry_after = queue_ref.get().unwrap();
        assert_eq!(entry_changed, entry_after)
    }
}

#[test]
fn ref_mark_pending() {
    let (_, db) = init_db("queue_ref_mark_pending");
    let refs: Vec<QueueEntryReference> = db.queue().iter().collect_vec();

    for queue_ref in refs {
        let entry_before = queue_ref.get().unwrap();
        let entry_changed = QueueEntry {
            status: DownloadStatus::Pending,
            ..entry_before.clone()
        };
        queue_ref.mark_pending().unwrap();
        let entry_after = queue_ref.get().unwrap();
        assert_eq!(entry_changed, entry_after)
    }
}

fn data() -> Db {
    let podcasts = test_data::podcasts()
        .enumerate()
        .map(|(i, p)| crate::Podcast {
            audio: i.to_string(),
            ..p
        })
        .map(QueueItem::from);
    let twitch_videos = test_data::twitchvideos()
        .enumerate()
        .map(|(i, t)| crate::TwitchVideo {
            id: i.to_string(),
            ..t
        })
        .map(QueueItem::from);
    let youtube_videos = test_data::youtubevideos()
        .enumerate()
        .map(|(i, y)| crate::YouTubeVideo {
            id: i.to_string(),
            ..y
        })
        .map(QueueItem::from);

    let mut data = Db {
        queue: podcasts
            .chain(twitch_videos)
            .chain(youtube_videos)
            .map(QueueEntry::from)
            .enumerate()
            .map(|(id, entry)| (id as u64, entry))
            .collect(),
        ..Default::default()
    };
    data.history = data.queue.values().map(HistoryEntry::from).collect();
    data
}

fn init_db(s: &str) -> (Db, crate::Datastore) {
    let data = data();
    let db = new_db(s);

    const ERROR_MSG: &str = "could not insert data into queue table";
    for d in data.queue.values() {
        match &d.item {
            QueueItem::Podcast(d) => db.queue().insert(d.clone()).expect(ERROR_MSG),
            QueueItem::Twitch(d) => db.queue().insert(d.clone()).expect(ERROR_MSG),
            QueueItem::YouTube(d) => db.queue().insert(d.clone()).expect(ERROR_MSG),
        };
    }
    (data, db)
}
