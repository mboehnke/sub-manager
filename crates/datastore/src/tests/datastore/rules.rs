use itertools::Itertools;

use super::{assert_eq_weak, new_db, Db};
use crate::tests::test_data;

#[test]
fn insert() {
    let (data, db) = init_db("rules_insert");
    let data_rules = data.rules.values().cloned().collect_vec();
    let db_rules = Db::from(&db).rules.values().cloned().collect_vec();
    assert_eq_weak(data_rules, db_rules);
}

#[test]
fn remove() {
    let (_, db) = init_db("rules_remove");
    let mut data = Db::from(&db);

    while let Some(key) = data.rules.keys().next().cloned() {
        data.rules.remove(&key);
        db.rules()
            .remove(key)
            .expect("could not remove data from rules table");

        assert_eq_weak(data.clone(), Db::from(&db));
    }
    assert_eq_weak(data, Db::default());
}

#[test]
fn swap() {
    let (_, db) = init_db("rules_swap");
    let mut data = Db::from(&db);

    let mut rules = data.rules.iter();
    let (key1, value1) = rules.next().map(|(k, v)| (*k, v.clone())).unwrap();
    let (key2, value2) = rules.last().map(|(k, v)| (*k, v.clone())).unwrap();
    assert_ne!(key1, key2);
    data.rules.insert(key1, value2);
    data.rules.insert(key2, value1);
    db.rules()
        .swap(key1, key2)
        .expect("could not swap entries in rules table");

    assert_eq_weak(data, Db::from(&db));
}

fn init_db(s: &str) -> (Db, crate::Datastore) {
    let db = new_db(s);
    let data = Db {
        rules: test_data::rules()
            .enumerate()
            .map(|(k, v)| (k as u64, v))
            .collect(),
        ..Default::default()
    };
    for d in data.rules.values() {
        db.rules()
            .insert(d.clone())
            .expect("could not insert data into rules table");
    }
    (data, db)
}
