//! # Warning:
//! this will overwrite all test data and invalidate all serialization tests
//!
//! until the db format changes none of this should be needed anymore

use std::io::Write as _;

use apply::Apply as _;
use itertools::Itertools as _;
use serde::Serialize;

use super::test_data;
use crate::table::serialize;

#[test]
fn generate_all() {
    generate(test_data::u64s());
    generate(test_data::strings());
    generate(test_data::settings());
    generate(test_data::subscriptions());
    generate(test_data::rules());
    generate(test_data::historyentrys());
    generate(test_data::queueentrys());
}

fn generate<T, I>(data: I)
where
    T: Serialize + Clone,
    I: IntoIterator<Item = T>,
{
    let data = data
        .into_iter()
        .map(|d| {
            let serialized = serialize(&d).unwrap().to_vec();
            (d, serialized)
        })
        .collect_vec()
        .apply_ref(bincode::serialize)
        .unwrap();

    let datatype = std::any::type_name::<T>().split("::").last().unwrap();
    let path = format!("src/tests/data/{datatype}.bin");
    std::fs::File::create(path)
        .unwrap()
        .write_all(&data)
        .unwrap();
}
