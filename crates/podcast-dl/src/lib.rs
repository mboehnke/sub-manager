use apply::Apply;
use camino::Utf8PathBuf;
pub use chrono::Duration;
use chrono::{DateTime, Utc};
use network::{Client, NetworkError};
use rss::{Channel, Item};
use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub struct Podcast {
    pub channel: String,
    pub title: String,
    pub description: String,
    pub upload_date: DateTime<Utc>,
    pub audio: String,
    pub thumbnail: String,
}

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum PodcastError {
    #[error("cannot parse an absolute URL from {0:?}")]
    UrlParseError(String),

    #[error(transparent)]
    NetworkError(#[from] NetworkError),

    #[error("could not parse rss feed: {url:?}")]
    FeedParseError { source: rss::Error, url: String },

    #[error("error spawning ffmpeg child process")]
    FfmpegChildError { source: std::io::Error },

    #[error("error piping ffmpeg subprocesses")]
    FfmpegPipeError,

    #[error("could not locate file after creation: {0:?}")]
    FileError(Utf8PathBuf),

    #[error("could not delete file: {file:?}")]
    FileDeleteError {
        source: std::io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not create file: {file:?}")]
    FileCreateError {
        source: std::io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not write to file: {file:?}")]
    FileWriteError {
        source: std::io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not create download directory: {dir:?}")]
    DirectoryCreateError {
        source: std::io::Error,
        dir: Utf8PathBuf,
    },
}

impl Podcast {
    /// gets a list of podcast episodes
    pub async fn from_url(
        client: &Client,
        url: &str,
        max_age: Duration,
    ) -> Result<Vec<Podcast>, PodcastError> {
        let channel = network::download_text(client, url)
            .await?
            .parse::<Channel>()
            .map_err(|source| PodcastError::FeedParseError {
                source,
                url: url.to_string(),
            })?;

        let now = Utc::now();

        let podcasts = channel
            .items()
            .iter()
            .filter_map(|item| Self::maybe_from(item, &channel))
            // younger than max_age
            .filter(|i| i.upload_date + max_age > now)
            .collect();

        Ok(podcasts)
    }

    fn maybe_from(item: &Item, channel: &Channel) -> Option<Self> {
        // get published date
        let upload_date = item
            .pub_date()?
            .apply(DateTime::parse_from_rfc2822)
            .ok()?
            .into();
        let audio = item
            .enclosure()
            .filter(|e| e.mime_type().contains("audio"))?
            .url()
            .split('?')
            .next()?
            .to_string();
        let thumbnail = item
            .itunes_ext()?
            .image()
            // default to channel image if no episode image can be found
            .or_else(|| channel.image().map(|img| img.url()))
            .or_else(|| channel.itunes_ext().and_then(|e| e.image()))?
            // delete any post data in url
            .split('?')
            .next()?
            .to_string();

        Some(Podcast {
            channel: channel.title().to_string(),
            title: item.title()?.to_string(),
            description: item.description().unwrap_or_default().to_string(),
            upload_date,
            audio,
            thumbnail,
        })
    }
}
