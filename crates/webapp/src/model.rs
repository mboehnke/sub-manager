use std::str::FromStr;

use apply::Apply;
use camino::Utf8PathBuf;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Eq, PartialEq, Hash, PartialOrd, Ord, Serialize, Deserialize)]
pub enum Subscription {
    Podcast(String),
    Twitch(String),
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum Setting {
    Text(String),
    Number(i64),
    Date(DateTime<Utc>),
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum Rule {
    Podcast(PodcastRule),
    Twitch(TwitchRule),
    YouTube(YouTubeRule),
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum PodcastRule {
    Channel(String, Utf8PathBuf),
    Title(String, Utf8PathBuf),
    Description(String, Utf8PathBuf),
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum TwitchRule {
    Channel(String, Utf8PathBuf),
    Title(String, Utf8PathBuf),
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum YouTubeRule {
    Channel(String, Utf8PathBuf),
    Title(String, Utf8PathBuf),
    Description(String, Utf8PathBuf),
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct QueueEntry {
    pub added: DateTime<Utc>,
    pub status: DownloadStatus,
    pub item: QueueItem,
    pub errors: Vec<DownloadError>,
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum DownloadStatus {
    Active,
    Pending,
    Paused(DateTime<Utc>),
    Stopped,
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct DownloadError(pub DateTime<Utc>, pub String);

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum QueueItem {
    YouTube(YouTubeVideo),
    Twitch(TwitchVideo),
    Podcast(Podcast),
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct YouTubeVideo {
    pub channel: String,
    pub title: String,
    pub description: String,
    pub upload_date: DateTime<Utc>,
    pub id: String,
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct TwitchVideo {
    pub channel: String,
    pub title: String,
    pub upload_date: DateTime<Utc>,
    pub id: String,
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Podcast {
    pub channel: String,
    pub title: String,
    pub description: String,
    pub upload_date: DateTime<Utc>,
    pub audio: String,
    pub thumbnail: String,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, PartialOrd, Ord, Serialize, Deserialize)]
pub struct LogEntry {
    pub date: String,
    pub level: String,
    pub module: String,
    pub msg: String,
}

impl LogEntry {
    pub fn from_raw(s: String) -> Result<Vec<LogEntry>, String> {
        strip_formatting(s)
            .apply(split_entries)?
            .into_iter()
            .map(|e| e.parse())
            .collect()
    }
}

impl FromStr for LogEntry {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut chars = s.chars();

        let mut extract = |start, end| -> Result<String, String> {
            if Some(start) != chars.next() {
                return Err("error parsing log, unexpected character".into());
            }
            (&mut chars)
                .take_while(|&c| c != end)
                .collect::<String>()
                .apply(Ok)
        };

        let date = extract('[', ']')?;
        let level = extract(' ', ' ')?;
        let module = extract('[', ']')?;
        if Some(' ') != chars.next() {
            return Err("error parsing log, unexpected character".into());
        }
        let msg = chars.collect();

        Ok(LogEntry {
            date,
            level,
            module,
            msg,
        })
    }
}

fn strip_formatting(s: String) -> String {
    s.chars()
        .scan((false, ' '), |(prev_is_cc, prev), c| {
            let is_cc = (*prev_is_cc && prev != &'m') || c == '\x1B';
            *prev_is_cc = is_cc;
            *prev = c;
            Some((is_cc, c))
        })
        .filter(|(is_cc, _)| !is_cc)
        .map(|(_, c)| c)
        .collect()
}

fn split_entries(s: String) -> Result<Vec<String>, String> {
    let mut entries = Vec::new();
    for line in s.lines() {
        if line.starts_with('[') {
            entries.push(line.to_string());
        } else {
            let last = entries
                .last_mut()
                .ok_or_else(|| "error parsing log, unexpected character".to_string())?;
            last.push('\n');
            last.push_str(line);
        }
    }
    Ok(entries)
}

impl std::fmt::Display for Setting {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Setting::Text(v) => write!(f, "{}", v),
            Setting::Number(v) => write!(f, "{}", v),
            Setting::Date(v) => write!(f, "{}", v.format("%d.%m.%Y %H:%M")),
        }
    }
}
