use chrono::{DateTime, Utc};
use hashbrown::HashMap;
use iced::{
    button, scrollable, text_input, Application, Clipboard, Color, Command, Element, Settings,
};
use model::{LogEntry, QueueEntry, Rule, Setting, Subscription};
use theme::Theme;

mod api;
mod model;
mod theme;
mod time;
mod update;
mod view;

#[derive(Default)]
pub struct App {
    menu: Menu,
    page: Page,
    scroll: scrollable::State,
}

#[derive(Default)]
pub struct Menu {
    selected: MenuItem,
    queue_button: button::State,
    subscriptions_button: button::State,
    rules_button: button::State,
    settings_button: button::State,
    log_button: button::State,
}

#[derive(PartialEq)]
pub enum MenuItem {
    Queue,
    Subscriptions,
    Rules,
    Settings,
    Log,
    None,
}

pub enum Page {
    Queue(HashMap<u64, QueueEntry>),
    Subscriptions(SubscriptionsPage),
    Rules(RulesPage),
    Settings(SettingsPage),
    Log(Vec<LogEntry>),
    Empty,
    Loading,
    Error(String),
}

#[derive(Clone, Debug, Default)]
pub struct SubscriptionsPage {
    subs: Vec<Subscription>,
    add: AddSubscriptionsPage,
}

#[derive(Clone, Debug, Default)]
pub struct RulesPage {
    rules: HashMap<u64, Rule>,
    add: AddRulePage,
}

#[derive(Clone, Debug, Default)]
pub struct SettingsPage {
    settings: HashMap<String, Setting>,
    add: AddSettingsPage,
}

#[derive(Clone, Debug, Default)]
pub struct AddSubscriptionsPage {
    add_type: SubType,
    add_text: String,
    add_text_state: text_input::State,
    // add_button: button::State,
    twitch_button: button::State,
    podcast_button: button::State,
}

#[derive(Clone, Debug, Default)]
pub struct AddRulePage {
    add_type: RuleType,
    add_field: RuleField,
    add_value: String,
    add_directory: String,
    add_value_state: text_input::State,
    add_directory_state: text_input::State,
    channel_button: button::State,
    title_button: button::State,
    description_button: button::State,
    twitch_button: button::State,
    podcast_button: button::State,
    youtube_button: button::State,
}

#[derive(Clone, Debug, PartialEq)]
pub enum RuleType {
    Twitch,
    Podcast,
    YouTube,
}

#[derive(Clone, Debug, PartialEq)]
pub enum RuleField {
    Channel,
    Title,
    Description,
}

#[derive(Clone, Debug, PartialEq)]
pub enum SubType {
    Twitch,
    Podcast,
}

#[derive(Clone, Debug, Default)]
pub struct AddSettingsPage {
    add_type: SettingType,
    // add_button: button::State,
    add_key: String,
    add_value: String,
    add_key_state: text_input::State,
    add_value_state: text_input::State,
    text_button: button::State,
    number_button: button::State,
    date_button: button::State,
}

#[derive(Clone, Debug, PartialEq)]
pub enum SettingType {
    Text,
    Number,
    Date,
}

#[derive(Debug, Clone)]
pub enum Message {
    GetQueue,
    Queue(HashMap<u64, QueueEntry>),
    RemoveQueueEntry(u64),
    MarkQueueEntryPending(u64),
    MarkQueueEntryStopped(u64),
    MarkQueueEntryPaused(u64, DateTime<Utc>),

    GetSubscriptions,
    Subscriptions(Vec<Subscription>),
    InsertSubscription(Subscription),
    InsertSubscriptionTypeChanged(SubType),
    InsertSubscriptionTextChanged(String),
    RemoveSubscription(Subscription),

    GetRules,
    Rules(HashMap<u64, Rule>),
    InsertRule(Rule),
    InsertRuleTypeChanged(RuleType),
    InsertRuleFieldChanged(RuleField),
    InsertRuleValueChanged(String),
    InsertRuleDirectoryChanged(String),
    SwapRules(u64, u64),
    RemoveRule(u64),

    GetSettings,
    Settings(HashMap<String, Setting>),
    InsertSetting(String, Setting),
    InsertSettingTypeChanged(SettingType),
    InsertSettingKeyChanged(String),
    InsertSettingValueChanged(String),
    RemoveSetting(String),

    GetLog,
    Log(Vec<LogEntry>),

    Error(String),
}

fn main() -> iced::Result {
    let settings = Settings {
        default_text_size: Theme::FONT_SIZE_L,
        ..Settings::default()
    };
    App::run(settings)
}

impl Application for App {
    type Executor = iced::executor::Default;

    type Message = Message;

    type Flags = ();

    fn new(_: Self::Flags) -> (Self, Command<Self::Message>) {
        (App::default(), async { Message::GetQueue }.into())
    }

    fn title(&self) -> String {
        "Sub Manager".into()
    }

    fn update(&mut self, msg: Self::Message, clp: &mut Clipboard) -> Command<Self::Message> {
        update::root(self, msg, clp)
    }

    fn view(&mut self) -> Element<'_, Self::Message> {
        view::root(self).explain(Color::BLACK)
    }
}

impl Default for MenuItem {
    fn default() -> Self {
        MenuItem::None
    }
}

impl Default for Page {
    fn default() -> Self {
        Page::Loading
    }
}

impl std::default::Default for RuleType {
    fn default() -> Self {
        RuleType::YouTube
    }
}

impl std::default::Default for RuleField {
    fn default() -> Self {
        RuleField::Channel
    }
}

impl std::default::Default for SubType {
    fn default() -> Self {
        SubType::Podcast
    }
}

impl std::default::Default for SettingType {
    fn default() -> Self {
        SettingType::Text
    }
}
