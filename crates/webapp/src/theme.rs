use iced::{button, container, text_input, Color};
use Theme::*;

pub enum Theme {
    Window,
    Page,
    Button,
    HButton,
    SButton,
    AButton,
    Input,
    AInput,
}

impl Theme {
    pub const PADDING: u16 = 8;
    pub const SPACING: u16 = 4;
    pub const MENU_SPACING: u16 = 32;
    pub const IMG_SIZE_S: u16 = 32;
    pub const IMG_SIZE_L: u16 = 40;
    pub const FONT_SIZE_L: u16 = 20;
    pub const FONT_SIZE_S: u16 = 16;

    pub const BACKGROUND: Color = Self::color(0);
    pub const FOREGROUND: Color = Self::color(7);
    pub const FOREGROUND_BRIGHT: Color = Self::color(9);
    pub const WINDOW_BACKGROUND: Color = Self::color(8);
    pub const ALERT: Color = Self::color(1);
    pub const SELECTION: Color = Self::color(6);

    const COLORS: [(f32, f32, f32); 10] = [
        (0.16, 0.17, 0.2),
        (0.88, 0.42, 0.46),
        (0.6, 0.76, 0.47),
        (0.82, 0.6, 0.4),
        (0.38, 0.69, 0.94),
        (0.78, 0.47, 0.87),
        (0.34, 0.71, 0.76),
        (0.67, 0.7, 0.75),
        (0.0, 0.0, 0.0),
        (1.0, 1.0, 1.0),
    ];

    const fn color(i: usize) -> Color {
        let (r, g, b) = Self::COLORS[i];
        Color::from_rgb(r, g, b)
    }
}

impl container::StyleSheet for Theme {
    fn style(&self) -> container::Style {
        match self {
            Window => container::Style {
                text_color: Some(Self::FOREGROUND),
                background: Some(Self::WINDOW_BACKGROUND.into()),
                ..container::Style::default()
            },
            Page | Button => container::Style {
                text_color: Some(Self::FOREGROUND),
                background: Some(Self::BACKGROUND.into()),
                ..container::Style::default()
            },
            HButton => container::Style {
                text_color: Some(Self::SELECTION),
                background: Some(Self::BACKGROUND.into()),
                ..container::Style::default()
            },
            SButton => container::Style {
                text_color: Some(Self::FOREGROUND_BRIGHT),
                background: Some(Self::SELECTION.into()),
                ..container::Style::default()
            },
            AButton => container::Style {
                text_color: Some(Self::FOREGROUND_BRIGHT),
                background: Some(Self::ALERT.into()),
                ..container::Style::default()
            },
            Input => container::Style {
                text_color: Some(Self::BACKGROUND),
                background: Some(Self::FOREGROUND.into()),
                ..container::Style::default()
            },
            AInput => container::Style {
                text_color: Some(Self::ALERT),
                background: Some(Self::FOREGROUND.into()),
                ..container::Style::default()
            },
        }
    }
}

impl text_input::StyleSheet for Theme {
    fn active(&self) -> text_input::Style {
        text_input::Style {
            background: Self::FOREGROUND.into(),
            ..Default::default()
        }
    }

    fn focused(&self) -> text_input::Style {
        self.active()
    }

    fn placeholder_color(&self) -> Color {
        Self::BACKGROUND
    }

    fn value_color(&self) -> Color {
        match self {
            AInput => Self::ALERT,
            _ => Self::WINDOW_BACKGROUND,
        }
    }

    fn selection_color(&self) -> Color {
        Self::SELECTION
    }
}

impl button::StyleSheet for Theme {
    fn active(&self) -> button::Style {
        match self {
            Button => button::Style {
                background: Some(Self::BACKGROUND.into()),
                border_color: Self::BACKGROUND,
                text_color: Self::FOREGROUND,
                ..button::Style::default()
            },
            HButton => button::Style {
                background: Some(Self::BACKGROUND.into()),
                border_color: Self::BACKGROUND,
                text_color: Self::SELECTION,
                ..button::Style::default()
            },
            SButton => button::Style {
                background: Some(Self::SELECTION.into()),
                border_color: Self::SELECTION,
                text_color: Self::FOREGROUND_BRIGHT,
                ..button::Style::default()
            },
            AButton => button::Style {
                background: Some(Self::ALERT.into()),
                border_color: Self::ALERT,
                text_color: Self::FOREGROUND_BRIGHT,
                ..button::Style::default()
            },
            _ => button::Style::default(),
        }
    }

    fn hovered(&self) -> button::Style {
        self.active()
    }
}
