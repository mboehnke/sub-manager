use serde::{de::DeserializeOwned, Serialize};
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
use web_sys::{Request, RequestInit, Response};

pub mod log;
pub mod queue;
pub mod rules;
pub mod settings;
pub mod subscriptions;

struct ApiRequest {
    path: String,
    opts: RequestInit,
}

impl ApiRequest {
    pub fn get(path: &str) -> Self {
        Self::new("GET", path)
    }

    pub fn post(path: &str) -> Self {
        Self::new("POST", path)
    }

    pub fn delete(path: &str) -> Self {
        Self::new("DELETE", path)
    }

    pub fn head(path: &str) -> Self {
        Self::new("HEAD", path)
    }

    pub fn with_body(mut self, data: impl Serialize) -> Result<Self, String> {
        let body =
            serde_json::to_string(&data).map_err(|_| "could not serialize data into json")?;
        self.opts.body(Some(&body.into()));
        Ok(self)
    }

    pub async fn send(self) -> Result<Response, String> {
        let window = web_sys::window().ok_or("could not get application window")?;

        let request = Request::new_with_str_and_init(&self.path, &self.opts)
            .map_err(|_| "could not build http request")?;

        request
            .headers()
            .set("Content-Type", "application/json")
            .map_err(|_| "could not build headers for http request")?;

        let promise = window.fetch_with_request(&request);

        let response = JsFuture::from(promise)
            .await
            .map_err(|_| "http request failed")?
            .dyn_into::<Response>()
            .map_err(|_| "http request failed")?;

        Ok(response)
    }

    fn new(method: &str, path: &str) -> Self {
        let mut opts = RequestInit::new();
        opts.method(method);
        ApiRequest {
            path: format!("/api/{path}" ),
            opts,
        }
    }
}

async fn string_from_response(response: Response) -> Result<String, String> {
    let a = response
        .text()
        .map_err(|_| "could not process response to http request")?;
    JsFuture::from(a)
        .await
        .map_err(|_| "could not process response to http request")?
        .as_string()
        .ok_or_else(|| "could not process response to http request".into())
}

async fn parse_json<T>(response: Response) -> Result<T, String>
where
    T: DeserializeOwned,
{
    let body = string_from_response(response).await?;
    serde_json::from_str(&body).map_err(|_| "could not deserialize json".into())
}
