use apply::Apply;

use crate::model::Subscription;

use super::{parse_json, ApiRequest};

pub async fn get() -> Result<Vec<Subscription>, String> {
    ApiRequest::get("subscriptions")
        .send()
        .await?
        .apply(parse_json)
        .await
}

pub async fn insert(sub: Subscription) -> Result<(), String> {
    ApiRequest::post("subscriptions")
        .with_body(sub)?
        .send()
        .await
        .map(|_| ())
}

pub async fn remove(sub: Subscription) -> Result<(), String> {
    ApiRequest::delete("subscriptions")
        .with_body(sub)?
        .send()
        .await
        .map(|_| ())
}
