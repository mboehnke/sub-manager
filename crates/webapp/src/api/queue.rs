use apply::Apply;
use chrono::{DateTime, Utc};
use hashbrown::HashMap;

use crate::model::QueueEntry;

use super::{parse_json, ApiRequest};

pub async fn get() -> Result<HashMap<u64, QueueEntry>, String> {
    ApiRequest::get("queue")
        .send()
        .await?
        .apply(parse_json)
        .await
}

pub async fn remove(id: u64) -> Result<(), String> {
    ApiRequest::delete(&format!("queue/{id}"))
        .send()
        .await
        .map(|_| ())
}

pub async fn mark_paused(id: u64, date: DateTime<Utc>) -> Result<(), String> {
    ApiRequest::post("queue/mark_paused")
        .with_body((id, date))?
        .send()
        .await
        .map(|_| ())
}

pub async fn mark_pending(id: u64) -> Result<(), String> {
    ApiRequest::head(&format!("queue/mark_pending/{id}"))
        .send()
        .await
        .map(|_| ())
}

pub async fn mark_stopped(id: u64) -> Result<(), String> {
    ApiRequest::head(&format!("queue/mark_stopped/{id}"))
        .send()
        .await
        .map(|_| ())
}
