use apply::Apply;
use hashbrown::HashMap;

use crate::model::Rule;

use super::{parse_json, ApiRequest};

pub async fn get() -> Result<HashMap<u64, Rule>, String> {
    ApiRequest::get("rules")
        .send()
        .await?
        .apply(parse_json)
        .await
}

pub async fn insert(rule: Rule) -> Result<(), String> {
    ApiRequest::post("rules")
        .with_body(rule)?
        .send()
        .await
        .map(|_| ())
}

pub async fn remove(id: u64) -> Result<(), String> {
    ApiRequest::delete(&format!("rules/{id}"))
        .send()
        .await
        .map(|_| ())
}

pub async fn swap(id1: u64, id2: u64) -> Result<(), String> {
    ApiRequest::delete(&format!("rules/swap/{id1}/{id2}"))
        .send()
        .await
        .map(|_| ())
}
