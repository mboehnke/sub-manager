use apply::Apply;
use hashbrown::HashMap;

use crate::model::Setting;

use super::{parse_json, ApiRequest};

pub async fn get() -> Result<HashMap<String, Setting>, String> {
    ApiRequest::get("settings")
        .send()
        .await?
        .apply(parse_json)
        .await
}

pub async fn insert(key: String, value: Setting) -> Result<(), String> {
    ApiRequest::post("settings")
        .with_body((key, value))?
        .send()
        .await
        .map(|_| ())
}

pub async fn remove(key: String) -> Result<(), String> {
    ApiRequest::delete("settings")
        .with_body(key)?
        .send()
        .await
        .map(|_| ())
}
