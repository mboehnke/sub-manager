use apply::Apply;

use crate::model::LogEntry;

use super::{string_from_response, ApiRequest};

pub async fn get() -> Result<Vec<LogEntry>, String> {
    ApiRequest::get("log")
        .send()
        .await?
        .apply(string_from_response)
        .await?
        .apply(LogEntry::from_raw)
}
