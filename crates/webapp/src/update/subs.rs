use iced::Command;

use crate::{api, model::Subscription, App, MenuItem, Message, Page, SubType, SubscriptionsPage};

pub fn get(_app: &mut App) -> Command<Message> {
    Command::perform(api::subscriptions::get(), |r| {
        r.map(Message::Subscriptions).unwrap_or_else(Message::Error)
    })
}

pub fn insert(_app: &mut App, sub: Subscription) -> Command<Message> {
    Command::perform(api::subscriptions::insert(sub), |r| {
        r.map(|_| Message::GetSubscriptions)
            .unwrap_or_else(Message::Error)
    })
}

pub fn remove(_app: &mut App, sub: Subscription) -> Command<Message> {
    Command::perform(api::subscriptions::remove(sub), |r| {
        r.map(|_| Message::GetSubscriptions)
            .unwrap_or_else(Message::Error)
    })
}

pub fn set(app: &mut App, subs: Vec<Subscription>) -> Command<Message> {
    app.menu.selected = MenuItem::Subscriptions;
    let page = SubscriptionsPage {
        subs,
        ..Default::default()
    };
    app.page = Page::Subscriptions(page);
    Command::none()
}

pub fn insert_text_set(app: &mut App, text: String) -> Command<Message> {
    if let Page::Subscriptions(ref mut page) = app.page {
        page.add.add_text = text;
    };
    Command::none()
}

pub fn insert_type_set(app: &mut App, t: SubType) -> Command<Message> {
    if let Page::Subscriptions(ref mut page) = app.page {
        page.add.add_type = t;
    };
    Command::none()
}
