use crate::api;
use crate::{model::LogEntry, App, MenuItem, Message, Page};
use iced::Command;

pub fn get(_app: &mut App) -> Command<Message> {
    Command::perform(api::log::get(), |r| {
        r.map(Message::Log).unwrap_or_else(Message::Error)
    })
}

pub fn set(app: &mut App, entries: Vec<LogEntry>) -> Command<Message> {
    app.menu.selected = MenuItem::Log;
    app.page = Page::Log(entries);
    Command::none()
}
