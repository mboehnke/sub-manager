use hashbrown::HashMap;
use iced::Command;

use crate::{api, model::Rule, App, MenuItem, Message, Page, RuleField, RuleType, RulesPage};

pub fn get(_app: &mut App) -> Command<Message> {
    Command::perform(api::rules::get(), |r| {
        r.map(Message::Rules).unwrap_or_else(Message::Error)
    })
}

pub fn set(app: &mut App, rules: HashMap<u64, Rule>) -> Command<Message> {
    app.menu.selected = MenuItem::Rules;
    let page = RulesPage {
        rules,
        ..Default::default()
    };
    app.page = Page::Rules(page);
    Command::none()
}

pub fn insert(_app: &mut App, rule: Rule) -> Command<Message> {
    Command::perform(api::rules::insert(rule), |r| {
        r.map(|_| Message::GetRules).unwrap_or_else(Message::Error)
    })
}

pub fn remove(_app: &mut App, id: u64) -> Command<Message> {
    Command::perform(api::rules::remove(id), |r| {
        r.map(|_| Message::GetRules).unwrap_or_else(Message::Error)
    })
}

pub fn swap(_app: &mut App, id1: u64, id2: u64) -> Command<Message> {
    Command::perform(api::rules::swap(id1, id2), |r| {
        r.map(|_| Message::GetRules).unwrap_or_else(Message::Error)
    })
}

pub fn insert_type_set(app: &mut App, value: RuleType) -> Command<Message> {
    if let Page::Rules(ref mut page) = app.page {
        page.add.add_type = value.clone();
        if value == RuleType::Twitch && page.add.add_field == RuleField::Description {
            page.add.add_field = RuleField::Channel
        }
    };
    Command::none()
}

pub fn insert_field_set(app: &mut App, value: RuleField) -> Command<Message> {
    if let Page::Rules(ref mut page) = app.page {
        page.add.add_field = value;
    };
    Command::none()
}

pub fn insert_value_set(app: &mut App, value: String) -> Command<Message> {
    if let Page::Rules(ref mut page) = app.page {
        page.add.add_value = value;
    };
    Command::none()
}

pub fn insert_dir_set(app: &mut App, value: String) -> Command<Message> {
    if let Page::Rules(ref mut page) = app.page {
        page.add.add_directory = value;
    };
    Command::none()
}
