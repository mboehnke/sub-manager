use chrono::{DateTime, Utc};
use hashbrown::HashMap;
use iced::Command;

use crate::{api, model::QueueEntry, App, MenuItem, Message, Page};

pub fn get(_app: &mut App) -> Command<Message> {
    Command::perform(api::queue::get(), |r| {
        r.map(Message::Queue).unwrap_or_else(Message::Error)
    })
}

pub fn set(app: &mut App, queue: HashMap<u64, QueueEntry>) -> Command<Message> {
    app.menu.selected = MenuItem::Queue;
    app.page = if queue.is_empty() {
        Page::Empty
    } else {
        Page::Queue(queue)
    };
    Command::none()
}

pub fn remove(_app: &mut App, id: u64) -> Command<Message> {
    Command::perform(api::queue::remove(id), |r| {
        r.map(|_| Message::GetQueue).unwrap_or_else(Message::Error)
    })
}

pub fn mark_pending(_app: &mut App, id: u64) -> Command<Message> {
    Command::perform(api::queue::mark_pending(id), |r| {
        r.map(|_| Message::GetQueue).unwrap_or_else(Message::Error)
    })
}

pub fn mark_stopped(_app: &mut App, id: u64) -> Command<Message> {
    Command::perform(api::queue::mark_stopped(id), |r| {
        r.map(|_| Message::GetQueue).unwrap_or_else(Message::Error)
    })
}

pub fn mark_paused(_app: &mut App, id: u64, date: DateTime<Utc>) -> Command<Message> {
    Command::perform(api::queue::mark_paused(id, date), |r| {
        r.map(|_| Message::GetQueue).unwrap_or_else(Message::Error)
    })
}
