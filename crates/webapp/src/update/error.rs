use iced::Command;

use crate::{App, MenuItem, Message, Page};

pub fn set(app: &mut App, msg: String) -> Command<Message> {
    app.menu.selected = MenuItem::None;
    app.page = Page::Error(msg);
    Command::none()
}
