use hashbrown::HashMap;
use iced::Command;

use crate::{api, model::Setting, App, MenuItem, Message, Page, SettingType, SettingsPage};

pub fn get(_app: &mut App) -> Command<Message> {
    Command::perform(api::settings::get(), |r| {
        r.map(Message::Settings).unwrap_or_else(Message::Error)
    })
}

pub fn insert(_app: &mut App, key: String, value: Setting) -> Command<Message> {
    Command::perform(api::settings::insert(key, value), |r| {
        r.map(|_| Message::GetSettings)
            .unwrap_or_else(Message::Error)
    })
}

pub fn remove(_app: &mut App, key: String) -> Command<Message> {
    Command::perform(api::settings::remove(key), |r| {
        r.map(|_| Message::GetSettings)
            .unwrap_or_else(Message::Error)
    })
}

pub fn set(app: &mut App, settings: HashMap<String, Setting>) -> Command<Message> {
    app.menu.selected = MenuItem::Settings;
    let settings_page = SettingsPage {
        settings,
        ..Default::default()
    };
    app.page = Page::Settings(settings_page);
    Command::none()
}

pub fn insert_type_set(app: &mut App, t: SettingType) -> Command<Message> {
    if let Page::Settings(ref mut page) = app.page {
        page.add.add_type = t;
    };
    Command::none()
}

pub fn insert_key_set(app: &mut App, key: String) -> Command<Message> {
    if let Page::Settings(ref mut page) = app.page {
        page.add.add_key = key;
    };
    Command::none()
}

pub fn insert_value_set(app: &mut App, value: String) -> Command<Message> {
    if let Page::Settings(ref mut page) = app.page {
        page.add.add_value = value;
    };
    Command::none()
}
