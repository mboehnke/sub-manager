use iced::{Clipboard, Command};
use Message::*;

use crate::{App, Message};

mod error;
mod log;
mod queue;
mod rules;
mod settings;
mod subs;

pub fn root(app: &mut App, message: Message, _clipboard: &mut Clipboard) -> Command<Message> {
    match message {
        GetQueue => queue::get(app),
        Queue(queue) => queue::set(app, queue),
        MarkQueueEntryPending(id) => queue::mark_pending(app, id),
        MarkQueueEntryStopped(id) => queue::mark_stopped(app, id),
        MarkQueueEntryPaused(id, date) => queue::mark_paused(app, id, date),
        RemoveQueueEntry(id) => queue::remove(app, id),

        GetSubscriptions => subs::get(app),
        Subscriptions(subs) => subs::set(app, subs),
        InsertSubscription(sub) => subs::insert(app, sub),
        InsertSubscriptionTextChanged(text) => subs::insert_text_set(app, text),
        InsertSubscriptionTypeChanged(t) => subs::insert_type_set(app, t),
        RemoveSubscription(sub) => subs::remove(app, sub),

        GetRules => rules::get(app),
        Rules(r) => rules::set(app, r),
        InsertRule(rule) => rules::insert(app, rule),
        InsertRuleTypeChanged(v) => rules::insert_type_set(app, v),
        InsertRuleFieldChanged(v) => rules::insert_field_set(app, v),
        InsertRuleValueChanged(v) => rules::insert_value_set(app, v),
        InsertRuleDirectoryChanged(v) => rules::insert_dir_set(app, v),
        RemoveRule(id) => rules::remove(app, id),
        SwapRules(id1, id2) => rules::swap(app, id1, id2),

        GetSettings => settings::get(app),
        Settings(s) => settings::set(app, s),
        InsertSetting(key, value) => settings::insert(app, key, value),
        InsertSettingKeyChanged(key) => settings::insert_key_set(app, key),
        InsertSettingValueChanged(value) => settings::insert_value_set(app, value),
        InsertSettingTypeChanged(t) => settings::insert_type_set(app, t),
        RemoveSetting(key) => settings::remove(app, key),

        GetLog => log::get(app),
        Log(entries) => log::set(app, entries),

        Error(msg) => error::set(app, msg),
    }
}
