use std::{cell::RefCell, rc::Rc};

use apply::Apply;
use gazebo::prelude::*;
use iced::{Button, Column, Container, Element, Length, Row, Text};

use crate::{
    model::Subscription, theme::Theme, AddSubscriptionsPage, Message, SubType, SubscriptionsPage,
};

use super::image;

pub fn root(page: &mut SubscriptionsPage) -> Container<Message> {
    Column::new()
        .push(subscriptions(&page.subs))
        .push(add_subscription(&mut page.add))
        .spacing(Theme::PADDING)
        .apply(Container::new)
}

pub fn subscriptions(subs: &[Subscription]) -> Container<Message> {
    subs.map(subscription)
        .apply(Column::with_children)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .width(Length::Fill)
}

pub fn add_subscription(page: &mut AddSubscriptionsPage) -> Container<Message> {
    let submit_msg = match page.add_type {
        SubType::Twitch => Subscription::Twitch(page.add_text.clone()),
        SubType::Podcast => Subscription::Podcast(page.add_text.clone()),
    }
    .apply(Message::InsertSubscription);

    let buttons = {
        let (podcast_handle, twitch_handle) = match page.add_type {
            SubType::Twitch => ("podcast.webp", "ltwitch.webp"),
            SubType::Podcast => ("lpodcast.webp", "twitch.webp"),
        };
        let podcast_img = image(podcast_handle, Theme::IMG_SIZE_L);
        let twitch_img = image(twitch_handle, Theme::IMG_SIZE_L);
        let twitch_button = Button::new(&mut page.twitch_button, twitch_img)
            .on_press(Message::InsertSubscriptionTypeChanged(SubType::Twitch))
            .style(Theme::Button);
        let podcast_button = Button::new(&mut page.podcast_button, podcast_img)
            .on_press(Message::InsertSubscriptionTypeChanged(SubType::Podcast))
            .style(Theme::Button);
        Row::new().push(podcast_button).push(twitch_button)
    }
    .apply(Container::new)
    .padding(2);

    let channel = Text::new("CHANNEL")
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_L).into())
        .padding(Theme::PADDING + (Theme::IMG_SIZE_L - 32) / 2);

    let text = iced::TextInput::new(
        &mut page.add_text_state,
        "",
        &page.add_text,
        Message::InsertSubscriptionTextChanged,
    )
    .on_submit(submit_msg.clone())
    .style(Theme::Input)
    .padding(Theme::PADDING)
    .apply(Container::new)
    .width(Length::Fill)
    .style(Theme::Page)
    .padding(Theme::IMG_SIZE_L - 32);

    let button = {
        let state = Rc::new(RefCell::new(iced::button::State::new()));
        let content = Text::new("+");
        Button::new(unsafe { &mut *state.as_ptr() }, content)
    }
    .on_press(submit_msg)
    .style(Theme::SButton)
    .apply(Container::new)
    .style(Theme::SButton)
    .padding(Theme::PADDING + (Theme::IMG_SIZE_L - 32) / 2);

    Row::new()
        .push(buttons)
        .push(channel)
        .push(text)
        .push(button)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .width(Length::Fill)
        .style(Theme::Page)
}

fn subscription(sub: &Subscription) -> Element<Message> {
    let (handle, label) = match sub {
        Subscription::Podcast(v) => ("lpodcast.webp", v),
        Subscription::Twitch(v) => ("ltwitch.webp", v),
    };

    let img = image(handle, Theme::IMG_SIZE_S)
        .apply(Container::new)
        .padding(Theme::PADDING);
    let text = Text::new(label)
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .width(Length::Fill)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_S).into())
        .padding(Theme::PADDING);
    let button = {
        let state = Rc::new(RefCell::new(iced::button::State::new()));
        let content = Text::new("X");
        Button::new(unsafe { &mut *state.as_ptr() }, content)
    }
    .on_press(Message::RemoveSubscription(sub.clone()))
    .style(Theme::AButton)
    .apply(Container::new)
    .style(Theme::AButton)
    .padding(Theme::PADDING);

    Row::new()
        .push(img)
        .push(text)
        .push(button)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .width(Length::Fill)
        .style(Theme::Page)
        .into()
}
