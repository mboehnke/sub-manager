use std::{cell::RefCell, rc::Rc};

use apply::Apply;
use hashbrown::HashMap;
use iced::{button::State, Button, Column, Container, Element, Length, Row, Text};

use crate::{
    model::{PodcastRule, Rule, TwitchRule, YouTubeRule},
    theme::Theme,
    AddRulePage, Message, RuleField, RuleType, RulesPage,
};

use super::image;

pub fn root(page: &mut RulesPage) -> Container<Message> {
    Column::new()
        .push(rules(&page.rules))
        .push(add_rule(&mut page.add))
        .spacing(Theme::PADDING)
        .apply(Container::new)
}

pub fn rules(rules: &HashMap<u64, Rule>) -> Container<Message> {
    let mut rules = rules.iter().collect::<Vec<_>>();
    rules.sort_unstable();

    rules
        .iter()
        .enumerate()
        .map(|(i, &(&id, rule))| {
            let prev = rules
                .get(i.saturating_sub(1))
                .map(|(id, _)| **id)
                .unwrap_or(id);
            let next = rules.get(i + 1).map(|(id, _)| **id).unwrap_or(id);
            (id, rule, prev, next)
        })
        .map(rule)
        .collect::<Vec<_>>()
        .apply(Column::with_children)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .width(Length::Fill)
}

fn rule((id, rule, prev, next): (u64, &Rule, u64, u64)) -> Element<Message> {
    let (handle, field, value, directory) = match rule {
        Rule::Podcast(r) => match r {
            PodcastRule::Channel(val, dir) => ("podcast", "channel", val, dir),
            PodcastRule::Title(val, dir) => ("podcast", "title", val, dir),
            PodcastRule::Description(val, dir) => ("podcast", "description", val, dir),
        },
        Rule::Twitch(r) => match r {
            TwitchRule::Channel(val, dir) => ("twitch", "channel", val, dir),
            TwitchRule::Title(val, dir) => ("twitch", "title", val, dir),
        },
        Rule::YouTube(r) => match r {
            YouTubeRule::Channel(val, dir) => ("youtube", "channel", val, dir),
            YouTubeRule::Title(val, dir) => ("youtube", "title", val, dir),
            YouTubeRule::Description(val, dir) => ("youtube", "description", val, dir),
        },
    };

    let up_button = {
        let state = Rc::new(RefCell::new(iced::button::State::new()));
        let content = Text::new("▲");
        Button::new(unsafe { &mut *state.as_ptr() }, content)
    }
    .on_press(Message::SwapRules(id, prev))
    .style(Theme::Button);

    let down_button = {
        let state = Rc::new(RefCell::new(iced::button::State::new()));
        let content = Text::new("▼");
        Button::new(unsafe { &mut *state.as_ptr() }, content)
    }
    .on_press(Message::SwapRules(id, next))
    .style(Theme::Button);

    let up_down_buttons = Row::new()
        .push(up_button)
        .push(down_button)
        .apply(Container::new)
        .style(Theme::Page)
        .padding(Theme::PADDING);

    let image = image(&format!("l{handle}.webp"), Theme::IMG_SIZE_S)
        .apply(Container::new)
        .padding(Theme::PADDING);

    let field = Text::new(field)
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .width(430.into())
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_S).into())
        .padding(Theme::PADDING);

    let contains = Text::new("contains")
        .color(Theme::SELECTION)
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_S).into())
        .padding(Theme::PADDING);

    let value = Text::new(value)
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .width(Length::Fill)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_S).into())
        .padding(Theme::PADDING);

    let dl_dir = Text::new("directory")
        .color(Theme::SELECTION)
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_S).into())
        .padding(Theme::PADDING);

    let directory = Text::new(directory.as_str())
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .width(Length::Fill)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_S).into())
        .padding(Theme::PADDING);

    let button = {
        let state = Rc::new(RefCell::new(iced::button::State::new()));
        let content = Text::new("X");
        Button::new(unsafe { &mut *state.as_ptr() }, content)
    }
    .on_press(Message::RemoveRule(id))
    .style(Theme::AButton)
    .apply(Container::new)
    .style(Theme::AButton)
    .padding(Theme::PADDING);

    Row::new()
        .push(up_down_buttons)
        .push(image)
        .push(field)
        .push(contains)
        .push(value)
        .push(dl_dir)
        .push(directory)
        .push(button)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .width(Length::Fill)
        .style(Theme::Page)
        .into()
}

pub fn add_rule(page: &mut AddRulePage) -> Container<Message> {
    let submit_msg = match page.add_type {
        RuleType::Twitch => match page.add_field {
            crate::RuleField::Channel => Some(Rule::Twitch(TwitchRule::Channel(
                page.add_value.clone(),
                page.add_directory.clone().into(),
            ))),
            crate::RuleField::Title => Some(Rule::Twitch(TwitchRule::Title(
                page.add_value.clone(),
                page.add_directory.clone().into(),
            ))),
            crate::RuleField::Description => None,
        },
        RuleType::Podcast => match page.add_field {
            crate::RuleField::Channel => Some(Rule::Podcast(PodcastRule::Channel(
                page.add_value.clone(),
                page.add_directory.clone().into(),
            ))),
            crate::RuleField::Title => Some(Rule::Podcast(PodcastRule::Title(
                page.add_value.clone(),
                page.add_directory.clone().into(),
            ))),
            crate::RuleField::Description => Some(Rule::Podcast(PodcastRule::Description(
                page.add_value.clone(),
                page.add_directory.clone().into(),
            ))),
        },
        RuleType::YouTube => match page.add_field {
            crate::RuleField::Channel => Some(Rule::YouTube(YouTubeRule::Channel(
                page.add_value.clone(),
                page.add_directory.clone().into(),
            ))),
            crate::RuleField::Title => Some(Rule::YouTube(YouTubeRule::Title(
                page.add_value.clone(),
                page.add_directory.clone().into(),
            ))),
            crate::RuleField::Description => Some(Rule::YouTube(YouTubeRule::Description(
                page.add_value.clone(),
                page.add_directory.clone().into(),
            ))),
        },
    }
    .map(Message::InsertRule)
    .unwrap_or_else(|| Message::Error("invalid input".into()));

    let buttons = {
        let (youtube_handle, podcast_handle, twitch_handle) = match page.add_type {
            RuleType::YouTube => ("lyoutube.webp", "podcast.webp", "twitch.webp"),
            RuleType::Podcast => ("youtube.webp", "lpodcast.webp", "twitch.webp"),
            RuleType::Twitch => ("youtube.webp", "podcast.webp", "ltwitch.webp"),
        };
        let youtube_img = image(youtube_handle, Theme::IMG_SIZE_L);
        let podcast_img = image(podcast_handle, Theme::IMG_SIZE_L);
        let twitch_img = image(twitch_handle, Theme::IMG_SIZE_L);
        let youtube_button = Button::new(&mut page.youtube_button, youtube_img)
            .on_press(Message::InsertRuleTypeChanged(RuleType::YouTube))
            .style(Theme::Button);
        let podcast_button = Button::new(&mut page.podcast_button, podcast_img)
            .on_press(Message::InsertRuleTypeChanged(RuleType::Podcast))
            .style(Theme::Button);
        let twitch_button = Button::new(&mut page.twitch_button, twitch_img)
            .on_press(Message::InsertRuleTypeChanged(RuleType::Twitch))
            .style(Theme::Button);
        Row::new()
            .push(youtube_button)
            .push(podcast_button)
            .push(twitch_button)
    }
    .apply(Container::new)
    .padding(2);

    let contains_text = Text::new("CONTAINS")
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_L).into())
        .padding(Theme::PADDING + (Theme::IMG_SIZE_L - 32) / 2);

    let directory_text = Text::new("DIRECTORY")
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_L).into())
        .padding(Theme::PADDING + (Theme::IMG_SIZE_L - 32) / 2);

    let value_input = iced::TextInput::new(
        &mut page.add_value_state,
        "",
        &page.add_value,
        Message::InsertRuleValueChanged,
    )
    .on_submit(submit_msg.clone())
    .style(Theme::Input)
    .padding(Theme::PADDING)
    .apply(Container::new)
    .width(Length::Fill)
    .style(Theme::Page)
    .padding(Theme::IMG_SIZE_L - 32);

    let directory_input = iced::TextInput::new(
        &mut page.add_directory_state,
        "",
        &page.add_directory,
        Message::InsertRuleDirectoryChanged,
    )
    .on_submit(submit_msg.clone())
    .style(Theme::Input)
    .padding(Theme::PADDING)
    .apply(Container::new)
    .width(Length::Fill)
    .style(Theme::Page)
    .padding(Theme::IMG_SIZE_L - 32);

    let submit_button = {
        let state = Rc::new(RefCell::new(iced::button::State::new()));
        let content = Text::new("+");
        Button::new(unsafe { &mut *state.as_ptr() }, content)
    }
    .on_press(submit_msg)
    .style(Theme::SButton)
    .apply(Container::new)
    .style(Theme::SButton)
    .padding(Theme::PADDING + (Theme::IMG_SIZE_L - 32) / 2);

    let field_buttons = {
        let channel_button = button(
            &mut page.channel_button,
            "Channel",
            Message::InsertRuleFieldChanged(RuleField::Channel),
            page.add_field == RuleField::Channel,
        )
        .into();
        let title_button = button(
            &mut page.title_button,
            "Title",
            Message::InsertRuleFieldChanged(RuleField::Title),
            page.add_field == RuleField::Title,
        )
        .into();
        let description_button = button(
            &mut page.description_button,
            "Description",
            Message::InsertRuleFieldChanged(RuleField::Description),
            page.add_field == RuleField::Description,
        )
        .into();

        let mut children = vec![channel_button, title_button];
        if page.add_type != RuleType::Twitch {
            children.push(description_button);
        }
        Row::with_children(children)
    }
    .apply(Container::new)
    .padding(Theme::PADDING + (Theme::IMG_SIZE_L - 32) / 2);

    Row::new()
        .push(buttons)
        .push(field_buttons)
        .push(contains_text)
        .push(value_input)
        .push(directory_text)
        .push(directory_input)
        .push(submit_button)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .width(Length::Fill)
        .style(Theme::Page)
}

fn button<'a>(
    state: &'a mut State,
    label: &'static str,
    msg: Message,
    selected: bool,
) -> Button<'a, Message> {
    let style = if selected {
        Theme::HButton
    } else {
        Theme::Button
    };
    let content = Text::new(label);
    Button::new(state, content).style(style).on_press(msg)
}
