use apply::Apply;
use iced::{Container, Length, Text};

use crate::{theme::Theme, Message};

pub fn root<'a>() -> Container<'a, Message> {
    Text::new("LOADING ...")
        .apply(Container::new)
        .style(Theme::Page)
        .width(Length::Fill)
        .center_x()
        .padding(Theme::PADDING)
}
