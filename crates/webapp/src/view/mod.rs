use apply::Apply;
use iced::{Container, Element, Image, Length, Scrollable};

use crate::{theme::Theme, App, Message, Page};

mod empty;
mod error;
mod loading;
mod log;
mod menu;
mod queue;
mod rules;
mod settings;
mod subscriptions;

pub fn root(app: &mut App) -> Element<'_, Message> {
    let menu = menu::root(&mut app.menu)
        .apply(Container::new)
        .apply(Container::new)
        .center_x()
        .style(Theme::Page)
        .width(Length::Fill);

    let page_content = match &mut app.page {
        Page::Queue(queue) => queue::root(queue),
        Page::Subscriptions(page) => subscriptions::root(page),
        Page::Rules(page) => rules::root(page),
        Page::Settings(page) => settings::root(page),
        Page::Log(entries) => log::root(entries),
        Page::Empty => empty::root(),
        Page::Loading => loading::root(),
        Page::Error(msg) => error::root(msg),
    }
    .width(Length::Fill)
    .apply(Container::new)
    .style(Theme::Window)
    .padding(Theme::PADDING)
    .width(Length::Fill);
    let page = Scrollable::new(&mut app.scroll).push(page_content);

    iced::Column::new()
        .push(menu)
        .push(page)
        .height(Length::Fill)
        .width(Length::Fill)
        .apply(Container::new)
        .height(Length::Fill)
        .width(Length::Fill)
        .style(Theme::Window)
        .into()
}

fn image(handle: &str, size: u16) -> Image {
    Image::new(handle).height(size.into()).width(size.into())
}
