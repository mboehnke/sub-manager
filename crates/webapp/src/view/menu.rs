use iced::{button::State, Button, Element, Row, Text};

use crate::{theme::Theme, Menu, MenuItem, Message};

pub fn root(menu: &mut Menu) -> Element<Message> {
    let queue_button = button(
        &mut menu.queue_button,
        "QUEUE",
        Message::GetQueue,
        menu.selected == MenuItem::Queue,
    );
    let subscriptions_button = button(
        &mut menu.subscriptions_button,
        "SUBSCRIPTIONS",
        Message::GetSubscriptions,
        menu.selected == MenuItem::Subscriptions,
    );
    let rules_button = button(
        &mut menu.rules_button,
        "RULES",
        Message::GetRules,
        menu.selected == MenuItem::Rules,
    );
    let settings_button = button(
        &mut menu.settings_button,
        "SETTINGS",
        Message::GetSettings,
        menu.selected == MenuItem::Settings,
    );
    let log_button = button(
        &mut menu.log_button,
        "LOG",
        Message::GetLog,
        menu.selected == MenuItem::Log,
    );

    Row::new()
        .spacing(Theme::MENU_SPACING)
        .padding(Theme::PADDING)
        .push(queue_button)
        .push(subscriptions_button)
        .push(rules_button)
        .push(settings_button)
        .push(log_button)
        .into()
}

fn button<'a>(
    state: &'a mut State,
    label: &str,
    msg: Message,
    selected: bool,
) -> Button<'a, Message> {
    let style = if selected {
        Theme::HButton
    } else {
        Theme::Button
    };
    let content = Text::new(label);
    Button::new(state, content).style(style).on_press(msg)
}
