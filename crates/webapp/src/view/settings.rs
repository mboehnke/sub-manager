use std::{cell::RefCell, rc::Rc};

use apply::Apply;
use gazebo::prelude::*;
use hashbrown::HashMap;
use iced::{Button, Column, Container, Element, Length, Row, Text};

use crate::{model::Setting, theme::Theme, AddSettingsPage, Message, SettingType, SettingsPage};

use super::image;

pub fn root(page: &mut SettingsPage) -> Container<Message> {
    Column::new()
        .push(settings(&page.settings))
        .push(add_setting(&mut page.add))
        .spacing(Theme::PADDING)
        .apply(Container::new)
}

pub fn settings(settings: &HashMap<String, Setting>) -> Container<Message> {
    let mut settings = settings.iter().collect::<Vec<_>>();
    settings.sort_unstable();
    settings
        .map(setting)
        .apply(Column::with_children)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .width(Length::Fill)
}

fn setting<'a>(&(key, value): &(&'a String, &'a Setting)) -> Element<'a, Message> {
    let handle = match value {
        Setting::Text(_) => "ltext.webp",
        Setting::Number(_) => "linteger.webp",
        Setting::Date(_) => "ldate.webp",
    };
    let value_label = value.to_string();

    let img = image(handle, Theme::IMG_SIZE_S)
        .apply(Container::new)
        .padding(Theme::PADDING);
    let key_text = Text::new(key)
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .width(Length::Fill)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_S).into())
        .padding(Theme::PADDING);
    let value_text = Text::new(value_label)
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .width(Length::Fill)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_S).into())
        .padding(Theme::PADDING);
    let button = {
        let state = Rc::new(RefCell::new(iced::button::State::new()));
        let content = Text::new("X");
        Button::new(unsafe { &mut *state.as_ptr() }, content)
    }
    .on_press(Message::RemoveSetting(key.clone()))
    .style(Theme::AButton)
    .apply(Container::new)
    .style(Theme::AButton)
    .padding(Theme::PADDING);

    Row::new()
        .push(img)
        .push(key_text)
        .push(value_text)
        .push(button)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .width(Length::Fill)
        .style(Theme::Page)
        .into()
}

pub fn add_setting(page: &mut AddSettingsPage) -> Container<Message> {
    let placeholder = match page.add_type {
        SettingType::Text => "abc",
        SettingType::Number => "123",
        SettingType::Date => "01.02.2003 04:05",
    };

    let submit_value = match page.add_type {
        SettingType::Text => Some(Setting::Text(page.add_value.clone())),
        SettingType::Number => page.add_value.parse().ok().map(Setting::Number),
        SettingType::Date => chrono::DateTime::parse_from_str(
            &format!("{} +0000", page.add_value),
            "%d.%m.%Y %H:%M %z",
        )
        .ok()
        .map(|d| Setting::Date(d.into())),
    };
    let submit_msg = match submit_value {
        Some(ref value) => Message::InsertSetting(page.add_key.clone(), value.clone()),
        None => Message::Error("invalid input".into()),
    };

    let buttons = {
        let (text_handle, number_handle, date_handle) = match page.add_type {
            SettingType::Text => ("ltext.webp", "integer.webp", "date.webp"),
            SettingType::Number => ("text.webp", "linteger.webp", "date.webp"),
            SettingType::Date => ("text.webp", "integer.webp", "ldate.webp"),
        };
        let text_img = image(text_handle, Theme::IMG_SIZE_L);
        let number_img = image(number_handle, Theme::IMG_SIZE_L);
        let date_img = image(date_handle, Theme::IMG_SIZE_L);
        let text_button = Button::new(&mut page.text_button, text_img)
            .on_press(Message::InsertSettingTypeChanged(SettingType::Text))
            .style(Theme::Button);
        let number_button = Button::new(&mut page.number_button, number_img)
            .on_press(Message::InsertSettingTypeChanged(SettingType::Number))
            .style(Theme::Button);
        let date_button = Button::new(&mut page.date_button, date_img)
            .on_press(Message::InsertSettingTypeChanged(SettingType::Date))
            .style(Theme::Button);
        Row::new()
            .push(text_button)
            .push(number_button)
            .push(date_button)
    }
    .apply(Container::new)
    .padding(2);

    let key_text = Text::new("KEY")
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_L).into())
        .padding(Theme::PADDING + (Theme::IMG_SIZE_L - 32) / 2);

    let value_text = Text::new("VALUE")
        .apply(Container::new)
        .padding(4)
        .apply(Container::new)
        .height((Theme::PADDING * 2 + Theme::IMG_SIZE_L).into())
        .padding(Theme::PADDING + (Theme::IMG_SIZE_L - 32) / 2);

    let key_input = iced::TextInput::new(
        &mut page.add_key_state,
        "",
        &page.add_key,
        Message::InsertSettingKeyChanged,
    )
    .on_submit(submit_msg.clone())
    .style(Theme::Input)
    .padding(Theme::PADDING)
    .apply(Container::new)
    .width(Length::Fill)
    .style(Theme::Page)
    .padding(Theme::IMG_SIZE_L - 32);

    let value_input = iced::TextInput::new(
        &mut page.add_value_state,
        placeholder,
        &page.add_value,
        Message::InsertSettingValueChanged,
    )
    .on_submit(submit_msg.clone());
    let value_input = if submit_value.is_some() {
        value_input.style(Theme::Input)
    } else {
        value_input.style(Theme::AInput)
    }
    .padding(Theme::PADDING)
    .apply(Container::new)
    .width(Length::Fill)
    .style(Theme::Page)
    .padding(Theme::IMG_SIZE_L - 32);

    let button = {
        let state = Rc::new(RefCell::new(iced::button::State::new()));
        let content = Text::new("+");
        Button::new(unsafe { &mut *state.as_ptr() }, content)
    }
    .on_press(submit_msg)
    .style(Theme::SButton)
    .apply(Container::new)
    .style(Theme::SButton)
    .padding(Theme::PADDING + (Theme::IMG_SIZE_L - 32) / 2);

    Row::new()
        .push(buttons)
        .push(key_text)
        .push(key_input)
        .push(value_text)
        .push(value_input)
        .push(button)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .width(Length::Fill)
        .style(Theme::Page)
}
