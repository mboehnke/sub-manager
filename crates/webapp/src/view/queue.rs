use std::{cell::RefCell, rc::Rc};

use apply::Apply;
use chrono::Duration;
use gazebo::prelude::*;
use hashbrown::HashMap;
use iced::{Button, Column, Container, Element, Length, Row, Space, Text};

use crate::{
    model::{DownloadError, QueueEntry, QueueItem},
    theme::Theme,
    time, Message,
};

use super::image;

pub fn root(queue: &mut HashMap<u64, QueueEntry>) -> Container<Message> {
    entries(queue).apply(Container::new)
}

pub fn entries(queue: &HashMap<u64, QueueEntry>) -> Container<Message> {
    let mut queue = queue.iter().collect::<Vec<_>>();
    queue.sort_unstable_by_key(|&(_, e)| e.added);

    queue
        .map(entry)
        .apply(Column::with_children)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .width(Length::Fill)
}

pub fn entry<'a>(&(&id, entry): &(&'a u64, &'a QueueEntry)) -> Element<'a, Message> {
    let (image_handle, channel, title) = match &entry.item {
        QueueItem::YouTube(i) => ("lyoutube.webp", &i.channel, &i.title),
        QueueItem::Podcast(i) => ("lpodcast.webp", &i.channel, &i.title),
        QueueItem::Twitch(i) => ("ltwitch.webp", &i.channel, &i.title),
    };

    let image = image(image_handle, Theme::IMG_SIZE_L);

    let status = match entry.status {
        crate::model::DownloadStatus::Active => "DOWNLOADING".to_string(),
        crate::model::DownloadStatus::Pending => "PENDING".to_string(),
        crate::model::DownloadStatus::Paused(date) => date
            .format("PAUSED\u{A0}UNTIL\u{A0}%d.%m.\u{A0}%H:%M")
            .to_string(),
        crate::model::DownloadStatus::Stopped => "STOPPED".to_string(),
    }
    .apply(Text::new)
    .apply(Container::new)
    .padding(Theme::PADDING / 2 + (Theme::IMG_SIZE_L - 32) / 2);

    let title = {
        let date = entry.added.format("%d.%m.%Y").to_string().apply(Text::new);
        let title = format!("{channel} - {title}")
            .apply(Text::new)
            .color(Theme::SELECTION);
        Row::new()
            .push(date)
            .push(title)
            .spacing(Theme::SPACING * 3)
    };

    let errors = {
        let error = |e: &DownloadError| -> Element<Message> {
            let date = e.0.format("%d.%m.%Y %H:%M");
            let msg = &e.1;
            format!("{date} {msg}")
                .apply(Text::new)
                .color(Theme::ALERT)
                .size(Theme::FONT_SIZE_S)
                .into()
        };
        entry.errors.map(error)
    }
    .apply(Column::with_children);

    let button = |label| {
        let state = Rc::new(RefCell::new(iced::button::State::new()));
        let content = Text::new(label);
        Button::new(unsafe { &mut *state.as_ptr() }, content)
    };

    let delete_button = button("DELETE")
        .on_press(Message::RemoveQueueEntry(id))
        .style(Theme::AButton)
        .apply(Container::new)
        .style(Theme::AButton)
        .padding((Theme::IMG_SIZE_L - 32) / 2);

    let resume_button = button("RESUME")
        .on_press(Message::MarkQueueEntryPending(id))
        .style(Theme::SButton)
        .apply(Container::new)
        .style(Theme::SButton)
        .padding((Theme::IMG_SIZE_L - 32) / 2);

    let stop_button = button("STOP")
        .on_press(Message::MarkQueueEntryStopped(id))
        .style(Theme::SButton)
        .apply(Container::new)
        .style(Theme::SButton)
        .padding((Theme::IMG_SIZE_L - 32) / 2);

    let pause_button = button("PAUSE")
        .on_press(Message::MarkQueueEntryPaused(
            id,
            time::now() + Duration::days(1),
        ))
        .style(Theme::SButton)
        .apply(Container::new)
        .style(Theme::SButton)
        .padding((Theme::IMG_SIZE_L - 32) / 2);

    let controls = Row::new()
        .push(image)
        .push(Space::with_width(Theme::MENU_SPACING.into()))
        .push(status)
        .push(Space::with_width(Length::Fill))
        .push(resume_button)
        .push(pause_button)
        .push(stop_button)
        .push(Space::with_width(Theme::MENU_SPACING.into()))
        .push(delete_button)
        .spacing(Theme::SPACING)
        .width(Length::Fill);

    let mut children = vec![title.into()];
    children.push(errors.into());
    children.push(controls.into());
    Column::with_children(children)
        .spacing(Theme::SPACING)
        .apply(Container::new)
        .padding(Theme::PADDING)
        .style(Theme::Page)
        .width(Length::Fill)
        .into()
}
