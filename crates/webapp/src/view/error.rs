use apply::Apply;
use iced::{Column, Container, Length, Text};

use crate::{theme::Theme, Message};

pub fn root(msg: &str) -> Container<Message> {
    let header = Text::new("ERROR")
        .color(Theme::ALERT)
        .apply(Container::new)
        .style(Theme::Page)
        .width(Length::Fill)
        .padding(Theme::PADDING)
        .center_x();
    let msg = Text::new(msg.to_string())
        .apply(Container::new)
        .style(Theme::Page)
        .width(Length::Fill)
        .padding(Theme::PADDING)
        .center_x();
    Column::new()
        .spacing(Theme::SPACING)
        .push(header)
        .push(msg)
        .apply(Container::new)
}
