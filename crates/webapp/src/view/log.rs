use apply::Apply;
use iced::{Column, Container, Element, Length, Row, Text};

use crate::{theme::Theme, LogEntry, Message};

pub fn root(entries: &[LogEntry]) -> Container<Message> {
    Column::with_children(entries.iter().map(entry).collect())
        .apply(Container::new)
        .padding(Theme::PADDING)
        .style(Theme::Page)
}

fn entry(entry: &LogEntry) -> Element<Message> {
    let color = if entry.level.to_lowercase() == "error" {
        Theme::ALERT
    } else {
        Theme::FOREGROUND
    };
    let date = Text::new(&entry.date)
        .size(Theme::FONT_SIZE_S)
        .apply(Container::new)
        .width(Length::Units(190));
    let level = Text::new(&entry.level)
        .size(Theme::FONT_SIZE_S)
        .color(color)
        .apply(Container::new)
        .width(Length::Units(90));
    let msg = entry
        .msg
        .lines()
        .map(|l| Text::new(l).size(Theme::FONT_SIZE_S).into())
        .collect::<Vec<_>>()
        .apply(Column::with_children)
        .width(Length::Fill);

    Row::new().push(date).push(level).push(msg).into()
}
