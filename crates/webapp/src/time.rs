use chrono::NaiveDateTime;
use chrono::{DateTime, Utc};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(typescript_type = "object")]
    pub type Object;

    #[wasm_bindgen(extends = Object, typescript_type = "Date")]
    pub type Date;

    #[wasm_bindgen(static_method_of = Date)]
    pub fn now() -> f64;
}

pub fn now() -> DateTime<Utc> {
    let secs = Date::now() as i64 / 1000;
    let datetime = NaiveDateTime::from_timestamp(secs, 0);
    DateTime::from_utc(datetime, Utc)
}
