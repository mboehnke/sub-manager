use std::process::Command;

fn main() {
    println!("cargo:rerun-if-changed=../webapp");

    let out_dir = std::env::var("OUT_DIR").unwrap();
    let cwd = std::env::var("CARGO_MANIFEST_DIR").unwrap();
    let release_build = std::env::var("PROFILE").unwrap() == "release";
    let build = if release_build { "release" } else { "debug" };
    let binary = format!("{out_dir}/wasm32-unknown-unknown/{build}/webapp.wasm");
    let webapp_dir = format!("{out_dir}/webapp");
    let webapp_crate_dir = format!("{cwd}/../webapp");

    let success = Command::new("cargo")
        .current_dir(&webapp_crate_dir)
        .args(
            vec![
                "build",
                "--target",
                "wasm32-unknown-unknown",
                "--target-dir",
                &out_dir,
            ]
            .into_iter()
            .chain(Some("--release").filter(|_| release_build)),
        )
        .spawn()
        .unwrap()
        .wait()
        .unwrap()
        .success();
    assert!(success);

    wasm_bindgen_cli_support::Bindgen::new()
        .input_path(&binary)
        .web(true)
        .unwrap()
        .generate(&webapp_dir)
        .unwrap();

    println!("cargo:rustc-env=WEBAPP_DIR={webapp_dir}");
}

trait Execute {
    fn execute(&mut self);
}

impl Execute for Command {
    fn execute(&mut self) {
        assert!(self.spawn().unwrap().wait().unwrap().success())
    }
}
