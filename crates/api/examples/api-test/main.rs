//! spins up a server, fills the database with some data for test purposes

mod test_data;

use std::{
    env::{current_dir, temp_dir},
    path::PathBuf,
};

use apply::Apply as _;
use camino::Utf8PathBuf;
use datastore::Datastore;

const PORT: i64 = 8080;
const TEST_DB: &str = "webapp-test";
const LOGFILE: &str = "examples/api-test/sample.log";

type Error = Box<dyn std::error::Error>;

fn init_db(db: Datastore) -> Result<Datastore, Error> {
    db.settings().insert("api address", "0.0.0.0")?;
    db.settings().insert("api port", PORT)?;
    for (i, value) in test_data::settings().enumerate() {
        db.settings().insert(&i.to_string(), value)?;
    }
    for value in test_data::rules() {
        db.rules().insert(value)?;
    }
    for value in test_data::subscriptions() {
        db.subscriptions().insert(value)?;
    }
    for item in test_data::queueitems() {
        db.queue().insert(item)?;
    }
    Ok(db)
}

fn join(p1: PathBuf, p2: &str) -> Result<Utf8PathBuf, Error> {
    p1.join(p2)
        .apply(Utf8PathBuf::from_path_buf)
        .map_err(|_| "path contains non-utf8 characters".into())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let datastore = {
        let path = join(temp_dir(), TEST_DB)?;
        if path.exists() {
            std::fs::remove_dir_all(&path)?;
        }
        Datastore::open(&path)?.apply(init_db)
    }?;
    let logfile = join(current_dir()?, LOGFILE)?;
    println!("hosting example api at http://localhost:{PORT}");
    api::start(&datastore, logfile)?.await?.apply(Ok)
}
