| method | path                          | body                  | action                                           | response                  |
| ------ | ----------------------------- | --------------------- | ------------------------------------------------ | ------------------------- |
| GET    | /                             |                       |                                                  | _/index.html_             |
| GET    | /\<filename>                  |                       |                                                  | _/\<filename>_            |
| GET    | /api/log                      |                       |                                                  | _logfile_                 |
| GET    | /api/settings                 |                       | settings().get_all()                             | HashMap\<String, Setting> |
| POST   | /api/settings                 | (String, Setting)     | settings().insert(\<body>.0, \<body>.1)          | ()                        |
| DELETE | /api/settings                 | String                | settings().remove(\<body>.0)                     | ()                        |
| GET    | /api/subscriptions            |                       | subscriptions().get_all()                        | Vec\<Subscription>        |
| POST   | /api/subscriptions            | Subscription          | subscriptions().insert(\<body>)                  | ()                        |
| DELETE | /api/subscriptions            | Subscription          | subscriptions().remove(\<body>)                  | ()                        |
| GET    | /api/history                  |                       | history().get_all()                              | Vec\<HistoryEntry>        |
| POST   | /api/history                  | HistoryEntry          | history().insert(\<body>)                        | ()                        |
| DELETE | /api/history                  | HistoryEntry          | history().remove(\<body>)                        | ()                        |
| GET    | /api/rules                    |                       | rules().get_all()                                | HashMap\<u64, Rule>       |
| POST   | /api/rules                    | Rule                  | rules().insert(\<body>)                          | ()                        |
| DELETE | /api/rules/\<id>              |                       | rules().remove(\<id>)                            | ()                        |
| ANY    | /api/rules/swap/\<id1>/\<id2> |                       | rules().swap(\<id1>, \<id2>)                     | ()                        |
| GET    | /api/queue                    |                       | queue().get_all()                                | HashMap\<u64, QueueEntry> |
| POST   | /api/queue                    | QueueItem             | queue().insert(\<body>)                          | ()                        |
| POST   | /api/queue/if_new             | QueueItem             | queue().insert_if_new(\<body>)                   | ()                        |
| GET    | /api/queue/\<id>              |                       | queue().entry(id).get()                          | QueueEntry                |
| DELETE | /api/queue/\<id>              |                       | queue().entry(id).remove()                       | ()                        |
| POST   | /api/queue/record_error       | (u64, String)         | queue().entry(\<body>.0).record_error(\<body>.1) | ()                        |
| POST   | /api/queue/mark_paused        | (u64, DateTime\<Utc>) | queue().entry(\<body>.0).mark_paused(\<body>.1)  | ()                        |
| ANY    | /api/queue/mark_active/\<id>  |                       | queue().entry(id).mark_active()                  | ()                        |
| ANY    | /api/queue/mark_pending/\<id> |                       | queue().entry(id).mark_pending()                 | ()                        |
| ANY    | /api/queue/mark_stopped/\<id> |                       | queue().entry(id).mark_stopped()                 | ()                        |
