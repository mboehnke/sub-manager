#[cfg(test)]
mod tests;

mod api;
mod static_files;

use camino::Utf8Path;
use datastore::Datastore;
use log::debug;
use serde::Serialize;
use std::{convert::TryInto, net::Ipv4Addr};
use tokio::task::JoinHandle;
use warp::http::StatusCode;
use warp::reply::Response;
use warp::{path, Filter as _, Reply as _};

type ApiError = Box<dyn std::error::Error>;

pub fn start<T: AsRef<Utf8Path>>(
    datastore: &Datastore,
    logfile: T,
) -> Result<JoinHandle<()>, ApiError> {
    let address = datastore
        .settings()
        .get_or_insert("api address", "0.0.0.0")?
        .text()
        .ok_or("")?
        .parse::<Ipv4Addr>()?;
    debug!("Setting: {:?} = {address:?}", "api address");

    let port = datastore
        .settings()
        .get_or_insert("api port", 8000)?
        .number()
        .ok_or("")?
        .try_into()?;
    debug!("Setting: {:?} = {port:?}", "api port");

    Ok(tokio::spawn(
        warp::serve(
            path("api")
                .and(api::routes(datastore, logfile.as_ref().to_path_buf()))
                .or(static_files::routes()),
        )
        .run((address, port)),
    ))
}

trait UnwrapStatus {
    /// turns a `Result` or `Option` into a `Response` of either
    /// `StatusCode::OK` or `StatusCode::INTERNAL_SERVER_ERROR`
    fn unwrap_status(self) -> Response;
}

trait UnwrapJSON {
    /// turns a `Result` into a `Response` of either
    /// the JSON encoded `Ok` value or `StatusCode::INTERNAL_SERVER_ERROR`
    fn unwrap_json(self) -> Response;
}

impl<T, E> UnwrapStatus for Result<T, E> {
    fn unwrap_status(self) -> Response {
        self.map(|_| StatusCode::OK)
            .unwrap_or(StatusCode::INTERNAL_SERVER_ERROR)
            .into_response()
    }
}

impl<T> UnwrapStatus for Option<T> {
    fn unwrap_status(self) -> Response {
        self.map(|_| StatusCode::OK)
            .unwrap_or(StatusCode::INTERNAL_SERVER_ERROR)
            .into_response()
    }
}

impl<T: Serialize, E> UnwrapJSON for Result<T, E> {
    fn unwrap_json(self) -> Response {
        self.map(|x| warp::reply::json(&x).into_response())
            .unwrap_or_else(|_| StatusCode::INTERNAL_SERVER_ERROR.into_response())
    }
}
