use apply::Apply as _;
// use proc_macro;
use rust_embed::RustEmbed;
use warp::http::{HeaderValue, StatusCode};
use warp::hyper::Body;
use warp::reply::Response;
use warp::{filters::BoxedFilter, path::Tail};
use warp::{path, Filter, Reply};

#[derive(RustEmbed)]
#[folder = "assets"]
struct StaticAsset;

#[derive(RustEmbed)]
#[folder = "$WEBAPP_DIR"]
struct WebApp;

pub fn routes() -> BoxedFilter<(impl Reply,)> {
    index().or(webapp_file()).or(file()).apply(Filter::boxed)
}

fn index() -> BoxedFilter<(impl Reply,)> {
    warp::get()
        .and(path::end())
        .map(|| response_from::<StaticAsset>("index.html"))
        .boxed()
}

fn webapp_file() -> BoxedFilter<(impl Reply,)> {
    warp::get()
        .and(path("webapp"))
        .and(path::tail())
        .map(|path: Tail| response_from::<WebApp>(path.as_str()))
        .boxed()
}

fn file() -> BoxedFilter<(impl Reply,)> {
    warp::get()
        .and(path::tail())
        .map(|path: Tail| response_from::<StaticAsset>(path.as_str()))
        .boxed()
}

fn response_from<A: RustEmbed>(path: &str) -> Response {
    A::get(path)
        .map(|f| f.data)
        .map(Body::from)
        .map(Response::new)
        .map(|mut response| {
            if let Ok(media_type) = mime_guess::from_path(path)
                .first_or_octet_stream()
                .as_ref()
                .apply(HeaderValue::from_str)
            {
                response.headers_mut().insert("Content-Type", media_type);
            }
            response
        })
        .unwrap_or_else(|| StatusCode::NOT_FOUND.into_response())
}
