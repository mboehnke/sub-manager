use crate::tests::new_api;
use rust_embed::RustEmbed;

#[derive(RustEmbed)]
#[folder = "assets"]
struct StaticAsset;

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn get_index() {
    let (api_url, _) = new_api("static_files_get_index", 10100).await;
    let data_api = reqwest::get(api_url).await.unwrap().bytes().await.unwrap();
    let data_file = StaticAsset::get("index.html").unwrap().data.to_vec();
    assert_eq!(data_api, data_file);
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn get_file() {
    let (api_url, _) = new_api("static_files_get_file", 10101).await;
    for file in StaticAsset::iter() {
        let file_url = api_url.join(&file).unwrap();
        let data_api = reqwest::get(file_url).await.unwrap().bytes().await.unwrap();
        let data_file = StaticAsset::get(&file).unwrap().data.to_vec();
        assert_eq!(data_api, data_file)
    }
}
