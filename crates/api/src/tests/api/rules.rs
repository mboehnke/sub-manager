use datastore::Datastore;
use reqwest::Url;

use crate::tests::{assert_eq_weak, new_api, test_data};

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn insert() {
    let (api_url, db) = new_api("rules_insert", 11500).await;
    let client = reqwest::Client::new();
    assert_eq_weak(&api_url, &db).await;
    for rule in test_data::rules() {
        client
            .post(api_url.join("api/rules").unwrap())
            .json(&rule)
            .send()
            .await
            .unwrap();
        db.rules().insert(rule.clone()).unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn remove() {
    let (api_url, db) = new_api("rules_remove", 11501).await;
    let client = reqwest::Client::new();
    initialize(&api_url, &db, &client).await;
    for (id, _) in db.rules().get_all().unwrap() {
        let url = api_url
            .join("api/rules/")
            .unwrap()
            .join(&id.to_string())
            .unwrap();
        client.delete(url).send().await.unwrap();
        db.rules().remove(id).unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn swap() {
    let (api_url, db) = new_api("rules_swap", 11502).await;
    let client = reqwest::Client::new();
    initialize(&api_url, &db, &client).await;
    let mut rules = db.rules().get_all().unwrap().into_iter();
    let (key1, _) = rules.next().unwrap();
    let (key2, _) = rules.last().unwrap();
    assert_ne!(key1, key2);
    let url = api_url
        .join("api/rules/swap/")
        .unwrap()
        .join(&format!("{key1}/{key2}"))
        .unwrap();
    client.head(url).send().await.unwrap();
    db.rules().swap(key1, key2).unwrap();
    assert_eq_weak(&api_url, &db).await;
}

async fn initialize(api_url: &Url, db: &Datastore, client: &reqwest::Client) {
    for rule in test_data::rules() {
        client
            .post(api_url.join("api/rules").unwrap())
            .json(&rule)
            .send()
            .await
            .unwrap();
        db.rules().insert(rule.clone()).unwrap();
    }
}
