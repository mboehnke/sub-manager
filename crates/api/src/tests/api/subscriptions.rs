use crate::tests::{assert_eq_weak, new_api, test_data};

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn insert() {
    let (api_url, db) = new_api("subscriptions_insert", 11300).await;
    let client = reqwest::Client::new();
    assert_eq_weak(&api_url, &db).await;
    for sub in test_data::subscriptions() {
        client
            .post(api_url.join("api/subscriptions").unwrap())
            .json(&sub)
            .send()
            .await
            .unwrap();
        db.subscriptions().insert(sub.clone()).unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn remove() {
    let (api_url, db) = new_api("subscriptions_remove", 11301).await;
    let client = reqwest::Client::new();
    for sub in test_data::subscriptions() {
        client
            .post(api_url.join("api/subscriptions").unwrap())
            .json(&sub)
            .send()
            .await
            .unwrap();
        db.subscriptions().insert(sub.clone()).unwrap();
    }
    for sub in db.subscriptions().get_all().unwrap() {
        client
            .delete(api_url.join("api/subscriptions").unwrap())
            .json(&sub)
            .send()
            .await
            .unwrap();
        db.subscriptions().remove(&sub).unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}
