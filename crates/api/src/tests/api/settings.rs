use crate::tests::{assert_eq_weak, new_api, test_data};

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn insert() {
    let (api_url, db) = new_api("settings_insert", 11400).await;
    let client = reqwest::Client::new();
    assert_eq_weak(&api_url, &db).await;
    for (i, setting) in test_data::settings().enumerate() {
        client
            .post(api_url.join("api/settings").unwrap())
            .json(&(i.to_string(), setting.clone()))
            .send()
            .await
            .unwrap();
        db.settings().insert(&i.to_string(), setting).unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn remove() {
    let (api_url, db) = new_api("settings_remove", 11401).await;
    let client = reqwest::Client::new();
    for (i, setting) in test_data::settings().enumerate() {
        client
            .post(api_url.join("api/settings").unwrap())
            .json(&(i.to_string(), setting.clone()))
            .send()
            .await
            .unwrap();
        db.settings().insert(&i.to_string(), setting).unwrap();
    }
    for (i, _) in test_data::settings().enumerate() {
        client
            .delete(api_url.join("api/settings/").unwrap())
            .json(&i.to_string())
            .send()
            .await
            .unwrap();
        db.settings().remove(&i.to_string()).unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}
