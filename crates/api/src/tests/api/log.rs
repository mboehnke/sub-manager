use crate::tests::{log_path, new_api};

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn get() {
    const S: &str = "api_log_get";
    let (api_url, _) = new_api(S, 11100).await;
    let log_path = log_path(S);
    let log_url = api_url.join("api/log").unwrap();
    let data_api = reqwest::get(log_url).await.unwrap().bytes().await.unwrap();
    let data_file = std::fs::read(log_path).unwrap();
    assert_eq!(data_api, data_file);
}
