use crate::tests::{assert_eq_weak, new_api, test_data};

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn insert() {
    let (api_url, db) = new_api("history_insert", 11200).await;
    let client = reqwest::Client::new();
    assert_eq_weak(&api_url, &db).await;
    for entry in test_data::historyentrys() {
        client
            .post(api_url.join("api/history").unwrap())
            .json(&entry)
            .send()
            .await
            .unwrap();
        db.history().insert(entry.clone()).unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn remove() {
    let (api_url, db) = new_api("history_remove", 11201).await;
    let client = reqwest::Client::new();
    for entry in test_data::historyentrys() {
        client
            .post(api_url.join("api/history").unwrap())
            .json(&entry)
            .send()
            .await
            .unwrap();
        db.history().insert(entry.clone()).unwrap();
    }
    for entry in db.history().get_all().unwrap() {
        client
            .delete(api_url.join("api/history").unwrap())
            .json(&entry)
            .send()
            .await
            .unwrap();
        db.history().remove(&entry).unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}
