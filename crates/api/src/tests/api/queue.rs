use datastore::{Datastore, DownloadError, QueueEntry};
use itertools::Itertools;
use reqwest::Url;

use crate::tests::{assert_eq_weak, new_api, test_data};

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn insert() {
    let (api_url, db) = new_api("queue_insert", 11600).await;
    let client = reqwest::Client::new();
    assert_eq_weak(&api_url, &db).await;
    for item in test_data::queueitems() {
        client
            .post(api_url.join("api/queue").unwrap())
            .json(&item)
            .send()
            .await
            .unwrap();
        db.queue().insert(item.clone()).unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn insert_if_new() {
    let (api_url, db) = new_api("queue_insert_if_new", 11601).await;
    let client = reqwest::Client::new();
    initialize(&api_url, &db, &client).await;
    for item in test_data::queueitems() {
        client
            .post(api_url.join("api/queue/if_new").unwrap())
            .json(&item)
            .send()
            .await
            .unwrap();
        assert!(!db.queue().insert_if_new(item.clone()).unwrap());
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn entry_get() {
    let date = test_data::dates().next().unwrap();
    let (api_url, db) = new_api("queue_entry_get", 11602).await;
    let client = reqwest::Client::new();
    initialize(&api_url, &db, &client).await;
    for id in db.queue().iter().map(|entry| entry.id) {
        let url = api_url
            .join("api/queue/")
            .unwrap()
            .join(&id.to_string())
            .unwrap();
        let mut a: QueueEntry = client.get(url).send().await.unwrap().json().await.unwrap();
        let mut b = db.queue().entry(id).get().unwrap();
        a.added = date;
        b.added = date;
        assert_eq!(a, b)
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn entry_remove() {
    let (api_url, db) = new_api("queue_entry_remove", 11603).await;
    let client = reqwest::Client::new();
    initialize(&api_url, &db, &client).await;
    for id in db.queue().iter().map(|entry| entry.id).collect_vec() {
        let url = api_url
            .join("api/queue/")
            .unwrap()
            .join(&id.to_string())
            .unwrap();
        client.delete(url).send().await.unwrap();
        db.queue().entry(id).remove().unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn entry_record_error() {
    const ERROR_MSG_1: &str = "some error message";
    const ERROR_MSG_2: &str = "some other error message";
    let (api_url, db) = new_api("queue_entry_record_error", 11604).await;
    let client = reqwest::Client::new();
    initialize(&api_url, &db, &client).await;
    for id in db.queue().iter().map(|entry| entry.id).collect_vec() {
        let url = api_url.join("api/queue/record_error").unwrap();
        client
            .post(url.clone())
            .json(&(id, ERROR_MSG_1.to_string()))
            .send()
            .await
            .unwrap();
        client
            .post(url)
            .json(&(id, ERROR_MSG_2.to_string()))
            .send()
            .await
            .unwrap();
        db.queue().entry(id).record_error(ERROR_MSG_1).unwrap();
        db.queue().entry(id).record_error(ERROR_MSG_2).unwrap();

        let url = api_url
            .join("api/queue/")
            .unwrap()
            .join(&id.to_string())
            .unwrap();
        let a = client
            .get(url)
            .send()
            .await
            .unwrap()
            .json::<QueueEntry>()
            .await
            .unwrap()
            .errors
            .into_iter()
            .map(|DownloadError(_, msg)| msg)
            .collect_vec();
        let b = db
            .queue()
            .entry(id)
            .get()
            .unwrap()
            .errors
            .into_iter()
            .map(|DownloadError(_, msg)| msg)
            .collect_vec();
        assert_eq!(a.len(), b.len());
        assert!(a.iter().all(|va| b.contains(va)))
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn entry_mark_paused() {
    let date = test_data::dates().next().unwrap();
    let (api_url, db) = new_api("queue_entry_mark_paused", 11605).await;
    let client = reqwest::Client::new();
    initialize(&api_url, &db, &client).await;
    for id in db.queue().iter().map(|entry| entry.id).collect_vec() {
        let url = api_url.join("api/queue/mark_paused").unwrap();
        client.post(url).json(&(id, date)).send().await.unwrap();
        db.queue().entry(id).mark_paused(date).unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn entry_mark_active() {
    let (api_url, db) = new_api("queue_entry_mark_active", 11606).await;
    let client = reqwest::Client::new();
    initialize(&api_url, &db, &client).await;
    for id in db.queue().iter().map(|entry| entry.id).collect_vec() {
        let url = api_url
            .join("api/queue/mark_active/")
            .unwrap()
            .join(&id.to_string())
            .unwrap();
        client.head(url).send().await.unwrap();
        db.queue().entry(id).mark_active().unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn entry_mark_pending() {
    let (api_url, db) = new_api("queue_entry_mark_pending", 11607).await;
    let client = reqwest::Client::new();
    initialize(&api_url, &db, &client).await;
    for id in db.queue().iter().map(|entry| entry.id).collect_vec() {
        let url = api_url
            .join("api/queue/mark_pending/")
            .unwrap()
            .join(&id.to_string())
            .unwrap();
        client.head(url).send().await.unwrap();
        db.queue().entry(id).mark_pending().unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn entry_mark_stopped() {
    let (api_url, db) = new_api("queue_entry_mark_stopped", 11608).await;
    let client = reqwest::Client::new();
    initialize(&api_url, &db, &client).await;
    for id in db.queue().iter().map(|entry| entry.id).collect_vec() {
        let url = api_url
            .join("api/queue/mark_stopped/")
            .unwrap()
            .join(&id.to_string())
            .unwrap();
        client.head(url).send().await.unwrap();
        db.queue().entry(id).mark_stopped().unwrap();
        assert_eq_weak(&api_url, &db).await;
    }
}

async fn initialize(api_url: &Url, db: &Datastore, client: &reqwest::Client) {
    for item in test_data::queueitems() {
        client
            .post(api_url.join("api/queue").unwrap())
            .json(&item)
            .send()
            .await
            .unwrap();
        db.queue().insert(item.clone()).unwrap();
    }
}
