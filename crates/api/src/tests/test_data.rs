#![allow(dead_code)]

use camino::Utf8PathBuf;
use chrono::{DateTime, Utc};
use datastore::{
    DownloadError, DownloadStatus, HistoryEntry, Podcast, PodcastRule, QueueEntry, QueueItem, Rule,
    Setting, Subscription, TwitchRule, TwitchVideo, YouTubeRule, YouTubeVideo,
};
use itertools::{iproduct, Itertools as _};

pub fn u64s() -> impl Iterator<Item = u64> {
    [u64::MIN, 5, u64::MAX].iter().copied()
}

pub fn strings() -> impl Iterator<Item = String> {
    ["", "test ✖️ ✔️"].iter().map(|x| x.to_string())
}

pub fn settings() -> impl Iterator<Item = Setting> {
    (strings().map(Setting::Text))
        .chain(i64s().map(Setting::Number))
        .chain(dates().map(Setting::Date))
}

pub fn historyentrys() -> impl Iterator<Item = HistoryEntry> {
    (strings().map(HistoryEntry::Podcast))
        .chain(strings().map(HistoryEntry::Twitch))
        .chain(strings().map(HistoryEntry::YouTube))
}

pub fn subscriptions() -> impl Iterator<Item = Subscription> {
    (strings().map(Subscription::Podcast)).chain(strings().map(Subscription::Twitch))
}

pub fn rules() -> impl Iterator<Item = Rule> {
    (podcastrules().map(Rule::Podcast))
        .chain(twitchrules().map(Rule::Twitch))
        .chain(youtuberules().map(Rule::YouTube))
}

pub fn queueentrys() -> impl Iterator<Item = QueueEntry> {
    let items = queueitems().collect_vec();
    let errors = downloaderrors().collect_vec();
    let statuses = downloadstatuses().collect_vec();
    let dates = dates().collect_vec();
    iproduct!(items, errors, statuses, dates).map(|(item, errors, status, added)| QueueEntry {
        added,
        status,
        item,
        errors,
    })
}

pub fn i64s() -> impl Iterator<Item = i64> {
    [i64::MIN, -5, 0, 5, i64::MAX].iter().copied()
}

pub fn dates() -> impl Iterator<Item = DateTime<Utc>> {
    const DATE: &str = "1996-12-19T16:39:57-08:00";
    let date = DateTime::parse_from_rfc3339(DATE).unwrap().into();
    std::iter::once(date)
}

fn ruleinners() -> impl Iterator<Item = (String, Utf8PathBuf)> {
    let strings = strings().collect_vec();
    iproduct!(strings.clone(), strings).map(|(a, b)| (a, Utf8PathBuf::from(b)))
}

pub fn podcastrules() -> impl Iterator<Item = PodcastRule> {
    (ruleinners().map(|(a, b)| PodcastRule::Channel(a, b)))
        .chain(ruleinners().map(|(a, b)| PodcastRule::Title(a, b)))
        .chain(ruleinners().map(|(a, b)| PodcastRule::Description(a, b)))
}

pub fn twitchrules() -> impl Iterator<Item = TwitchRule> {
    (ruleinners().map(|(a, b)| TwitchRule::Channel(a, b)))
        .chain(ruleinners().map(|(a, b)| TwitchRule::Title(a, b)))
}

pub fn youtuberules() -> impl Iterator<Item = YouTubeRule> {
    (ruleinners().map(|(a, b)| YouTubeRule::Channel(a, b)))
        .chain(ruleinners().map(|(a, b)| YouTubeRule::Title(a, b)))
        .chain(ruleinners().map(|(a, b)| YouTubeRule::Description(a, b)))
}

fn downloadstatuses() -> impl Iterator<Item = DownloadStatus> {
    (dates().into_iter().map(DownloadStatus::Paused))
        .chain(std::iter::once(DownloadStatus::Pending))
        .chain(std::iter::once(DownloadStatus::Stopped))
        .chain(std::iter::once(DownloadStatus::Active))
}

fn downloaderrors() -> impl Iterator<Item = Vec<DownloadError>> {
    let strings = strings().collect_vec();
    let dates = dates().collect_vec();
    let errors = iproduct!(dates, strings)
        .map(|(date, error)| DownloadError(date, error))
        .collect_vec();
    std::iter::once(Vec::new())
        .chain(errors.clone().into_iter().map(|e| vec![e]))
        .chain(errors.into_iter().map(|e| vec![e.clone(), e]))
}

pub fn podcasts() -> impl Iterator<Item = Podcast> {
    let s = strings().collect_vec();
    let d = dates().collect_vec();
    iproduct!(s.clone(), s.clone(), s.clone(), s.clone(), s, d).map(
        |(channel, title, description, audio, thumbnail, upload_date)| Podcast {
            channel,
            title,
            description,
            upload_date,
            audio,
            thumbnail,
        },
    )
}

pub fn youtubevideos() -> impl Iterator<Item = YouTubeVideo> {
    let s = strings().collect_vec();
    let d = dates().collect_vec();
    iproduct!(s.clone(), s.clone(), s.clone(), s, d).map(
        |(channel, title, description, id, upload_date)| YouTubeVideo {
            channel,
            title,
            description,
            upload_date,
            id,
        },
    )
}

pub fn twitchvideos() -> impl Iterator<Item = TwitchVideo> {
    let s = strings().collect_vec();
    let d = dates().collect_vec();
    iproduct!(s.clone(), s.clone(), s, d).map(|(channel, title, id, upload_date)| TwitchVideo {
        channel,
        title,
        upload_date,
        id,
    })
}

pub fn queueitems() -> impl Iterator<Item = QueueItem> {
    (podcasts().map(QueueItem::Podcast))
        .chain(youtubevideos().map(QueueItem::YouTube))
        .chain(twitchvideos().map(QueueItem::Twitch))
}
