use std::{env::temp_dir, io::Write, time::Duration};

use apply::Apply;
use camino::Utf8PathBuf;
use datastore::{Datastore, HistoryEntry, QueueEntry, Rule, Setting, Subscription};
use hashbrown::HashMap;
use reqwest::Url;
use serde::de::DeserializeOwned;

mod api;
mod static_files;
mod test_data;

const TEST_DB: &str = "api-test";
const TEST_LOG: &str = "insert log contents here";

#[derive(Default, Clone, Debug)]
pub struct Db {
    pub settings: HashMap<String, Setting>,
    pub history: Vec<HistoryEntry>,
    pub subscriptions: Vec<Subscription>,
    pub rules: HashMap<u64, Rule>,
    pub queue: HashMap<u64, QueueEntry>,
}

trait EqWeak {
    fn eq_weak(self, _: Self) -> bool;
}

async fn new_api(s: &str, port: i64) -> (Url, Datastore) {
    let mut dbs = (1..).map(|i| format!("{s}_{i}")).map(|s| new_db(&s, port));
    let logfile = new_log(s);
    crate::start(&dbs.next().unwrap(), logfile)
        .unwrap_or_else(|e| panic!("could not start api server: {e}"));
    let api_url: Url = format!("http://127.0.0.1:{port}/").parse().unwrap();
    let db = dbs.next().unwrap();

    // make sure the api is up and running before we return
    // this should help to avoid a race condition
    while reqwest::get(api_url.clone()).await.is_err() {
        tokio::time::sleep(Duration::from_secs(1)).await;
    }

    (api_url, db)
}

async fn assert_eq_weak(api_url: &Url, db: &Datastore) {
    let a = Db::from_api(api_url);
    let b = Db::from(db);
    assert!(a.await.eq_weak(b));
}

impl Db {
    pub async fn from_api(base_url: &Url) -> Self {
        let client = reqwest::Client::new();
        let settings = api_request(base_url, "api/settings", &client).await;
        let history = api_request(base_url, "api/history", &client).await;
        let subscriptions = api_request(base_url, "api/subscriptions", &client).await;
        let rules = api_request(base_url, "api/rules", &client).await;
        let queue = api_request(base_url, "api/queue", &client).await;
        Db {
            settings,
            history,
            subscriptions,
            rules,
            queue,
        }
    }
}

async fn api_request<T: DeserializeOwned + Default>(
    base_url: &Url,
    path: &str,
    client: &reqwest::Client,
) -> T {
    client
        .get(base_url.join(path).unwrap())
        .send()
        .await
        .unwrap()
        .json()
        .await
        .unwrap_or_default()
}

impl From<&Datastore> for Db {
    fn from(db: &Datastore) -> Self {
        Db {
            settings: db
                .settings()
                .get_all()
                .expect("could not read settings table from test db"),
            history: db
                .history()
                .get_all()
                .expect("could not read history table from test db"),
            subscriptions: db
                .subscriptions()
                .get_all()
                .expect("could not read subscriptions table from test db"),
            rules: db
                .rules()
                .get_all()
                .expect("could not read rules table from test db"),
            queue: db
                .queue()
                .get_all()
                .expect("could not read queue table from test db"),
        }
    }
}

impl<T: Ord> EqWeak for Vec<T> {
    fn eq_weak(mut self, mut c: Self) -> bool {
        self.sort_unstable();
        c.sort_unstable();

        self == c
    }
}

impl EqWeak for Db {
    fn eq_weak(mut self, mut c: Self) -> bool {
        let date = test_data::dates().next().unwrap();
        self.queue.values_mut().for_each(|mut e| e.added = date);
        c.queue.values_mut().for_each(|mut e| e.added = date);

        self.settings == c.settings
            && self.history.eq_weak(c.history)
            && self.subscriptions.eq_weak(c.subscriptions)
            && self.rules == c.rules
            && self.queue == c.queue
    }
}

fn new_log(s: &str) -> Utf8PathBuf {
    let path = log_path(s);
    if let Some(dir) = path.parent() {
        std::fs::create_dir_all(dir)
            .unwrap_or_else(|e| panic!("could not create directory for logfile: {e}"));
    }
    std::fs::File::create(&path)
        .unwrap_or_else(|e| panic!("could not create logfile: {e}"))
        .write_all(TEST_LOG.as_bytes())
        .unwrap_or_else(|e| panic!("could not create logfile: {e}"));
    path
}

fn new_db(s: &str, port: i64) -> Datastore {
    delete_db(s);

    let db = Datastore::open(&path(s)).unwrap_or_else(|_| panic!("could not open test db: {s}"));

    db.settings()
        .insert("api address", "0.0.0.0")
        .unwrap_or_else(|e| panic!("could not initialize datastore: {e}"));
    db.settings()
        .insert("api port", port)
        .unwrap_or_else(|e| panic!("could not initialize datastore: {e}"));

    db
}

fn log_path(s: &str) -> Utf8PathBuf {
    path(s).with_extension("log")
}

fn path(s: &str) -> Utf8PathBuf {
    temp_dir()
        .join(TEST_DB)
        .apply(Utf8PathBuf::from_path_buf)
        .unwrap_or_else(|_| panic!("invalid db path: {TEST_DB}"))
        .join(s)
}

fn delete_db(s: &str) {
    if path(s).exists() {
        std::fs::remove_dir_all(&path(s))
            .unwrap_or_else(|_| panic!("could not delete test db: {s}"));
    }
}
