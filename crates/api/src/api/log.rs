use camino::Utf8PathBuf;
use warp::filters::BoxedFilter;
use warp::{path, Filter, Reply};

pub fn routes(logfile: Utf8PathBuf) -> BoxedFilter<(impl Reply,)> {
    warp::get()
        .and(path::end())
        .and(warp::fs::file(logfile))
        .boxed()
}
