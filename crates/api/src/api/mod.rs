mod history;
mod log;
mod queue;
mod rules;
mod settings;
mod subscriptions;

use camino::Utf8PathBuf;
use datastore::Datastore;
use warp::filters::BoxedFilter;
use warp::{path, Filter, Reply};

pub fn routes(datastore: &Datastore, logfile: Utf8PathBuf) -> BoxedFilter<(impl Reply,)> {
    (path("log").and(log::routes(logfile)))
        .or(path("history").and(history::routes(datastore)))
        .or(path("queue").and(queue::routes(datastore)))
        .or(path("rules").and(rules::routes(datastore)))
        .or(path("settings").and(settings::routes(datastore)))
        .or(path("subscriptions").and(subscriptions::routes(datastore)))
        .boxed()
}
