use crate::{UnwrapJSON as _, UnwrapStatus as _};
use datastore::Datastore;
use warp::filters::BoxedFilter;
use warp::{path, Filter, Reply};

pub fn routes(datastore: &Datastore) -> BoxedFilter<(impl Reply,)> {
    get_all(datastore.clone())
        .or(insert(datastore.clone()))
        .or(remove(datastore.clone()))
        .or(swap(datastore.clone()))
        .boxed()
}

fn get_all(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move || datastore.rules().get_all().unwrap_json();

    warp::get().and(path::end()).map(response).boxed()
}

fn insert(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |rule| datastore.rules().insert(rule).unwrap_status();

    warp::post()
        .and(path::end())
        .and(warp::body::json())
        .map(response)
        .boxed()
}

fn remove(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |id| datastore.rules().remove(id).unwrap_status();

    warp::delete().and(path!(u64)).map(response).boxed()
}

fn swap(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |id1, id2| datastore.rules().swap(id1, id2).unwrap_status();

    warp::any()
        .and(path!("swap" / u64 / u64))
        .map(response)
        .boxed()
}
