use crate::{UnwrapJSON as _, UnwrapStatus as _};
use datastore::{Datastore, HistoryEntry};
use warp::filters::BoxedFilter;
use warp::{path, Filter, Reply};

pub fn routes(datastore: &Datastore) -> BoxedFilter<(impl Reply,)> {
    get_all(datastore.clone())
        .or(insert(datastore.clone()))
        .or(remove(datastore.clone()))
        .boxed()
}

fn get_all(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move || datastore.history().get_all().unwrap_json();

    warp::get().and(path::end()).map(response).boxed()
}

fn insert(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |entry: HistoryEntry| datastore.history().insert(entry).unwrap_status();

    warp::post()
        .and(path::end())
        .and(warp::body::json())
        .map(response)
        .boxed()
}

fn remove(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |sub_id| datastore.history().remove(&sub_id).unwrap_status();

    warp::delete()
        .and(path::end())
        .and(warp::body::json())
        .map(response)
        .boxed()
}
