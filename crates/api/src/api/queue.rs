use crate::{UnwrapJSON as _, UnwrapStatus as _};
use datastore::{Datastore, QueueItem};
use warp::filters::BoxedFilter;
use warp::{path, Filter, Reply};

pub fn routes(datastore: &Datastore) -> BoxedFilter<(impl Reply,)> {
    get_all(datastore.clone())
        .or(insert(datastore.clone()))
        .or(insert_if_new(datastore.clone()))
        .or(get(datastore.clone()))
        .or(remove(datastore.clone()))
        .or(record_error(datastore.clone()))
        .or(mark_paused(datastore.clone()))
        .or(mark_active(datastore.clone()))
        .or(mark_pending(datastore.clone()))
        .or(mark_stopped(datastore.clone()))
        .boxed()
}

fn get_all(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move || datastore.queue().get_all().unwrap_json();

    warp::get().and(path::end()).map(response).boxed()
}

fn insert(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |item: QueueItem| datastore.queue().insert(item).unwrap_status();

    warp::post()
        .and(path::end())
        .and(warp::body::json())
        .map(response)
        .boxed()
}

fn insert_if_new(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |item: QueueItem| datastore.queue().insert_if_new(item).unwrap_status();

    warp::post()
        .and(path!("if_new"))
        .and(warp::body::json())
        .map(response)
        .boxed()
}

fn get(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |id| datastore.queue().entry(id).get().unwrap_json();

    warp::get().and(path!(u64)).map(response).boxed()
}

fn remove(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |id| datastore.queue().entry(id).remove().unwrap_status();

    warp::delete().and(path!(u64)).map(response).boxed()
}

fn record_error(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |(id, error): (u64, String)| {
        datastore
            .queue()
            .entry(id)
            .record_error(error)
            .unwrap_status()
    };

    warp::post()
        .and(path!("record_error"))
        .and(warp::body::json())
        .map(response)
        .boxed()
}

fn mark_paused(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |(id, resume_at)| {
        datastore
            .queue()
            .entry(id)
            .mark_paused(resume_at)
            .unwrap_status()
    };

    warp::post()
        .and(path!("mark_paused"))
        .and(warp::body::json())
        .map(response)
        .boxed()
}

fn mark_active(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |id| datastore.queue().entry(id).mark_active().unwrap_status();

    warp::any()
        .and(path!("mark_active" / u64))
        .map(response)
        .boxed()
}

fn mark_pending(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |id| datastore.queue().entry(id).mark_pending().unwrap_status();

    warp::any()
        .and(path!("mark_pending" / u64))
        .map(response)
        .boxed()
}

fn mark_stopped(datastore: Datastore) -> BoxedFilter<(impl Reply,)> {
    let response = move |id| datastore.queue().entry(id).mark_stopped().unwrap_status();

    warp::any()
        .and(path!("mark_stopped" / u64))
        .map(response)
        .boxed()
}
