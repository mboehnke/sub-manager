#[tokio::main]
async fn main() {
    println!("updating yt-dlp:");
    let res = yt_dlp::update().await;
    let output = match res {
        Ok(o) => o,
        Err(e) => e.to_string(),
    };
    println!("{output}");
}
