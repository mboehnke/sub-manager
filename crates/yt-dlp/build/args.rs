use std::{
    iter::once,
    process::{Command, Stdio},
};

use apply::Apply;

use super::{Arg, Error, IArg};

pub fn args() -> Result<Vec<Arg>, Error> {
    help_page()?
        .apply(reformat_help_page)?
        .into_iter()
        .map(|s| super::parser::line(&s).unwrap().1)
        .chain(once(Arg {
            arg: IArg::Params("--".to_string(), 1),
            option_name: "Url".to_string(),
            function_name: "url".to_string(),
            description: "video url".to_string(),
        }))
        .collect::<Vec<_>>()
        .apply(Ok)
}

fn help_page() -> Result<String, Error> {
    let output = Command::new("yt-dlp")
        .arg("--help")
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?
        .wait_with_output()?;
    if output.status.success() {
        Ok(String::from_utf8(output.stdout)?)
    } else {
        Err(String::from_utf8_lossy(&output.stderr).into())
    }
}

fn reformat_help_page(s: String) -> Result<Vec<String>, Error> {
    fn is_comment(line: &str) -> bool {
        line.trim().is_empty() || !is_continuation(line) && !is_arg(line)
    }
    fn is_continuation(line: &str) -> bool {
        line.starts_with("       ") || line.is_empty()
    }
    fn is_arg(line: &str) -> bool {
        line.trim_start().starts_with('-')
    }
    fn is_end(line: &str) -> bool {
        line.starts_with("URI, MAGNET, TORRENT_FILE, METALINK_FILE:")
    }

    let mut res = Vec::<String>::new();
    for line in s.lines() {
        if is_end(line) {
            break;
        }
        if is_comment(line) {
        } else if is_continuation(line) {
            let prev = res.last_mut().unwrap();
            prev.push(' ');
            prev.push_str(line.trim_start());
        } else {
            res.push(line.trim_start().to_string())
        }
    }
    Ok(res)
}
