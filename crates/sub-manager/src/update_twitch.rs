use std::time::Duration;

use crate::{
    GetSettingOrDefault, LogError, TWITCH_UPDATE_INTERVAL_DEFAULT, TWITCH_UPDATE_INTERVAL_SETTING,
    TWITCH_VIDEO_MAX_AGE_DAYS,
};
use anyhow::{Context, Result};
use datastore::{Datastore, Subscription, TwitchVideo};
use log::{debug, info};
use network::Client;
use tokio::task::JoinHandle;

pub fn start(datastore: &Datastore) -> JoinHandle<()> {
    tokio::task::spawn(update_loop(datastore.clone()))
}

async fn update_loop(datastore: Datastore) {
    loop {
        info!("checking for new twitch streams");
        try_update(&datastore).await.log_error();

        let minutes = datastore.get_setting_or_default(
            TWITCH_UPDATE_INTERVAL_SETTING,
            TWITCH_UPDATE_INTERVAL_DEFAULT,
        );

        // making sure we sleep for at least 1 minute between runs
        let duration = Duration::from_secs(minutes.max(1) as u64 * 60);
        debug!("twitch update loop sleeping for {duration:?}");
        tokio::time::sleep(duration).await
    }
}

async fn try_update(datastore: &Datastore) -> Result<()> {
    let client = network::new_client().context("could not construct http client")?;
    let max_age = chrono::Duration::days(TWITCH_VIDEO_MAX_AGE_DAYS);

    let channels = datastore
        .subscriptions()
        .get_all()
        .context("could not get twitch subscriptions")?
        .into_iter()
        .filter_map(Subscription::twitch);

    for channel_name in channels {
        try_update_sub(&channel_name, &client, max_age, datastore)
            .await
            .log_error();
    }

    Ok(())
}

async fn try_update_sub(
    channel_name: &str,
    client: &Client,
    max_age: chrono::Duration,
    datastore: &Datastore,
) -> Result<()> {
    let videos = TwitchVideo::from_channel_feed(client, channel_name, max_age)
        .await
        .with_context(|| format!("could not get channel feed for {channel_name:?}"))?;

    for video in videos {
        datastore
            .queue()
            .insert_if_new(video)
            .context("could not add video to download queue")?;
    }

    Ok(())
}
