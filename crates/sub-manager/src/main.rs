//! # Dependencies:
//! - python3
//! - pip3
//! - ffmpeg
//! - ffprobe
//! - aria2
//! - yt-dlp (this will be installed automatically with pip)

mod download;
mod update_podcast;
mod update_twitch;
mod update_youtube;

use anyhow::{Context, Result};
use camino::{Utf8Path, Utf8PathBuf};
use chrono::{DateTime, Utc};
use datastore::{Datastore, Setting};
use flexi_logger::{Age, Cleanup, Criterion, FileSpec, FormatFunction, Logger, Naming};
use log::{error, info};
use time::{format_description::FormatItem, macros::format_description};

const DEFAULT_CONFIG_DIRECTORY: &str = "/var/lib/sub-manager";

const LOGFILE: &str = "sub-manager_rCURRENT.log";

const DATASTORE_PATH: &str = "datastore";

const DOWNLOAD_INTERVAL: u64 = 1;

const TWITCH_UPDATE_INTERVAL_SETTING: &str = "twitch update interval";
const TWITCH_UPDATE_INTERVAL_DEFAULT: i64 = 15;
const TWITCH_VIDEO_MAX_AGE_DAYS: i64 = 14;

const PODCAST_UPDATE_INTERVAL_SETTING: &str = "podcast update interval";
const PODCAST_UPDATE_INTERVAL_DEFAULT: i64 = 15;
const PODCAST_VIDEO_MAX_AGE_DAYS: i64 = 14;

const YOUTUBE_UPDATE_INTERVAL_SETTING: &str = "youtube update interval";
const YOUTUBE_UPDATE_INTERVAL_DEFAULT: i64 = 15;
const YOUTUBE_VIDEO_MAX_AGE_DAYS: i64 = 14;
const YOUTUBE_SECRET_FILE: &str = "google-credentials.json";
const YOUTUBE_TOKEN_FILE: &str = "youtube-token.json";
const YOUTUBE_WATCH_LATER_PLAYLIST_SETTING: &str = "youtube managed playlist";
const YOUTUBE_WATCH_LATER_PLAYLIST_DEFAULT: &str = "watch later";
const YOUTUBE_LAST_FULL_SCAN_SETTING: &str = "youtube last full scan";

trait GetSettingOrDefault<D, V> {
    /// tries to get a setting from the datastore with a default value
    ///
    /// will try to insert the default value if `key` doesn't exist in the settings
    fn get_setting_or_default(&self, key: &str, default_value: D) -> V;
}

trait LogError {
    /// logs an error, then discards it
    ///
    /// useful when you want to take note of an error but don't need to act on in
    fn log_error(&self);
}

#[tokio::main]
async fn main() -> Result<()> {
    let config_directory = get_config_dir();

    let datastore = open_datastore(&config_directory);

    initialize_logger(&config_directory, &datastore);

    let api_handle = match api::start(&datastore, config_directory.join(LOGFILE)) {
        Ok(j) => j,
        Err(e) => {
            error!("could not start api service\n{e}");
            panic!()
        }
    };

    let mut threads = vec![api_handle];

    info!("checking for internet connection");
    if let Err(e) = network::wait_for_internet_connection()
        .await
        .context("could not check for internet connection")
    {
        e.log_error();
        std::panic::panic_any(e)
    };

    yt_dlp::update()
        .await
        .context("failed to update yt-dlp")
        .log_error();

    threads.push(download::start(&datastore));
    threads.push(update_twitch::start(&datastore));
    threads.push(update_podcast::start(&datastore));
    threads.push(update_youtube::start(&config_directory, &datastore));

    // keep going until the last thread dies
    for thread in threads {
        let _ = thread.await;
    }

    Ok(())
}

/// will try to get the config dir from (in this order):
/// - the first command line argument
/// - env variable `SUB_MANAGER_CONFIG_DIRECTORY`
/// - `DEFAULT_CONFIG_DIRECTORY` as defined in `main.rs`
fn get_config_dir() -> Utf8PathBuf {
    if let Some(dir) = std::env::args().nth(1) {
        return dir.into();
    }
    if let Ok(dir) = std::env::var("SUB_MANAGER_CONFIG_DIRECTORY") {
        return dir.into();
    }
    DEFAULT_CONFIG_DIRECTORY.into()
}

/// # Panics
/// errors here are unrecoverable and can't even be logged yet, so we just crash and burn
fn initialize_logger(config_directory: impl AsRef<Utf8Path>, datastore: &Datastore) {
    let log_level = datastore
        .settings()
        .get_or_insert("log level", "error")
        .unwrap()
        .text()
        .unwrap();

    let format_function: FormatFunction = |w, now, record| {
        const TIME_FORMAT: &[FormatItem<'static>] =
            format_description!("[year]-[month]-[day] [hour]:[minute]");
        let level = record.level();
        let timestamp = now.now().format(TIME_FORMAT).unwrap();
        let log_level = flexi_logger::style(level).paint(level.to_string());
        let module = record.module_path().unwrap_or("<unnamed>");
        let message = flexi_logger::style(level).paint(record.args().to_string());
        write!(w, "[{timestamp}] {log_level} [{module}] {message}")
    };

    let file_spec = FileSpec::default().directory(config_directory.as_ref());
    let rotate_criterion = Criterion::Age(Age::Day);
    let rotate_naming = Naming::Timestamps;
    let rotate_cleanup = Cleanup::KeepCompressedFiles(7);

    Logger::try_with_str(log_level)
        .unwrap()
        .format_for_files(format_function)
        .log_to_file(file_spec)
        .rotate(rotate_criterion, rotate_naming, rotate_cleanup)
        .start()
        .unwrap();
}

/// # Panics
/// errors here are unrecoverable and can't even be logged yet, so we just crash and burn
fn open_datastore(config_directory: impl AsRef<Utf8Path>) -> Datastore {
    let datastore_path = config_directory.as_ref().join(DATASTORE_PATH);
    Datastore::open(&datastore_path)
        .with_context(|| format!("could not open datastore: {datastore_path:?}"))
        .unwrap()
}

impl GetSettingOrDefault<Setting, Setting> for Datastore {
    fn get_setting_or_default(&self, key: &str, default_value: Setting) -> Setting {
        self.settings()
            .get_or_insert(key, default_value.clone())
            .with_context(|| {
                format!(
                    "could not get {:?} setting, defaulting to {:?}",
                    key, default_value
                )
            })
            .map_err(|e| e.log_error())
            .unwrap_or(default_value)
    }
}

impl GetSettingOrDefault<i64, i64> for Datastore {
    fn get_setting_or_default(&self, key: &str, default_value: i64) -> i64 {
        let setting: Setting = self.get_setting_or_default(key, default_value.into());
        setting.number().unwrap_or(default_value)
    }
}

impl GetSettingOrDefault<DateTime<Utc>, DateTime<Utc>> for Datastore {
    fn get_setting_or_default(&self, key: &str, default_value: DateTime<Utc>) -> DateTime<Utc> {
        let setting: Setting = self.get_setting_or_default(key, default_value.into());
        setting.date().unwrap_or(default_value)
    }
}

impl GetSettingOrDefault<&str, String> for Datastore {
    fn get_setting_or_default(&self, key: &str, default_value: &str) -> String {
        let setting: Setting = self.get_setting_or_default(key, default_value.into());
        setting.text().unwrap_or_else(|| default_value.to_string())
    }
}

impl LogError for anyhow::Error {
    fn log_error(&self) {
        let flattened = self
            .chain()
            .map(|e| e.to_string())
            .collect::<Vec<_>>()
            .join("\n");
        error!("{flattened}")
    }
}

impl<T> LogError for anyhow::Result<T> {
    fn log_error(&self) {
        if let Err(e) = self {
            e.log_error()
        }
    }
}
