use std::{
    convert::{TryFrom, TryInto},
    time::Duration,
};

use crate::{
    GetSettingOrDefault, LogError, YOUTUBE_LAST_FULL_SCAN_SETTING, YOUTUBE_SECRET_FILE,
    YOUTUBE_TOKEN_FILE, YOUTUBE_UPDATE_INTERVAL_DEFAULT, YOUTUBE_UPDATE_INTERVAL_SETTING,
    YOUTUBE_VIDEO_MAX_AGE_DAYS, YOUTUBE_WATCH_LATER_PLAYLIST_DEFAULT,
    YOUTUBE_WATCH_LATER_PLAYLIST_SETTING,
};
use anyhow::{Context, Result};
use camino::{Utf8Path, Utf8PathBuf};
use chrono::{TimeZone, Utc};
use chrono_tz::America::Los_Angeles;
use datastore::{Datastore, YouTubeVideo};
use google_api::YouTubeApi;
use log::{debug, info};
use network::Client;
use tokio::task::JoinHandle;

pub fn start(config_dir: &Utf8Path, datastore: &Datastore) -> JoinHandle<()> {
    let secret_file = config_dir.join(YOUTUBE_SECRET_FILE);
    let token_file = config_dir.join(YOUTUBE_TOKEN_FILE);
    tokio::task::spawn(update_loop(secret_file, token_file, datastore.clone()))
}

async fn update_loop(secret_file: Utf8PathBuf, token_file: Utf8PathBuf, datastore: Datastore) {
    loop {
        let api = match google_api::YouTubeApi::new(&secret_file, &token_file)
            .await
            .context("could not build youtube api client")
        {
            Ok(api) => api,
            Err(e) => {
                e.log_error();
                continue;
            }
        };

        try_update_watch_later(&api, &datastore).await.log_error();

        // we need to make sure we record when we do a full scan
        // if we cannot set that value we also cannot trust it and should err on the side of not spamming the api with requests
        // otherwise we we risk doing this too often and going over our daily quota
        let query_api = quota_has_reset(&datastore)
            && datastore
                .settings()
                .insert(YOUTUBE_LAST_FULL_SCAN_SETTING, Utc::now())
                .with_context(|| {
                    format!(
                        "could not update setting: {:?}",
                        YOUTUBE_LAST_FULL_SCAN_SETTING
                    )
                })
                .map_err(|e| e.log_error())
                .is_ok();

        if query_api {
            info!("checking youtube api for new videos");
            try_update_full_scan(&api, &datastore).await.log_error();
        } else {
            info!("checking youtube rss feeds for new videos");
            try_update_channel_feeds(&api, &datastore).await.log_error();
        }

        let minutes = datastore.get_setting_or_default(
            YOUTUBE_UPDATE_INTERVAL_SETTING,
            YOUTUBE_UPDATE_INTERVAL_DEFAULT,
        );

        // making sure we sleep for at least 1 minute between runs
        let duration = Duration::from_secs(minutes.max(1) as u64 * 60);
        debug!("youtube update loop sleeping for {duration:?}");
        tokio::time::sleep(duration).await
    }
}

async fn try_update_watch_later(api: &YouTubeApi, datastore: &Datastore) -> Result<()> {
    let playlist_name = datastore.get_setting_or_default(
        YOUTUBE_WATCH_LATER_PLAYLIST_SETTING,
        YOUTUBE_WATCH_LATER_PLAYLIST_DEFAULT,
    );

    info!("checking {playlist_name:?} playlist for new videos");

    let playlist_id = api
        .playlist_id(&playlist_name)
        .await
        .with_context(|| format!("could not read {playlist_name:?} playlist"))?;

    let videos = api
        .playlist_items(&playlist_id)
        .await
        .with_context(|| format!("could not read {playlist_name:?} playlist"))?;

    for (id, info) in videos {
        let video: YouTubeVideo = info.try_into().context("could not parse video info")?;
        datastore
            .queue()
            .insert(&video)
            .context("could not add video to download queue")?;
        if let Err(e) = api
            .delete_playlist_item(&id)
            .await
            .with_context(|| format!("could not remove item from {playlist_name:?} playlist"))
        {
            if let Some((id, _)) =
                datastore
                    .queue()
                    .get_all()
                    .with_context(|| format!(
                        "could not read download queue after failing to remove a video from {:?} playlist",
                        playlist_name
                    ))?
                    .into_iter()
                    .find(|(_, entry)| {
                        entry.item.clone().youtube().map(|y| y.id) == Some(video.id.clone())
                    })
            {
                datastore
                    .queue()
                    .entry(id)
                    .remove()
                    .with_context(|| format!(
                        "could not remove video from download queue after failing to remove it from {:?} playlist",
                        playlist_name
                    ))?;
            }
            return Err(e);
        }
    }

    Ok(())
}

async fn try_update_channel_feeds(api: &YouTubeApi, datastore: &Datastore) -> Result<()> {
    let client = network::new_client().context("could not construct http client")?;
    let max_age = chrono::Duration::days(YOUTUBE_VIDEO_MAX_AGE_DAYS);

    let channels = api
        .subscribed_channels()
        .await
        .context("could not get subscribed channels")?;

    for channel_id in channels {
        try_update_channel_feed(&channel_id, &client, max_age, datastore)
            .await
            .log_error();

        // sleep for 2s between requests
        // maybe slowing things down to a crawl will stop google from blocking our application?
        tokio::time::sleep(Duration::from_secs(2)).await
    }
    Ok(())
}

async fn try_update_channel_feed(
    channel_id: &str,
    client: &Client,
    max_age: chrono::Duration,
    datastore: &Datastore,
) -> Result<()> {
    let videos = YouTubeVideo::from_channel_feed(client, channel_id, max_age)
        .await
        .context("could not read channel feed")?;
    for video in videos {
        datastore
            .queue()
            .insert_if_new(video)
            .context("could not add video to download queue")?;
    }
    Ok(())
}

async fn try_update_full_scan(api: &YouTubeApi, datastore: &Datastore) -> Result<()> {
    let max_age = chrono::Duration::days(YOUTUBE_VIDEO_MAX_AGE_DAYS);
    let videos = api
        .subscription_uploads(max_age)
        .await
        .context("could not get uploads of subscribed channels")?
        .into_iter()
        .map(YouTubeVideo::try_from)
        .filter_map(Result::ok);

    for video in videos {
        datastore
            .queue()
            .insert_if_new(video)
            .context("could not add video to download queue")
            .log_error();
    }

    Ok(())
}

/// tries to determine if the quota has been reset since the last full scan
///
/// defaults to `false`
fn quota_has_reset(datastore: &Datastore) -> bool {
    // if the setting isn't even in there we err on the side of caution and just set it to `now`
    let last_full_scan =
        datastore.get_setting_or_default(YOUTUBE_LAST_FULL_SCAN_SETTING, Utc::now());

    // convert to America/LosAngeles timezone
    let reset_time = Los_Angeles.from_utc_datetime(&last_full_scan.naive_utc());
    // add 10 minutes leeway
    // if our system clock is a little bit off we don't want to cut it too close
    let reset_time = reset_time + chrono::Duration::minutes(10);
    // next midnight LA time (this is when quota resets)
    let reset_time =
        reset_time.date_naive().and_hms_opt(0, 0, 0).unwrap() + chrono::Duration::days(1);
    // convert to UTC
    let reset_time = Utc.from_utc_datetime(&reset_time);
    let now = chrono::Utc::now();
    info!(
        "last full scan: {}, quota reset: {}, now: {}",
        &last_full_scan.format("%d.%m. %H:%M"),
        &reset_time.format("%d.%m. %H:%M"),
        &now.format("%d.%m. %H:%M"),
    );
    reset_time < now
}
