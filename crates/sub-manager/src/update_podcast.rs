use std::time::Duration;

use crate::{
    GetSettingOrDefault, LogError, PODCAST_UPDATE_INTERVAL_DEFAULT,
    PODCAST_UPDATE_INTERVAL_SETTING, PODCAST_VIDEO_MAX_AGE_DAYS,
};
use anyhow::{Context, Result};
use datastore::{Datastore, Subscription};
use log::{debug, info};
use network::Client;
use podcast_dl::Podcast;
use tokio::task::JoinHandle;

pub fn start(datastore: &Datastore) -> JoinHandle<()> {
    tokio::task::spawn(update_loop(datastore.clone()))
}

async fn update_loop(datastore: Datastore) {
    loop {
        info!("checking for new podcast episodes");

        try_update(&datastore).await.log_error();

        let minutes = datastore.get_setting_or_default(
            PODCAST_UPDATE_INTERVAL_SETTING,
            PODCAST_UPDATE_INTERVAL_DEFAULT,
        );

        // making sure we sleep for at least 1 minute between runs
        let duration = Duration::from_secs(minutes.max(1) as u64 * 60);
        debug!("podcast update loop sleeping for {duration:?}");
        tokio::time::sleep(duration).await
    }
}

async fn try_update(datastore: &Datastore) -> Result<()> {
    let client = network::new_client().context("could not construct http client")?;
    let max_age = chrono::Duration::days(PODCAST_VIDEO_MAX_AGE_DAYS);

    let urls = datastore
        .subscriptions()
        .get_all()
        .context("could not get podcast subscriptions")?
        .into_iter()
        .filter_map(Subscription::podcast);

    for url in urls {
        try_update_sub(&url, &client, max_age, datastore)
            .await
            .log_error();
    }

    Ok(())
}

async fn try_update_sub(
    url: &str,
    client: &Client,
    max_age: chrono::Duration,
    datastore: &Datastore,
) -> Result<()> {
    let videos = Podcast::from_url(client, url, max_age)
        .await
        .with_context(|| format!("could not get channel feed for {url:?}"))?;

    for video in videos {
        datastore
            .queue()
            .insert_if_new(video)
            .context("could not add video to download queue")?;
    }

    Ok(())
}
