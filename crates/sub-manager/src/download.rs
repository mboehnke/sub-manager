use std::time::Duration;

use anyhow::{Context, Result};
use chrono::Utc;
use datastore::{Datastore, DownloadStatus, QueueEntryReference};
use downloader::Download;
use log::{debug, error, info};
use tokio::task::JoinHandle;

use crate::{LogError, DOWNLOAD_INTERVAL};

pub fn start(datastore: &Datastore) -> JoinHandle<()> {
    clean_temp_directories(datastore.clone());

    tokio::task::spawn(download_loop(datastore.clone()))
}

/// deletes the temp directories to avoid clogging them up with GBs of half-finished downloads
fn clean_temp_directories(datastore: Datastore) {
    info!("cleaning temp directories");
    let dir_setting = |setting| datastore.settings().get(setting).ok()?.text();
    for path in dir_setting(downloader::PODCAST_TEMP_DIRECTORY_SETTING)
        .into_iter()
        .chain(dir_setting(downloader::TWITCH_TEMP_DIRECTORY_SETTING))
        .chain(dir_setting(downloader::YOUTUBE_TEMP_DIRECTORY_SETTING))
    {
        if let Err(e) =
            std::fs::remove_dir_all(&path).with_context(|| format!("could not delete {path:?}"))
        {
            error!("{e}");
        }
    }
}

async fn download_loop(datastore: Datastore) {
    loop {
        for entry_ref in datastore.queue().iter() {
            try_download(entry_ref, &datastore).await.log_error();
        }

        let duration = Duration::from_secs(DOWNLOAD_INTERVAL * 60);
        debug!("download loop sleeping for {duration:?}");
        tokio::time::sleep(duration).await
    }
}

async fn try_download(entry_ref: QueueEntryReference, datastore: &Datastore) -> Result<()> {
    let entry = entry_ref.get().context("could not read queue entry")?;

    let (channel, title) = match &entry.item {
        datastore::QueueItem::YouTube(i) => (&i.channel, &i.title),
        datastore::QueueItem::Twitch(i) => (&i.channel, &i.title),
        datastore::QueueItem::Podcast(i) => (&i.channel, &i.title),
    };

    match entry.status {
        DownloadStatus::Stopped => return Ok(()),
        DownloadStatus::Paused(d) if d > Utc::now() => return Ok(()),
        _ => {}
    }

    info!("downloading \"{channel} - {title}\"");

    entry_ref
        .mark_active()
        .context("could not mark queue entry as active")?;

    if let Err(e) = entry
        .download(datastore)
        .await
        .context("could not download queue entry")
    {
        let error = e
            .chain()
            .last()
            .map(|e| e.to_string())
            .unwrap_or_else(|| e.to_string());
        entry_ref
            .record_error(error)
            .context("could not download queue entry and then failed to record the error")?;
        // pauses the download for a day longer for each consecutive error
        let hours = 24 * (entry.errors.len() + 1) as i64;
        entry_ref
            .mark_paused(Utc::now() + chrono::Duration::hours(hours))
            .context("could not download queue entry and then failed to mark it as paused")?;
        return Err(e);
    }

    if let Err(e) = entry_ref.remove().context("could not remove queue entry") {
        // try to at least mark the download as stopped if deleting it doesn't work
        entry_ref
            .mark_stopped()
            .context("could not remove queue entry and then failed to mark it as stopped")?;
        return Err(e);
    };

    Ok(())
}
