use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

use apply::Apply;
use async_trait::async_trait;
use camino::Utf8PathBuf;
use chrono::Utc;
use datastore::{Datastore, PodcastRule, Rule};
use podcast_dl::Podcast;
use tokio::fs::remove_dir_all;

use crate::{
    clean_dir, default_temp_dir, encoding, filename_from, get_dir_setting, move_files, write_nfo,
    DownloadError, DownloadFiles, PODCAST_DEFAULT_DIRECTORY_SETTING,
    PODCAST_TEMP_DIRECTORY_SETTING,
};

#[async_trait]
impl DownloadFiles for Podcast {
    async fn download_files(
        &self,
        datastore: &Datastore,
    ) -> Result<Vec<Utf8PathBuf>, DownloadError> {
        let temp_dir = self.temp_directory(datastore)?;
        let download_dir = self.download_directory(datastore)?;
        let base_filename = self.base_filename();

        clean_dir(&temp_dir).await?;

        let mut files = download_process::podcast(
            &self.audio,
            &self.thumbnail,
            &Utf8PathBuf::from(&base_filename),
            &temp_dir,
        )
        .await?;

        let nfo = self.nfo().await;
        let nfo_file = write_nfo(&temp_dir, &base_filename, &nfo).await?;

        files.push(nfo_file);

        let files = move_files(files, download_dir).await?;

        let _ = remove_dir_all(temp_dir).await;

        Ok(files)
    }

    fn temp_directory(&self, datastore: &Datastore) -> Result<Utf8PathBuf, DownloadError> {
        get_dir_setting(PODCAST_TEMP_DIRECTORY_SETTING, datastore)
            .or_else(|_| default_temp_dir("podcast"))
            .map(|d| d.join(self.base_filename()))
    }

    fn download_directory(&self, datastore: &Datastore) -> Result<Utf8PathBuf, DownloadError> {
        match self.eval_rules(datastore)? {
            Some(directory) => Ok(directory),
            None => get_dir_setting(PODCAST_DEFAULT_DIRECTORY_SETTING, datastore),
        }
    }

    fn eval_rules(&self, datastore: &Datastore) -> Result<Option<Utf8PathBuf>, DownloadError> {
        let rules = datastore.rules().get_all()?;
        let directory = rules
            .iter()
            .map(|(_, value)| value)
            .cloned()
            .filter_map(Rule::podcast)
            .find_map(|rule| match rule {
                PodcastRule::Channel(value, path) => Some(path)
                    .filter(|_| self.channel.to_lowercase().contains(&value.to_lowercase())),
                PodcastRule::Title(value, path) => {
                    Some(path).filter(|_| self.title.to_lowercase().contains(&value.to_lowercase()))
                }
                PodcastRule::Description(value, path) => Some(path).filter(|_| {
                    self.description
                        .to_lowercase()
                        .contains(&value.to_lowercase())
                }),
            });
        Ok(directory)
    }

    fn base_filename(&self) -> String {
        // generates a unique id from the audio url
        // just in case the rest of the filename is not unique itself

        let calculate_hash = |s: &String| {
            let mut h = DefaultHasher::new();
            s.hash(&mut h);
            h.finish()
        };

        let id = self
            .audio
            .apply_ref(calculate_hash)
            .to_be_bytes()
            .apply_ref(base64_url::encode);

        filename_from(&self.upload_date, &[&self.channel, &self.title], &id)
    }

    async fn nfo(&self) -> String {
        let (title, channel, description) = tokio::join!(
            encoding::html_safe(&self.title),
            encoding::html_safe(&self.channel),
            encoding::html_safe(&self.description)
        );
        format!(
            "<movie>\n\
                <title>{}</title>\n\
                <actor><name>{}</name></actor>\n\
                <plot>{}</plot>\n\
                <premiered>{}</premiered>\n\
                <dateadded>{}</dateadded>\n\
            </movie>",
            title,
            channel,
            description,
            self.upload_date.format("%Y-%m-%d %H:%M:%S"),
            Utc::now().format("%Y-%m-%d %H:%M:%S")
        )
    }
}
