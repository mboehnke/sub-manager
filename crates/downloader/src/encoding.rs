use tokio::io::AsyncWriteExt;

/// makes string safe for use in filenames
///
/// replaces anything other than `a-z` `A-Z` `0-9` `.` `,` `-` ` ` with `_`
pub fn filename_safe(s: &str) -> String {
    s.chars()
        .map(|c| {
            if c.is_ascii_alphanumeric() || b".,- ".contains(&(c as u8)) {
                c
            } else {
                '_'
            }
        })
        .collect()
}

/// HTML entity-encode a string.
///
/// Entity-encodes a string with a minimal set of entities:
///
/// - `" -- &quot;`
/// - `& -- &amp;`
/// - `' -- &#x27;`
/// - `< -- &lt;`
/// - `> -- &gt;`
///
/// # Safety notes
/// Using the function to encode an untrusted string that is to be used
/// as a HTML attribute value may lead to XSS vulnerabilities.
pub async fn html_safe(s: &str) -> String {
    let mut writer = Vec::with_capacity((s.len() / 3 + 1) * 4);
    let mut buf = [0u8; 4];
    for c in s.chars() {
        let _ = match c {
            '"' => writer.write_all(b"&quot;").await,
            '&' => writer.write_all(b"&amp;").await,
            '\'' => writer.write_all(b"&#x27;").await,
            '<' => writer.write_all(b"&lt;").await,
            '>' => writer.write_all(b"&gt;").await,
            c => {
                let bytes = encode_char_as_utf8(c, &mut buf);
                writer.write_all(bytes).await
            }
        };
    }
    String::from_utf8(writer).expect("impossible invalid UTF-8 in output")
}

fn encode_char_as_utf8(c: char, buf: &mut [u8]) -> &[u8] {
    let c = c as u32;
    if c <= 0x7f {
        buf[0] = c as u8;
        &buf[..1]
    } else if c <= 0x7ff {
        buf[1] = 0b10000000 | (c & 0b00111111) as u8;
        buf[0] = 0b11000000 | ((c >> 6) & 0b00011111) as u8;
        &buf[..2]
    } else if c <= 0xffff {
        buf[2] = 0b10000000 | (c & 0b00111111) as u8;
        buf[1] = 0b10000000 | ((c >> 6) & 0b00111111) as u8;
        buf[0] = 0b11100000 | ((c >> 12) & 0b00001111) as u8;
        &buf[..3]
    } else {
        buf[3] = 0b10000000 | (c & 0b00111111) as u8;
        buf[2] = 0b10000000 | ((c >> 6) & 0b00111111) as u8;
        buf[1] = 0b10000000 | ((c >> 12) & 0b00111111) as u8;
        buf[0] = 0b11110000 | ((c >> 18) & 0b00000111) as u8;
        &buf[..4]
    }
}
