use async_trait::async_trait;
use camino::Utf8PathBuf;
use chrono::Utc;
use datastore::{Datastore, Rule, TwitchRule};
use tokio::fs::remove_dir_all;
use twitch_dl::TwitchVideo;

use crate::{
    clean_dir, default_temp_dir, encoding, filename_from, get_dir_setting, move_files, write_nfo,
    DownloadError, DownloadFiles, TWITCH_DEFAULT_DIRECTORY_SETTING, TWITCH_TEMP_DIRECTORY_SETTING,
};

#[async_trait]
impl DownloadFiles for TwitchVideo {
    async fn download_files(
        &self,
        datastore: &Datastore,
    ) -> Result<Vec<Utf8PathBuf>, DownloadError> {
        let temp_dir = self.temp_directory(datastore)?;
        let download_dir = self.download_directory(datastore)?;
        let base_filename = self.base_filename();

        clean_dir(&temp_dir).await?;

        let mut files =
            download_process::twitch(&self.id, &Utf8PathBuf::from(&base_filename), &temp_dir)
                .get()
                .await?;

        let nfo = self.nfo().await;
        let nfo_file = write_nfo(&temp_dir, &base_filename, &nfo).await?;

        files.push(nfo_file);

        let files = move_files(files, download_dir).await?;

        let _ = remove_dir_all(temp_dir).await;

        Ok(files)
    }

    fn temp_directory(&self, datastore: &Datastore) -> Result<Utf8PathBuf, DownloadError> {
        get_dir_setting(TWITCH_TEMP_DIRECTORY_SETTING, datastore)
            .or_else(|_| default_temp_dir("twitch"))
            .map(|d| d.join(self.base_filename()))
    }

    fn download_directory(&self, datastore: &Datastore) -> Result<Utf8PathBuf, DownloadError> {
        match self.eval_rules(datastore)? {
            Some(directory) => Ok(directory),
            None => get_dir_setting(TWITCH_DEFAULT_DIRECTORY_SETTING, datastore),
        }
    }

    fn eval_rules(&self, datastore: &Datastore) -> Result<Option<Utf8PathBuf>, DownloadError> {
        let rules = datastore.rules().get_all()?;
        let directory = rules
            .iter()
            .map(|(_, value)| value)
            .cloned()
            .filter_map(Rule::twitch)
            .find_map(|rule| match rule {
                TwitchRule::Channel(value, path) => Some(path)
                    .filter(|_| self.channel.to_lowercase().contains(&value.to_lowercase())),
                TwitchRule::Title(value, path) => {
                    Some(path).filter(|_| self.title.to_lowercase().contains(&value.to_lowercase()))
                }
            });
        Ok(directory)
    }

    fn base_filename(&self) -> String {
        filename_from(&self.upload_date, &[&self.channel, &self.title], &self.id)
    }

    async fn nfo(&self) -> String {
        let (title, channel) = tokio::join!(
            encoding::html_safe(&self.title),
            encoding::html_safe(&self.channel),
        );
        format!(
            "<movie>\n\
                <title>{}</title>\n\
                <actor><name>{}</name></actor>\n\
                <premiered>{}</premiered>\n\
                <dateadded>{}</dateadded>\n\
            </movie>",
            title,
            channel,
            self.upload_date.format("%Y-%m-%d %H:%M:%S"),
            Utc::now().format("%Y-%m-%d %H:%M:%S")
        )
    }
}
