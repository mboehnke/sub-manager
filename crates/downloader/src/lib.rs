//! # Dependencies:
//! - python3
//! - yt-dlp
//! - ffmpeg
//! - ffprobe
//! - aria2

mod encoding;
mod podcast;
mod twitch;
mod youtube;

use std::convert::TryFrom;

use ::youtube::YouTubeError;
use apply::Apply;
use async_trait::async_trait;
use camino::{FromPathBufError, Utf8Path, Utf8PathBuf};
use chrono::{DateTime, Utc};
use datastore::{Datastore, DatastoreError, QueueEntry, QueueItem};
use path_ext::{Utf8PathError, Utf8PathExt};
use podcast_dl::PodcastError;
use thiserror::Error;
use twitch_dl::TwitchError;

pub const PODCAST_TEMP_DIRECTORY_SETTING: &str = "podcast temp directory";
pub const YOUTUBE_TEMP_DIRECTORY_SETTING: &str = "youtube temp directory";
pub const TWITCH_TEMP_DIRECTORY_SETTING: &str = "twitch temp directory";

const PODCAST_DEFAULT_DIRECTORY_SETTING: &str = "podcast default directory";
const YOUTUBE_DEFAULT_DIRECTORY_SETTING: &str = "youtube default directory";
const TWITCH_DEFAULT_DIRECTORY_SETTING: &str = "twitch default directory";

const YOUTUBE_DOWNLOAD_TIMEOUT_SETTING: &str = "youtube download timeout";
const YOUTUBE_DOWNLOAD_TIMEOUT_DEFAULT: i64 = 30;

const YOUTUBE_DOWNLOAD_CONNECTIONS_SETTING: &str = "youtube download connections";
const YOUTUBE_DOWNLOAD_CONNECTIONS_DEFAULT: i64 = 1;

const YOUTUBE_DOWNLOAD_SPONSORBLOCK_DISABLE_SETTING: &str =
    "youtube download sponsorblock disable setting";
const YOUTUBE_DOWNLOAD_SPONSORBLOCK_DISABLE_DEFAULT: &str = "false";
const YOUTUBE_DOWNLOAD_SPONSORBLOCK_API_SETTING: &str = "youtube download sponsorblock api setting";
const YOUTUBE_DOWNLOAD_SPONSORBLOCK_API_DEFAULT: &str = "https://sponsor.ajay.app";
const YOUTUBE_DOWNLOAD_SPONSORBLOCK_REMOVE_SETTING: &str =
    "youtube download sponsorblock remove setting";
const YOUTUBE_DOWNLOAD_SPONSORBLOCK_REMOVE_DEFAULT: &str = "default";
const YOUTUBE_DOWNLOAD_SPONSORBLOCK_MARK_SETTING: &str =
    "youtube download sponsorblock mark setting";
const YOUTUBE_DOWNLOAD_SPONSORBLOCK_MARK_DEFAULT: &str = "all";

const YOUTUBE_DOWNLOAD_FORMAT_SETTING: &str = "youtube download format setting";
const YOUTUBE_DOWNLOAD_FORMAT_DEFAULT: &str = "bv*[height>480][ext=mp4]+ba[ext=m4a]";

const YOUTUBE_DOWNLOAD_FORMAT_SORT_SETTING: &str = "youtube download format sort setting";
const YOUTUBE_DOWNLOAD_FORMAT_SORT_DEFAULT: &str = "";

const YOUTUBE_DOWNLOAD_THROTTLED_RATE_SETTING: &str = "youtube download throttled rate setting";
const YOUTUBE_DOWNLOAD_THROTTLED_RATE_DEFAULT: &str = "200k";

/// DownloadError enumerates all possible errors returned by this library.
#[derive(Error, Debug)]
#[non_exhaustive]
pub enum DownloadError {
    #[error(transparent)]
    DatastoreError(#[from] DatastoreError),

    #[error(transparent)]
    PodcastError(#[from] PodcastError),

    #[error(transparent)]
    YouTubeError(#[from] YouTubeError),

    #[error(transparent)]
    TwitchError(#[from] TwitchError),

    #[error(transparent)]
    Utf8PathError(#[from] Utf8PathError),

    #[error(transparent)]
    DownloadError(#[from] download_process::DownloadError),

    #[error("invalid value for setting {0:?}, expected a string")]
    InvalidValueError(String),

    #[error("could not convert filename to utf-8")]
    NonUtf8Error(#[from] FromPathBufError),

    #[error("could not create directory: {path:?}")]
    DirectoryCreateError {
        source: std::io::Error,
        path: Utf8PathBuf,
    },
}

#[async_trait]
pub trait Download {
    async fn download(&self, datastore: &Datastore) -> Result<Vec<Utf8PathBuf>, DownloadError>;
}

#[async_trait]
trait DownloadFiles {
    async fn download_files(
        &self,
        datastore: &Datastore,
    ) -> Result<Vec<Utf8PathBuf>, DownloadError>;
    fn temp_directory(&self, datastore: &Datastore) -> Result<Utf8PathBuf, DownloadError>;
    fn download_directory(&self, datastore: &Datastore) -> Result<Utf8PathBuf, DownloadError>;
    fn eval_rules(&self, datastore: &Datastore) -> Result<Option<Utf8PathBuf>, DownloadError>;
    fn base_filename(&self) -> String;
    async fn nfo(&self) -> String;
}

#[async_trait]
impl Download for QueueEntry {
    async fn download(&self, datastore: &Datastore) -> Result<Vec<Utf8PathBuf>, DownloadError> {
        match &self.item {
            QueueItem::YouTube(item) => item.download_files(datastore),
            QueueItem::Twitch(item) => item.download_files(datastore),
            QueueItem::Podcast(item) => item.download_files(datastore),
        }
        .await
    }
}

fn get_dir_setting(
    setting: &str,
    datastore: &datastore::Datastore,
) -> Result<Utf8PathBuf, DownloadError> {
    datastore
        .settings()
        .get(setting)?
        .text()
        .map(Utf8PathBuf::from)
        .ok_or_else(|| DownloadError::InvalidValueError(setting.to_string()))
}

fn default_temp_dir(sub_dir: &str) -> Result<Utf8PathBuf, DownloadError> {
    std::env::temp_dir()
        .join("sub_manager")
        .join(sub_dir)
        .apply(Utf8PathBuf::try_from)
        .map_err(FromPathBufError::into)
}

fn filename_from(date: &DateTime<Utc>, parts: &[&str], id: &str) -> String {
    let mut descriptive_part = date.format("%Y-%m-%d").to_string();
    for part in parts {
        descriptive_part.push_str(" - ");
        descriptive_part.push_str(part);
    }
    let descriptive_part = encoding::filename_safe(&descriptive_part)
        .chars()
        .take(230)
        .collect::<String>();
    format!("{descriptive_part} [{id}]")
}

async fn write_nfo(
    directory: &Utf8Path,
    base_filename: &str,
    nfo: &str,
) -> Result<Utf8PathBuf, DownloadError> {
    let filename = format!("{base_filename}.nfo");
    let file = directory.join(filename).create_file(nfo.as_bytes()).await?;
    Ok(file)
}

async fn move_files(
    files: Vec<Utf8PathBuf>,
    directory: Utf8PathBuf,
) -> Result<Vec<Utf8PathBuf>, DownloadError> {
    let mut moved_files = Vec::new();
    for file in files {
        file.set_permissions(0o666).await?;
        let f = file.move_to(&directory).await?;
        moved_files.push(f)
    }
    Ok(moved_files)
}

/// remove and recreate directory
/// to make sure we start with a empty directory
async fn clean_dir(path: &Utf8Path) -> Result<(), DownloadError> {
    // if we can't remove the directory things might still be ok, this error should be safe to ignore
    let _ = tokio::fs::remove_dir_all(path).await;
    tokio::fs::create_dir_all(path)
        .await
        .map_err(|source| DownloadError::DirectoryCreateError {
            source,
            path: path.to_path_buf(),
        })
}
