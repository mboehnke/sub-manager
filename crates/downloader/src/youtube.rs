use std::time::Duration;

use async_trait::async_trait;
use camino::Utf8PathBuf;
use chrono::Utc;
use datastore::{Datastore, Rule, YouTubeRule};
use tokio::fs::remove_dir_all;
use youtube::YouTubeVideo;

use crate::{
    clean_dir, default_temp_dir, encoding, filename_from, get_dir_setting, move_files, write_nfo,
    DownloadError, DownloadFiles, YOUTUBE_DEFAULT_DIRECTORY_SETTING,
    YOUTUBE_DOWNLOAD_CONNECTIONS_DEFAULT, YOUTUBE_DOWNLOAD_CONNECTIONS_SETTING,
    YOUTUBE_DOWNLOAD_FORMAT_DEFAULT, YOUTUBE_DOWNLOAD_FORMAT_SETTING,
    YOUTUBE_DOWNLOAD_FORMAT_SORT_DEFAULT, YOUTUBE_DOWNLOAD_FORMAT_SORT_SETTING,
    YOUTUBE_DOWNLOAD_SPONSORBLOCK_API_DEFAULT, YOUTUBE_DOWNLOAD_SPONSORBLOCK_API_SETTING,
    YOUTUBE_DOWNLOAD_SPONSORBLOCK_DISABLE_DEFAULT, YOUTUBE_DOWNLOAD_SPONSORBLOCK_DISABLE_SETTING,
    YOUTUBE_DOWNLOAD_SPONSORBLOCK_MARK_DEFAULT, YOUTUBE_DOWNLOAD_SPONSORBLOCK_MARK_SETTING,
    YOUTUBE_DOWNLOAD_SPONSORBLOCK_REMOVE_DEFAULT, YOUTUBE_DOWNLOAD_SPONSORBLOCK_REMOVE_SETTING,
    YOUTUBE_DOWNLOAD_THROTTLED_RATE_DEFAULT, YOUTUBE_DOWNLOAD_THROTTLED_RATE_SETTING,
    YOUTUBE_DOWNLOAD_TIMEOUT_DEFAULT, YOUTUBE_DOWNLOAD_TIMEOUT_SETTING,
    YOUTUBE_TEMP_DIRECTORY_SETTING,
};

#[async_trait]
impl DownloadFiles for YouTubeVideo {
    async fn download_files(
        &self,
        datastore: &Datastore,
    ) -> Result<Vec<Utf8PathBuf>, DownloadError> {
        let temp_dir = self.temp_directory(datastore)?;
        let download_dir = self.download_directory(datastore)?;
        let base_filename = self.base_filename();

        clean_dir(&temp_dir).await?;

        let secs = datastore.settings().get_insert_or_default_number(
            YOUTUBE_DOWNLOAD_TIMEOUT_SETTING,
            YOUTUBE_DOWNLOAD_TIMEOUT_DEFAULT,
        );
        let timeout = Duration::from_secs(secs as u64 * 60);

        let connections = datastore.settings().get_insert_or_default_number(
            YOUTUBE_DOWNLOAD_CONNECTIONS_SETTING,
            YOUTUBE_DOWNLOAD_CONNECTIONS_DEFAULT,
        ) as u8;

        let sponsorblock_disable = datastore
            .settings()
            .get_insert_or_default_text(
                YOUTUBE_DOWNLOAD_SPONSORBLOCK_DISABLE_SETTING,
                YOUTUBE_DOWNLOAD_SPONSORBLOCK_DISABLE_DEFAULT,
            )
            .to_ascii_lowercase()
            == "true";

        let sponsorblock_api = datastore.settings().get_insert_or_default_text(
            YOUTUBE_DOWNLOAD_SPONSORBLOCK_API_SETTING,
            YOUTUBE_DOWNLOAD_SPONSORBLOCK_API_DEFAULT,
        );

        let sponsorblock_remove = datastore.settings().get_insert_or_default_text(
            YOUTUBE_DOWNLOAD_SPONSORBLOCK_REMOVE_SETTING,
            YOUTUBE_DOWNLOAD_SPONSORBLOCK_REMOVE_DEFAULT,
        );

        let sponsorblock_mark = datastore.settings().get_insert_or_default_text(
            YOUTUBE_DOWNLOAD_SPONSORBLOCK_MARK_SETTING,
            YOUTUBE_DOWNLOAD_SPONSORBLOCK_MARK_DEFAULT,
        );

        let format = datastore.settings().get_insert_or_default_text(
            YOUTUBE_DOWNLOAD_FORMAT_SETTING,
            YOUTUBE_DOWNLOAD_FORMAT_DEFAULT,
        );

        let format_sort = datastore.settings().get_insert_or_default_text(
            YOUTUBE_DOWNLOAD_FORMAT_SORT_SETTING,
            YOUTUBE_DOWNLOAD_FORMAT_SORT_DEFAULT,
        );

        let throttled_rate = datastore.settings().get_insert_or_default_text(
            YOUTUBE_DOWNLOAD_THROTTLED_RATE_SETTING,
            YOUTUBE_DOWNLOAD_THROTTLED_RATE_DEFAULT,
        );

        let mut process =
            download_process::youtube(&self.id, &Utf8PathBuf::from(&base_filename), &temp_dir)
                .format(&format)
                .format_sort(&format_sort)
                .concurrent_fragments(connections)
                .throttled_rate(&throttled_rate)
                .timeout(timeout);
        if sponsorblock_disable {
            process = process.sponsorblock_disable();
        } else {
            process = process
                .sponsorblock_api(&sponsorblock_api)
                .sponsorblock_mark(&sponsorblock_mark)
                .sponsorblock_remove(&sponsorblock_remove);
        }
        let mut files = process.get().await?;

        let nfo = self.nfo().await;
        let nfo_file = write_nfo(&temp_dir, &base_filename, &nfo).await?;

        files.push(nfo_file);

        let files = move_files(files, download_dir).await?;

        let _ = remove_dir_all(temp_dir).await;

        Ok(files)
    }

    fn temp_directory(&self, datastore: &Datastore) -> Result<Utf8PathBuf, DownloadError> {
        get_dir_setting(YOUTUBE_TEMP_DIRECTORY_SETTING, datastore)
            .or_else(|_| default_temp_dir("youtube"))
            .map(|d| d.join(self.base_filename()))
    }

    fn download_directory(&self, datastore: &Datastore) -> Result<Utf8PathBuf, DownloadError> {
        match self.eval_rules(datastore)? {
            Some(directory) => Ok(directory),
            None => get_dir_setting(YOUTUBE_DEFAULT_DIRECTORY_SETTING, datastore),
        }
    }

    fn eval_rules(&self, datastore: &Datastore) -> Result<Option<Utf8PathBuf>, DownloadError> {
        let rules = datastore.rules().get_all()?;
        let directory = rules
            .iter()
            .map(|(_, value)| value)
            .cloned()
            .filter_map(Rule::youtube)
            .find_map(|rule| match rule {
                YouTubeRule::Channel(value, path) => Some(path)
                    .filter(|_| self.channel.to_lowercase().contains(&value.to_lowercase())),
                YouTubeRule::Title(value, path) => {
                    Some(path).filter(|_| self.title.to_lowercase().contains(&value.to_lowercase()))
                }
                YouTubeRule::Description(value, path) => Some(path).filter(|_| {
                    self.description
                        .to_lowercase()
                        .contains(&value.to_lowercase())
                }),
            });
        Ok(directory)
    }

    fn base_filename(&self) -> String {
        filename_from(&self.upload_date, &[&self.channel, &self.title], &self.id)
    }

    async fn nfo(&self) -> String {
        let (title, channel, description) = tokio::join!(
            encoding::html_safe(&self.title),
            encoding::html_safe(&self.channel),
            encoding::html_safe(&self.description)
        );
        format!(
            "<movie>\n\
                <title>{}</title>\n\
                <actor><name>{}</name></actor>\n\
                <plot>{}</plot>\n\
                <premiered>{}</premiered>\n\
                <dateadded>{}</dateadded>\n\
            </movie>",
            title,
            channel,
            description,
            self.upload_date.format("%Y-%m-%d %H:%M:%S"),
            Utc::now().format("%Y-%m-%d %H:%M:%S")
        )
    }
}
