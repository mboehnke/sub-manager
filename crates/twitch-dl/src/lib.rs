use std::str::FromStr as _;

use apply::Apply as _;
use camino::Utf8PathBuf;
pub use chrono::Duration;
use chrono::{DateTime, Utc};
use network::{Client, NetworkError};
use rss::{Channel, Item};
use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, PartialOrd, Ord)]
pub struct TwitchVideo {
    pub channel: String,
    pub title: String,
    pub upload_date: DateTime<Utc>,
    pub id: String,
}

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum TwitchError {
    #[error(transparent)]
    NetworkError(#[from] NetworkError),

    #[error("could not parse rss feed for channel: {channel:?}")]
    ChannelFeedParseError { source: rss::Error, channel: String },

    #[error("could not create download directory: {dir:?}")]
    DirectoryCreateError {
        source: std::io::Error,
        dir: Utf8PathBuf,
    },

    #[error("could not locate file after download: {file:?}")]
    StreamDownloadFileError { file: Utf8PathBuf },
}

impl TwitchVideo {
    pub async fn from_channel_feed(
        client: &Client,
        channel_name: &str,
        max_age: Duration,
    ) -> Result<Vec<TwitchVideo>, TwitchError> {
        let feed_url = format!("https://twitchrss.appspot.com/vod/{channel_name}");

        let channel = network::download_text(client, &feed_url)
            .await?
            .as_str()
            .apply(Channel::from_str)
            .map_err(|source| TwitchError::ChannelFeedParseError {
                source,
                channel: channel_name.to_string(),
            })?;

        let now = Utc::now();

        let videos = channel
            .items()
            .iter()
            .filter_map(Self::maybe_from)
            // younger than max_age
            .filter(|v| v.upload_date + max_age > now)
            .collect();

        Ok(videos)
    }

    fn maybe_from(item: &Item) -> Option<TwitchVideo> {
        let upload_date = item
            .pub_date()?
            .apply(DateTime::parse_from_rfc2822)
            .ok()?
            .into();
        Some(TwitchVideo {
            channel: item.source()?.title()?.to_string(),
            title: item.title()?.to_string(),
            upload_date,
            id: item.guid()?.value()[1..].to_string(),
        })
    }
}
