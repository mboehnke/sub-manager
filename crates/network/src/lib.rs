//! provides a few simple functions for accessing network ressources

#[cfg(test)]
mod tests;

use std::{io::Cursor, time::Duration};

use apply::Apply;
use camino::{Utf8Path, Utf8PathBuf};
use reqwest::ClientBuilder;
pub use reqwest::{Client, Url};
use thiserror::Error;
use tokio::fs::File;

/// NetworkError enumerates all possible errors returned by this library.
#[derive(Error, Debug)]
#[non_exhaustive]
pub enum NetworkError {
    #[error("cannot parse an absolute URL from {0:?}")]
    UrlParseError(String),

    #[error("could not construct http client")]
    ClientBuildError { source: reqwest::Error },

    #[error("could not download file: {url:?}")]
    FileDownloadError { source: reqwest::Error, url: String },

    #[error("could not create file: {file:?}")]
    FileCreateError {
        source: std::io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not write to file: {file:?}")]
    FileWriteError {
        source: std::io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not locate file after creation: {0:?}")]
    FileError(Utf8PathBuf),
}

/// default http client
///
/// (connection timeout: 20s, request timeout: 10min)
pub fn new_client() -> Result<Client, NetworkError> {
    ClientBuilder::new()
        .connect_timeout(std::time::Duration::from_secs(20))
        .timeout(std::time::Duration::from_secs(600))
        .build()
        .map_err(|source| NetworkError::ClientBuildError { source })
}

/// blocks until connection can be made
///
/// uses `http://google.com` for test
pub async fn wait_for_internet_connection() -> Result<(), NetworkError> {
    let url = "http://google.com";
    let client = new_client()?;
    while client.head(url).send().await.is_err() {
        tokio::time::sleep(Duration::from_secs(10)).await
    }
    Ok(())
}

/// follows redirections and returns the final url
///
/// (the maximum number of redirections depends on the client)
///
/// # Example
/// ```
/// # #[tokio::main]
/// # async fn main() {
/// let client = network::new_client().unwrap();
/// let url = network::resolve_redirects(&client, "http://youtu.be/dQw4w9WgXcQ").await.unwrap();
/// assert_eq!( url.to_string(), "https://www.youtube.com/watch?v=dQw4w9WgXcQ&feature=youtu.be" );
/// # }
/// ```
pub async fn resolve_redirects(client: &Client, url: &str) -> Result<Url, NetworkError> {
    let response =
        client
            .head(url)
            .send()
            .await
            .map_err(|source| NetworkError::FileDownloadError {
                source,
                url: url.to_string(),
            })?;
    Ok(response.url().clone())
}

/// downloads a file
///
/// returns content as String
pub async fn download_text(client: &Client, url: &str) -> Result<String, NetworkError> {
    client
        .get(url)
        .send()
        .await
        .map_err(|source| NetworkError::FileDownloadError {
            source,
            url: url.to_string(),
        })?
        .text()
        .await
        .map_err(|source| NetworkError::FileDownloadError {
            source,
            url: url.to_string(),
        })
}

/// downloads a file
///
/// returns path to downloaded file
pub async fn download_file(
    client: &Client,
    url: &str,
    path: &Utf8Path,
) -> Result<Utf8PathBuf, NetworkError> {
    let url: Url = url
        .parse()
        .map_err(|_| NetworkError::UrlParseError(url.to_string()))?;

    let mut bytes = client
        .get(url.clone())
        .send()
        .await
        .map_err(|source| NetworkError::FileDownloadError {
            source,
            url: url.to_string(),
        })?
        .bytes()
        .await
        .map_err(|source| NetworkError::FileDownloadError {
            source,
            url: url.to_string(),
        })?
        .apply(Cursor::new);

    let mut file = File::create(path)
        .await
        .map_err(|source| NetworkError::FileCreateError {
            source,
            file: path.to_path_buf(),
        })?;

    tokio::io::copy(&mut bytes, &mut file)
        .await
        .map_err(|source| NetworkError::FileWriteError {
            source,
            file: path.to_path_buf(),
        })?;

    if !path.is_file() {
        return Err(NetworkError::FileError(path.to_path_buf()));
    }

    Ok(path.to_path_buf())
}
