FROM rust AS builder
RUN rustup target add wasm32-unknown-unknown
RUN rustup component add rustfmt
COPY ./ ./
RUN cargo build --release

FROM ubuntu:focal
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \ 
    apt-get install -y aria2 ffmpeg openssl ca-certificates python3 python3-pip tzdata libwebp6 wget && \
    ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata
RUN cd /tmp && \
    wget "https://github.com/yt-dlp/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-linux64-gpl.tar.xz" &&\
    tar -xJf ffmpeg-master-latest-linux64-gpl.tar.xz &&\
    mv -f ffmpeg-master-latest-linux64-gpl/* /usr/bin
COPY --from=builder ./target/release/sub-manager /usr/bin/
CMD ["/usr/bin/sub-manager"]
